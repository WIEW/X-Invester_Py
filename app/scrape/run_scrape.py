import os
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from test_auto_project_renthub import startrenthub
from test_auto_project_res import startres
from test_full_ppth import startppth
from test_mongo_tht_full import starttht
import schedule
import time

def install_wd():

    options = Options()
    options.add_argument('--no-sandbox')
    options.add_argument("--headless")
    options.add_argument('--disable-dev-shm-usage')
    # options.add_argument("--remote-debugging-port=9230")  # this

    options.add_argument("--disable-dev-shm-using") 
    options.add_argument("--disable-extensions") 
    options.add_argument("--disable-gpu") 
    options.add_argument("start-maximized") 
    options.add_argument('--ignore-ssl-errors=yes')
    options.add_argument('--ignore-certificate-errors')
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
    options.add_argument(f'user-agent={user_agent}')
    wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)

    # self.wd = webdriver.Chrome(chrome_options=chromeOptions) 
    # self.wd = webdriver.Chrome(PATH, chrome_options=chrome_options)
    wd.maximize_window()
    wd.quit()

def wd_scarpe():
    print("I'm working wd_scarpe")
    os.system('sudo kill -9 $(sudo lsof -t -i:9230)')
    install_wd()
    os.system('sudo kill -9 $(sudo lsof -t -i:9230)')

    a = startrenthub()
    a.startrenthub()
    # rta = a.startrenthub()
    # if rta ==
    os.system('sudo kill -9 $(sudo lsof -t -i:9230)')

    b = startres()
    b.startres()
    os.system('sudo kill -9 $(sudo lsof -t -i:9230)')

    c = startppth()
    c.startppth()
    os.system('sudo kill -9 $(sudo lsof -t -i:9230)')

    d = starttht()
    d.starttht()
    print('Finish')



def job():
    print("I'm working...")

os.system('sudo kill -9 $(sudo lsof -t -i:9230)')

wd_scarpe()
# try:
schedule.every(50).days.do(wd_scarpe)
# schedule.every(10).seconds.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)