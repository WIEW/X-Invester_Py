from re import T
import requests
from bs4 import BeautifulSoup
from email.mime import image
import requests
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo
from dotenv import load_dotenv
from collections import ChainMap
from bson.objectid import ObjectId
from datetime import date
import schedule
import datetime


mode = 0o666


ch_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_lvisd"
sys_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_lvisd/imgs_{}"



# ##Start of db zone###

# myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")

mydb = myclient["devXInvester"]
mycol_lvisd = mydb["watchLists"]
mycol_c = mydb["watchLists_clvisd"]

# mydb = myclient["devXInvester"]
# mycol = mydb["propoty_project_urls"]


def insert_data_mongo(raw_data_home):

    mydict = raw_data_home

    x = mycol_lvisd.insert_one(mydict)

# def update_data_mongo_a(raw_data_home ,condo_data, house_data):
#     myquery = {"cut_find_url": condo_data}
#     mydoc = mycol_lvisd.find(myquery)

#     update_data = {"$set":raw_data_home}

#     mycol_lvisd.update_one(myquery,update_data)

def insert_fails(raw_data_home):
    mydict = raw_data_home

    mycol_c.insert_one(mydict)
##End of db zone###


condo_list = []
cd_page_url = []
hs_page_url = []
ld_page_url = []
th_page_url = []
sh_page_url = []
rt_page_url = []
of_page_url = []
ho_page_url = []
fw_page_url = []
sr_page_url = []
bs_page_url = []
condo_list_url = []
house_list_url = []
land_list_url = []
townhouse_list_url = []
shophouse_list_url = []
retail_list_url = []
office_list_url = []
homeoffice_list_url = []
fw_list_url = []
sr_list_url = []
bs_list_url = []

def get_condo_data():
    i = 0
    cd_p_url = 'https://www.livinginsider.com/searchword/Condo/all/1/%E0%B8%A3%E0%B8%A7%E0%B8%A1%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%82%E0%B8%B2%E0%B8%A2-%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%E0%B8%84%E0%B8%AD%E0%B8%99%E0%B9%82%E0%B8%94.html'
    web_data = requests.get(cd_p_url)
    soup = BeautifulSoup(web_data.text, 'html.parser')
    find_page = soup.find_all("a",{"class":"loading_page"})
    last_page = find_page[-9].text
    cd_int_last_page = int(last_page)
    # cd_int_last_page = int(3)
    print(cd_int_last_page)

    for page in range(cd_int_last_page):
        page += 1
        # cpage = 1
        cd_url = f"https://www.livinginsider.com/searchword/Condo/all/{page}/%E0%B8%A3%E0%B8%A7%E0%B8%A1%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%82%E0%B8%B2%E0%B8%A2-%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%E0%B8%84%E0%B8%AD%E0%B8%99%E0%B9%82%E0%B8%94.html"
        cd_page_url.append(cd_url)

        print('cd_url',cd_url)

        web_data = requests.get(cd_url)
        soup = BeautifulSoup(web_data.text, 'html.parser')
        find_condo_lists = soup.findAll('div', {'class' : 'item-desc'})

        for find_condo_list in find_condo_lists:
            
            hrefs = find_condo_list.find_all('a', href=True)
            for href in hrefs:
                condo_list_url.append(href['href'])
        # cpage += 1

    print('cd_page_url',cd_page_url)

    ct_condo_list_url = len(condo_list_url)
    print(condo_list_url)
    # print('condo_list_url')
    print(ct_condo_list_url)

    for condo_data in condo_list_url:

        try:

            details = []

            print(condo_data)

            web_data = requests.get(condo_data)
            soup = BeautifulSoup(web_data.text, 'html.parser')
            find_url = condo_data
            find_web = 'livinginsider'#find_url.split(".")[1]
            find_name = soup.find_all("h1",{"class":"font-Sarabun"})
            find_location = soup.find("div",{"class":"detail-text-zone"})
            find_address = soup.find("div",{"class":"detail-text-project"})
            find_price = soup.find_all("span",{"class":"price-detail"})
            find_list_type = soup.find_all("span",{"class":"box-tag-detail"})
            find_size = soup.find_all("span",{"class":"detail-property-list-text"})
            find_floor = soup.find_all("span",{"class":"detail-property-list-text"})
            find_bedroom = soup.find_all("span",{"class":"detail-property-list-text"})
            find_bathroom = soup.find_all("span",{"class":"detail-property-list-text"})
            find_description = soup.find("p",{"class":"wordwrap"}).text
            try:
                find_map = soup.find("a",{"class":"btn new-detail-gmap_"})["href"]
            except:
                find_map = 'no map'
            find_list_type = soup.find_all("span",{"class":"box-tag-detail"})
            try:
                find_property_type = soup.find("div",{"class":"box-tag-detail tag-build-condo-detail"}).text
            except:
                try:
                    find_property_type = soup.find("div",{"box-tag-detail tag-build-home-detail"}).text
                except:
                    find_property_type = soup.find("div",{"box-tag-detail tag-build-townhome-detail"}).text  

            img_url_lists = []
            img_path = []

            img_topic_dynamic = soup.find('div',{'class':'img-topic-dynamic'})
            try:
                img_url = soup.find('img',{'class':'mbSlideDown gridsection-1'})['src']
                img_url_lists.append(img_url)
            except:
                print('no 1 pic')
            try:
                img_url2 = soup.find('img',{'class':'mbSlideDown gridHeightcal1-1 gridsection-sub-main'})['src']
                img_url_lists.append(img_url2)
            except:
                print('no 2 pic')
            try:
                img_url3 = soup.find('img',{'class':'mbSlideDown gridHeightcal2-1 gridsection-sub1'})['src']
                img_url_lists.append(img_url3)
            except:
                print('no 3 pic')
            imgs = soup.find('div',{'class':'fotorama__thumb fotorama__loaded fotorama__loaded--img'})#['src']

            print(img_url_lists)

            post_id = condo_data.split('/')[4]
            save_path = sys_xpath.format(post_id)

            os.chdir(os.path.join(os.getcwd(), ch_xpath))

            try:
                os.mkdir(save_path, mode)
                os.chdir(os.path.join(os.getcwd(), save_path))
            except:
                os.chdir(os.path.join(os.getcwd(), save_path))

            c_p = 1

            for img_url_list in (img_url_lists):
                file_name = str(c_p) + ".jpg"
                try:
                    image_content = requests.get(img_url_list).content
                    image_file = io.BytesIO(image_content)
                    image = Image.open(image_file)
                    file_path = save_path +'/' + file_name

                    img_path.append(file_path)

                    with open(file_path, "wb") as f:
                        image.save(f, "PNG")
                        
                    print("Success")

                except Exception as e:
                    print('FAILED -', e)

                c_p += 1

            if len(find_name) == 1:
                cut_find_url = find_url
                cut_find_web = find_url.split(".")[1]
                cut_find_name = find_name[0].text.strip()
                cut_find_location = find_location.text.strip()
                try:
                    cut_find_address = find_address.text.strip()
                except:
                    cut_find_address = 'no address project'
                cut_find_price = find_price[0].text.strip()
                cut_find_size2 = find_size[0].text.split()
                cut_find_size = ' '.join(cut_find_size2)
                cut_find_floor = find_floor[1].text.strip()
                text_cut_find_bedroom = find_bedroom[2].text.strip().replace('\n                                                  ', ' ')
                cut_find_bedroom = text_cut_find_bedroom.split()[0]
                text_cut_find_bathroom = find_bathroom[3].text.strip()
                cut_find_bathroom = text_cut_find_bathroom.split()[0]
                cut_find_map = find_map
                cut_find_contract_type = find_list_type[0].text.strip()
                if 'เช่า' in cut_find_contract_type or cut_find_contract_type == 'ให้เช่า':
                    list_type = 'rent'
                elif cut_find_contract_type == 'ขาย':
                    list_type = 'sell'
                if find_property_type == 'คอนโด':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a0f')
                elif find_property_type == 'บ้าน':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a11')
                elif find_property_type == 'ทาวน์เฮ้าส์/ทาวน์โฮม':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a15')
                post_number = 'lvisd_'+post_id

                raw_data_home = {
                    'cut_find_web': cut_find_web,
                    'cut_find_url': condo_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': cut_find_name,
                    'listType': list_type,
                    'propertyTypes': propertyType,
                    'totalBedroom': cut_find_bedroom,
                    'totalToilet': cut_find_bathroom,
                    'startingPrice': cut_find_price,
                    'finalPrice': cut_find_price,
                    'minimumArea': cut_find_size,
                    'maximumArea': cut_find_size,
                    'description': find_description,
                    'cut_find_location': cut_find_location,
                    'cut_find_address': cut_find_address,
                    'cut_find_contract_type': cut_find_contract_type,
                    'cut_find_floor': cut_find_floor,
                    'cut_find_map': cut_find_map,
                    'date': datetime.datetime.utcnow(),
                }
                # print(raw_data_home)
                raw_data_homeu = {
                    'cut_find_web': cut_find_web,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': cut_find_name,
                    'listType': list_type,
                    'propertyTypes': propertyType,
                    'totalBedroom': cut_find_bedroom,
                    'totalToilet': cut_find_bathroom,
                    'startingPrice': cut_find_price,
                    'finalPrice': cut_find_price,
                    'minimumArea': cut_find_size,
                    'maximumArea': cut_find_size,
                    'description': find_description,
                    'cut_find_location': cut_find_location,
                    'cut_find_address': cut_find_address,
                    'cut_find_contract_type': cut_find_contract_type,
                    'cut_find_floor': cut_find_floor,
                    'cut_find_map': cut_find_map,
                    'date': datetime.datetime.utcnow(),
                }
                # insert_data_mongo(raw_data_home)
                def update_data_mongo(post_number ,raw_data_homeu):
                    myquery = {"post_no": post_number}
                    mydoc = mycol_lvisd.find(myquery)

                    update_data = {"$set":raw_data_homeu}

                    mycol_lvisd.update_one(myquery,update_data)

                try:
                    print('try before insert')
                    try:
                        insert_data_mongo(raw_data_home)
                    except Exception as e:
                        print('insert error:',e)
                        insert_data_mongo(raw_data_home)
                    print('try after insert')
                except:
                    print('try before update')
                    update_data_mongo(post_number ,raw_data_homeu)
                    print('try after update')



        ########################################################################################
                # if condo_list_url[i] not in {"cut_find_url": condo_list_url} :
                #     insert_data_mongo(raw_data_home)

                # else :
                #     update_data_mongo(raw_data_home)
        ########################################################################################

                print(condo_list_url[i])
                
                # print(i+1,condo_list_url[i],cut_find_name,cut_find_location,cut_find_address,cut_find_price,details)
                # print(i+1,cut_find_name,cut_find_location,cut_find_address,cut_find_price,details)


            else:
                print("222222222222222222222222222222")
                print('error',i,condo_list_url[i])

            i += 1

        except Exception as e:
            print(e)
            raw_data_home = {
                'cannot_open_url': condo_data
            }
            insert_fails(raw_data_home)

def get_house_data():
    i = 0
    hs_p_url = 'https://www.livinginsider.com/searchword/Home/all/1/%E0%B8%A3%E0%B8%A7%E0%B8%A1%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%82%E0%B8%B2%E0%B8%A2-%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99.html'
    web_data = requests.get(hs_p_url)
    soup = BeautifulSoup(web_data.text, 'html.parser')
    find_page = soup.find_all("a",{"class":"loading_page"})
    last_page = find_page[-9].text
    house_int_last_page = int(last_page)
    # house_int_last_page = int(3)
    print(house_int_last_page)

    for page in range(house_int_last_page):
        page += 1
        # cpage = 1    

        hs_url = f"https://www.livinginsider.com/searchword/Home/all/{page}/property-listing-house-for-sale-rent.html"
        hs_page_url.append(hs_url)

        print('hs_url',hs_url)

        web_data = requests.get(hs_url)
        soup = BeautifulSoup(web_data.text, 'html.parser')
        find_house_lists = soup.findAll('div', {'class' : 'item-desc'})

        for find_house_list in find_house_lists:
            
            hrefs = find_house_list.find_all('a', href=True)
            for href in hrefs:
                house_list_url.append(href['href'])
        # cpage += 1

    print(cd_page_url)
    print(house_int_last_page)

    ct_house_list_url = len(house_list_url)
    print(house_list_url)
    # print('condo_list_url')
    print(ct_house_list_url)

    for house_data in house_list_url:

        try:
            details = []

            web_data = requests.get(house_data)
            soup = BeautifulSoup(web_data.text, 'html.parser')
            find_url = house_data
            find_web = 'livinginsider'#find_url.split(".")[1]
            find_name = soup.find_all("h1",{"class":"font-Sarabun"})
            find_location = soup.find("div",{"class":"detail-text-zone"})
            find_address = soup.find("div",{"class":"detail-text-project"})
            find_price = soup.find_all("span",{"class":"price-detail"})
            find_list_type = soup.find_all("span",{"class":"box-tag-detail"})
            find_size = soup.find_all("span",{"class":"detail-property-list-text"})
            find_floor = soup.find_all("span",{"class":"detail-property-list-text"})
            find_bedroom = soup.find_all("span",{"class":"detail-property-list-text"})
            find_bathroom = soup.find_all("span",{"class":"detail-property-list-text"})
            find_description = soup.find("p",{"class":"wordwrap"}).text
            try:
                find_map = soup.find("a",{"class":"btn new-detail-gmap_"})["href"]
            except:
                find_map = 'no map'
            find_list_type = soup.find_all("span",{"class":"box-tag-detail"})
            try:
                find_property_type = soup.find("div",{"class":"box-tag-detail tag-build-condo-detail"}).text
            except:
                try:
                    find_property_type = soup.find("div",{"box-tag-detail tag-build-home-detail"}).text
                except:
                    find_property_type = soup.find("div",{"box-tag-detail tag-build-townhome-detail"}).text    

            img_url_lists = []
            img_path = []


            img_topic_dynamic = soup.find('div',{'class':'img-topic-dynamic'})
            try:
                img_url = soup.find('img',{'class':'mbSlideDown gridsection-1'})['src']
                img_url_lists.append(img_url)
            except:
                print('no 1 pic')
            try:
                img_url2 = soup.find('img',{'class':'mbSlideDown gridHeightcal1-1 gridsection-sub-main'})['src']
                img_url_lists.append(img_url2)
            except:
                print('no 2 pic')
            try:
                img_url3 = soup.find('img',{'class':'mbSlideDown gridHeightcal2-1 gridsection-sub1'})['src']
                img_url_lists.append(img_url3)
            except:
                print('no 3 pic')
            imgs = soup.find('div',{'class':'fotorama__thumb fotorama__loaded fotorama__loaded--img'})#['src']

            print(img_url_lists)

            post_id = house_data.split('/')[4]
            save_path = sys_xpath.format(post_id)

            os.chdir(os.path.join(os.getcwd(), ch_xpath))

            try:
                os.mkdir(save_path, mode)
                os.chdir(os.path.join(os.getcwd(), save_path))
            except:
                os.chdir(os.path.join(os.getcwd(), save_path))

            c_p = 1

            for img_url_list in (img_url_lists):
                file_name = str(c_p) + ".jpg"
                try:
                    image_content = requests.get(img_url_list).content
                    image_file = io.BytesIO(image_content)
                    image = Image.open(image_file)
                    file_path = save_path +'/' + file_name

                    img_path.append(file_path)

                    with open(file_path, "wb") as f:
                        image.save(f, "PNG")
                        
                    print("Success")

                except Exception as e:
                    print('FAILED -', e)

                c_p += 1 

            if len(find_name) == 1:
                cut_find_url = find_url
                cut_find_web = find_url.split(".")[1]
                cut_find_name = find_name[0].text.strip()
                cut_find_location = find_location[0].text.strip()
                try:
                    cut_find_address = find_address[0].text.strip()
                except:
                    cut_find_address = 'no address project'
                cut_find_price = find_price[0].text.strip()
                cut_find_size2 = find_size[0].text.split()
                cut_find_size = ' '.join(cut_find_size2)
                cut_find_floor = find_floor[1].text.strip()
                text_cut_find_bedroom = find_bedroom[2].text.strip().replace('\n                                                  ', ' ')
                cut_find_bedroom = text_cut_find_bedroom.split()[0]
                text_cut_find_bathroom = find_bathroom[3].text.strip()
                cut_find_bathroom = text_cut_find_bathroom.split()[0]
                cut_find_map = find_map
                cut_find_contract_type = find_list_type[0].text.strip()
                if 'เช่า' in cut_find_contract_type or cut_find_contract_type == 'ให้เช่า':
                    list_type = 'rent'
                elif cut_find_contract_type == 'ขาย':
                    list_type = 'sell'
                if find_property_type == 'คอนโด':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a0f')
                elif find_property_type == 'บ้าน':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a11')
                elif find_property_type == 'ทาวน์เฮ้าส์/ทาวน์โฮม':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a15')
                post_number = 'lvisd_'+post_id

                raw_data_home = {
                    'cut_find_web': cut_find_web,
                    'cut_find_url': house_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': cut_find_name,
                    'listType': list_type,
                    'propertyTypes': propertyType,
                    'totalBedroom': cut_find_bedroom,
                    'totalToilet': cut_find_bathroom,
                    'startingPrice': cut_find_price,
                    'finalPrice': cut_find_price,
                    'minimumArea': cut_find_size,
                    'maximumArea': cut_find_size,
                    'description': find_description,
                    'cut_find_location': cut_find_location,
                    'cut_find_address': cut_find_address,
                    'cut_find_contract_type': cut_find_contract_type,
                    'cut_find_floor': cut_find_floor,
                    'cut_find_map': cut_find_map,
                    'date': datetime.datetime.utcnow(),
                }
                raw_data_homeu = {
                    'cut_find_url': house_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': cut_find_name,
                    'listType': list_type,
                    'propertyTypes': propertyType,
                    'totalBedroom': cut_find_bedroom,
                    'totalToilet': cut_find_bathroom,
                    'startingPrice': cut_find_price,
                    'finalPrice': cut_find_price,
                    'minimumArea': cut_find_size,
                    'maximumArea': cut_find_size,
                    'description': find_description,
                    'cut_find_location': cut_find_location,
                    'cut_find_address': cut_find_address,
                    'cut_find_contract_type': cut_find_contract_type,
                    'cut_find_floor': cut_find_floor,
                    'cut_find_map': cut_find_map,
                    'date': datetime.datetime.utcnow(),
                }

                def update_data_mongo(post_number, raw_data_homeu):
                    myquery = {"post_no": post_number}
                    mydoc = mycol_lvisd.find(myquery)

                    update_data = {"$set":raw_data_homeu}

                    mycol_lvisd.update_one(myquery,update_data)

                try:
                    print('try before insert')
                    try:
                        insert_data_mongo(raw_data_home)
                    except Exception as e:
                        print('insert error:',e)
                        insert_data_mongo(raw_data_home)
                    print('try after insert')
                except:
                    print('try before update')
                    update_data_mongo(post_number, raw_data_homeu)
                    print('try after update')



        ########################################################################################
                # if condo_list_url[i] not in {"cut_find_url": condo_list_url} :
                #     insert_data_mongo(raw_data_home)

                # else :
                #     update_data_mongo(raw_data_home)
        ########################################################################################

                print(condo_list_url[i])
                
                # print(i+1,condo_list_url[i],cut_find_name,cut_find_location,cut_find_address,cut_find_price,details)
                # print(i+1,cut_find_name,cut_find_location,cut_find_address,cut_find_price,details)


            else:
                print("222222222222222222222222222222")
                print('error',i,condo_list_url[i])

            i += 1
        
        except Exception as e:
            print(e)
            raw_data_home = {
                'cannot_open_url': house_data
            }
            insert_fails(raw_data_home)

def get_land_data():
    i = 0
    ld_p_url = 'https://www.livinginsider.com/searchword/Land/all/1/%E0%B8%A3%E0%B8%A7%E0%B8%A1%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%82%E0%B8%B2%E0%B8%A2-%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%94%E0%B8%B4%E0%B8%99.html'
    web_data = requests.get(ld_p_url)
    soup = BeautifulSoup(web_data.text, 'html.parser')
    find_page = soup.find_all("a",{"class":"loading_page"})
    last_page = find_page[-9].text
    land_int_last_page = int(last_page)
    # land_int_last_page = int(3)
    print(land_int_last_page)

    for page in range(land_int_last_page):
        page += 1
        # cpage = 1    

        ld_url = f"https://www.livinginsider.com/searchword/Land/all/{page}/%E0%B8%A3%E0%B8%A7%E0%B8%A1%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%82%E0%B8%B2%E0%B8%A2-%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%94%E0%B8%B4%E0%B8%99.html"
        ld_page_url.append(ld_url)

        print('ld_url',ld_url)
        
        web_data = requests.get(ld_url)
        soup = BeautifulSoup(web_data.text, 'html.parser')
        find_land_lists = soup.findAll('div', {'class' : 'item-desc'})

        for find_land_list in find_land_lists:
            
            hrefs = find_land_list.find_all('a', href=True)
            for href in hrefs:
                land_list_url.append(href['href'])
        # cpage += 1

    print(ld_page_url)

    ct_land_list_url = len(land_list_url)
    print(land_list_url)
    # print('condo_list_url')
    print(ct_land_list_url)

    for land_data in land_list_url:

        try:

            details = []

            web_data = requests.get(land_data)
            soup = BeautifulSoup(web_data.text, 'html.parser')
            find_url = land_data
            find_web = 'livinginsider'#find_url.split(".")[1]
            find_name = soup.find_all("h1",{"class":"font-Sarabun"})
            find_location = soup.find("div",{"class":"detail-text-zone"})
            find_price = soup.find_all("span",{"class":"price-detail"})
            find_list_type = soup.find_all("span",{"class":"box-tag-detail"})
            find_size = soup.find_all("span",{"class":"detail-property-list-text"})
            find_description = soup.find("p",{"class":"wordwrap"}).text
            try:
                find_map = soup.find("a",{"class":"btn new-detail-gmap_"})["href"]
            except:
                find_map = 'no map'
            find_list_type = soup.find_all("span",{"class":"box-tag-detail"})
            try:
                find_property_type = soup.find("div",{"class":"box-tag-detail tag-build-condo-detail"}).text
            except:
                try:
                    find_property_type = soup.find("div",{"box-tag-detail tag-build-home-detail"}).text
                except:
                    find_property_type = soup.find("div",{"box-tag-detail tag-build-townhome-detail"}).text    

            img_url_lists = []
            img_path = []

            img_topic_dynamic = soup.find('div',{'class':'img-topic-dynamic'})
            try:
                img_url = soup.find('img',{'class':'mbSlideDown gridsection-1'})['src']
                img_url_lists.append(img_url)
            except:
                print('no 1 pic')
            try:
                img_url2 = soup.find('img',{'class':'mbSlideDown gridHeightcal1-1 gridsection-sub-main'})['src']
                img_url_lists.append(img_url2)
            except:
                print('no 2 pic')
            try:
                img_url3 = soup.find('img',{'class':'mbSlideDown gridHeightcal2-1 gridsection-sub1'})['src']
                img_url_lists.append(img_url3)
            except:
                print('no 3 pic')
            imgs = soup.find('div',{'class':'fotorama__thumb fotorama__loaded fotorama__loaded--img'})#['src']

            print(img_url_lists)

            #"/root/X-Invester_Py/app/public/api_imgs/test_lvisd/imgs_{}"
            post_id = land_data.split('/')[4]
            save_path = sys_xpath.format(post_id)

            os.chdir(os.path.join(os.getcwd(), ch_xpath))

            try:
                os.mkdir(save_path, mode)
                os.chdir(os.path.join(os.getcwd(), save_path))
            except:
                os.chdir(os.path.join(os.getcwd(), save_path))

            c_p = 1

            for img_url_list in (img_url_lists):
                file_name = str(c_p) + ".jpg"
                try:
                    image_content = requests.get(img_url_list).content
                    image_file = io.BytesIO(image_content)
                    image = Image.open(image_file)
                    file_path = save_path +'/' + file_name

                    img_path.append(file_path)

                    with open(file_path, "wb") as f:
                        image.save(f, "PNG")
                        
                    print("Success")

                except Exception as e:
                    print('FAILED -', e)

                c_p += 1

            if len(find_name) == 1:
                cut_find_url = find_url
                cut_find_web = find_url.split(".")[1]
                cut_find_name = find_name[0].text.strip()
                cut_find_location = find_location[0].text.strip()
                cut_find_price = find_price[0].text.strip()
                cut_find_size2 = find_size[0].text.split()
                cut_find_size = ' '.join(cut_find_size2)

                cut_find_map = find_map
                cut_find_contract_type = find_list_type[0].text.strip()
                if 'เช่า' in cut_find_contract_type or cut_find_contract_type == 'ให้เช่า':
                    list_type = 'rent'
                elif cut_find_contract_type == 'ขาย':
                    list_type = 'sell'
                if find_property_type == 'คอนโด':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a0f')
                elif find_property_type == 'บ้าน':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a11')
                elif find_property_type == 'ทาวน์เฮ้าส์/ทาวน์โฮม':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a15')
                post_number = 'lvisd_'+post_id

                raw_data_home = {
                    'cut_find_web': cut_find_web,
                    'cut_find_url': land_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': cut_find_name,
                    'listType': list_type,
                    'propertyTypes': propertyType,
                    'totalBedroom': '-',
                    'totalToilet': '-',
                    'startingPrice': cut_find_price,
                    'finalPrice': cut_find_price,
                    'minimumArea': cut_find_size,
                    'maximumArea': cut_find_size,
                    'description': find_description,
                    'cut_find_location': cut_find_location,
                    'cut_find_contract_type': cut_find_contract_type,
                    'cut_find_floor': '-',
                    'cut_find_map': cut_find_map,
                    'date': datetime.datetime.utcnow(),
                }

                raw_data_homeu = {
                    'cut_find_web': cut_find_web,
                    'galleryUrls': img_path,
                    'name': cut_find_name,
                    'listType': list_type,
                    'propertyTypes': propertyType,
                    'totalBedroom': '-',
                    'totalToilet': '-',
                    'startingPrice': cut_find_price,
                    'finalPrice': cut_find_price,
                    'minimumArea': cut_find_size,
                    'maximumArea': cut_find_size,
                    'description': find_description,
                    'cut_find_location': cut_find_location,
                    'cut_find_contract_type': cut_find_contract_type,
                    'cut_find_floor': '-',
                    'cut_find_map': cut_find_map,
                    'date': datetime.datetime.utcnow(),
                }

                def update_data_mongo(post_number ,raw_data_homeu):
                    myquery = {"post_no": post_number}
                    mydoc = mycol_lvisd.find(myquery)

                    update_data = {"$set":raw_data_homeu}

                    mycol_lvisd.update_one(myquery,update_data)

                try:
                    print('try before insert')
                    try:
                        insert_data_mongo(raw_data_home)
                    except Exception as e:
                        print('insert error:',e)
                        insert_data_mongo(raw_data_home)
                    print('try after insert')
                except:
                    print('try before update')
                    update_data_mongo(post_number ,raw_data_homeu)
                    print('try after update')



        ########################################################################################
                # if condo_list_url[i] not in {"cut_find_url": condo_list_url} :
                #     insert_data_mongo(raw_data_home)

                # else :
                #     update_data_mongo(raw_data_home)
        ########################################################################################

                print(condo_list_url[i])
                
                # print(i+1,condo_list_url[i],cut_find_name,cut_find_location,cut_find_address,cut_find_price,details)
                # print(i+1,cut_find_name,cut_find_location,cut_find_address,cut_find_price,details)


            else:
                print("222222222222222222222222222222")
                print('error',i,condo_list_url[i])

            i += 1

        except Exception as e:
            print(e)
            raw_data_home = {
                'cannot_open_url': land_data
            }
            insert_fails(raw_data_home)

def get_townhouse_data():
    i = 0
    ld_p_url = 'https://www.livinginsider.com/searchword/Townhome/all/1/%E0%B8%A3%E0%B8%A7%E0%B8%A1%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%82%E0%B8%B2%E0%B8%A2-%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2-%E0%B9%80%E0%B8%8B%E0%B9%89%E0%B8%87%E0%B8%97%E0%B8%B2%E0%B8%A7%E0%B8%99%E0%B9%8C%E0%B9%80%E0%B8%AE%E0%B9%89%E0%B8%B2%E0%B8%AA%E0%B9%8C-%E0%B8%97%E0%B8%B2%E0%B8%A7%E0%B8%99%E0%B9%8C%E0%B9%82%E0%B8%AE%E0%B8%A1.html'
    web_data = requests.get(ld_p_url)
    soup = BeautifulSoup(web_data.text, 'html.parser')
    find_page = soup.find_all("a",{"class":"loading_page"})
    last_page = find_page[-9].text
    townhouse_int_last_page = int(last_page)
    # townhouse_int_last_page = int(3)
    print(townhouse_int_last_page)

    for page in range(townhouse_int_last_page):
        page += 1
        # cpage = 1    

        th_url = f"https://www.livinginsider.com/searchword/Townhome/all/{page}/%E0%B8%A3%E0%B8%A7%E0%B8%A1%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%82%E0%B8%B2%E0%B8%A2-%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2-%E0%B9%80%E0%B8%8B%E0%B9%89%E0%B8%87%E0%B8%97%E0%B8%B2%E0%B8%A7%E0%B8%99%E0%B9%8C%E0%B9%80%E0%B8%AE%E0%B9%89%E0%B8%B2%E0%B8%AA%E0%B9%8C-%E0%B8%97%E0%B8%B2%E0%B8%A7%E0%B8%99%E0%B9%8C%E0%B9%82%E0%B8%AE%E0%B8%A1.html"
        th_page_url.append(th_url)

        print('th_url',th_url)

        web_data = requests.get(th_url)
        soup = BeautifulSoup(web_data.text, 'html.parser')
        find_th_lists = soup.findAll('div', {'class' : 'item-desc'})

        for find_th_list in find_th_lists:
            
            hrefs = find_th_list.find_all('a', href=True)
            for href in hrefs:
                townhouse_list_url.append(href['href'])
        # cpage += 1

    print(th_page_url)

    ct_land_list_url = len(townhouse_list_url)
    print(townhouse_list_url)
    # print('condo_list_url')
    print(ct_land_list_url)

    for townhouse_data in townhouse_list_url:

        try:

            details = []

            web_data = requests.get(townhouse_data)
            soup = BeautifulSoup(web_data.text, 'html.parser')
            find_url = townhouse_data
            find_web = 'livinginsider'#find_url.split(".")[1]
            find_name = soup.find_all("h1",{"class":"font-Sarabun"})
            find_location = soup.find_all("div",{"class":"detail-text-zone"})
            find_address = soup.find_all("div",{"class":"detail-text-project"})
            find_price = soup.find_all("span",{"class":"price-detail"})
            find_list_type = soup.find_all("span",{"class":"box-tag-detail"})
            find_size = soup.find_all("span",{"class":"detail-property-list-text"})
            find_floor = soup.find_all("span",{"class":"detail-property-list-text"})
            find_bedroom = soup.find_all("span",{"class":"detail-property-list-text"})
            find_bathroom = soup.find_all("span",{"class":"detail-property-list-text"})
            find_description = soup.find("p",{"class":"wordwrap"}).text
            try:
                find_map = soup.find("a",{"class":"btn new-detail-gmap_"})["href"]
            except:
                find_map = 'no map'
            find_list_type = soup.find_all("span",{"class":"box-tag-detail"})
            try:
                find_property_type = soup.find("div",{"class":"box-tag-detail tag-build-condo-detail"}).text
            except:
                try:
                    find_property_type = soup.find("div",{"box-tag-detail tag-build-home-detail"}).text
                except:
                    find_property_type = soup.find("div",{"box-tag-detail tag-build-townhome-detail"}).text    

            img_url_lists = []
            img_path = []

            img_topic_dynamic = soup.find('div',{'class':'img-topic-dynamic'})
            try:
                img_url = soup.find('img',{'class':'mbSlideDown gridsection-1'})['src']
                img_url_lists.append(img_url)
            except:
                print('no 1 pic')
            try:
                img_url2 = soup.find('img',{'class':'mbSlideDown gridHeightcal1-1 gridsection-sub-main'})['src']
                img_url_lists.append(img_url2)
            except:
                print('no 2 pic')
            try:
                img_url3 = soup.find('img',{'class':'mbSlideDown gridHeightcal2-1 gridsection-sub1'})['src']
                img_url_lists.append(img_url3)
            except:
                print('no 3 pic')
            imgs = soup.find('div',{'class':'fotorama__thumb fotorama__loaded fotorama__loaded--img'})#['src']

            print(img_url_lists)

            post_id = townhouse_data.split('/')[4]
            save_path = sys_xpath.format(post_id)

            os.chdir(os.path.join(os.getcwd(), ch_xpath))

            try:
                os.mkdir(save_path, mode)
                os.chdir(os.path.join(os.getcwd(), save_path))
            except:
                os.chdir(os.path.join(os.getcwd(), save_path))

            c_p = 1

            for img_url_list in (img_url_lists):
                file_name = str(c_p) + ".jpg"
                try:
                    image_content = requests.get(img_url_list).content
                    image_file = io.BytesIO(image_content)
                    image = Image.open(image_file)
                    file_path = save_path +'/' + file_name

                    img_path.append(file_path)

                    with open(file_path, "wb") as f:
                        image.save(f, "PNG")
                        
                    print("Success")

                except Exception as e:
                    print('FAILED -', e)

                c_p += 1

            if len(find_name) == 1:
                cut_find_url = find_url
                cut_find_web = find_url.split(".")[1]
                cut_find_name = find_name[0].text.strip()
                cut_find_location = find_location[0].text.strip()
                try:
                    cut_find_address = find_address[0].text.strip()
                except:
                    cut_find_address = 'no address project'
                cut_find_price = find_price[0].text.strip()
                cut_find_size2 = find_size[0].text.split()
                cut_find_size = ' '.join(cut_find_size2)
                cut_find_floor = find_floor[1].text.strip()
                text_cut_find_bedroom = find_bedroom[2].text.strip().replace('\n                                                  ', ' ')
                cut_find_bedroom = text_cut_find_bedroom.split()[0]
                text_cut_find_bathroom = find_bathroom[3].text.strip()
                cut_find_bathroom = text_cut_find_bathroom.split()[0]
                cut_find_map = find_map
                cut_find_contract_type = find_list_type[0].text.strip()
                if 'เช่า' in cut_find_contract_type or cut_find_contract_type == 'ให้เช่า':
                    list_type = 'rent'
                elif cut_find_contract_type == 'ขาย':
                    list_type = 'sell'
                if find_property_type == 'คอนโด':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a0f')
                elif find_property_type == 'บ้าน':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a11')
                elif find_property_type == 'ทาวน์เฮ้าส์/ทาวน์โฮม':
                    propertyType = ObjectId('62bd7104f0521b8890fe9a15')
                post_number = 'lvisd_'+post_id

                raw_data_home = {
                    'cut_find_web': cut_find_web,
                    'cut_find_url': townhouse_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': cut_find_name,
                    'listType': list_type,
                    'propertyTypes': propertyType,
                    'totalBedroom': cut_find_bedroom,
                    'totalToilet': cut_find_bathroom,
                    'startingPrice': cut_find_price,
                    'finalPrice': cut_find_price,
                    'minimumArea': cut_find_size,
                    'maximumArea': cut_find_size,
                    'description': find_description,
                    'cut_find_location': cut_find_location,
                    'cut_find_address': cut_find_address,
                    'cut_find_contract_type': cut_find_contract_type,
                    'cut_find_floor': cut_find_floor,
                    'cut_find_map': cut_find_map,
                    'date': datetime.datetime.utcnow(),
                }

                raw_data_homeu = {
                    'cut_find_web': cut_find_web,
                    'galleryUrls': img_path,
                    'name': cut_find_name,
                    'listType': list_type,
                    'propertyTypes': propertyType,
                    'totalBedroom': cut_find_bedroom,
                    'totalToilet': cut_find_bathroom,
                    'startingPrice': cut_find_price,
                    'finalPrice': cut_find_price,
                    'minimumArea': cut_find_size,
                    'maximumArea': cut_find_size,
                    'description': find_description,
                    'cut_find_location': cut_find_location,
                    'cut_find_address': cut_find_address,
                    'cut_find_contract_type': cut_find_contract_type,
                    'cut_find_floor': cut_find_floor,
                    'cut_find_map': cut_find_map,
                    'date': datetime.datetime.utcnow(),
                }

                def update_data_mongo(post_number ,raw_data_homeu):
                    myquery = {"post_no": post_number}
                    mydoc = mycol_lvisd.find(myquery)

                    update_data = {"$set":raw_data_homeu}

                    mycol_lvisd.update_one(myquery,update_data)

                try:
                    print('try before insert')
                    try:
                        insert_data_mongo(raw_data_home)
                    except Exception as e:
                        print('insert error:',e)
                        insert_data_mongo(raw_data_home)
                    print('try after insert')
                except:
                    print('try before update')
                    update_data_mongo(post_number ,raw_data_homeu)
                    print('try after update')



        ########################################################################################
                # if condo_list_url[i] not in {"cut_find_url": condo_list_url} :
                #     insert_data_mongo(raw_data_home)

                # else :
                #     update_data_mongo(raw_data_home)
        ########################################################################################

                print(townhouse_list_url[i])
                
                # print(i+1,condo_list_url[i],cut_find_name,cut_find_location,cut_find_address,cut_find_price,details)
                # print(i+1,cut_find_name,cut_find_location,cut_find_address,cut_find_price,details)


            else:
                print("222222222222222222222222222222")
                print('error',i,condo_list_url[i])

            i += 1

        except Exception as e:
            print('1039',e)
            raw_data_home = {
                'cannot_open_url': townhouse_data
            }
            # insert_fails(raw_data_home)

def scrapelvisd():
    get_condo_data()
    print('finish condo')
    get_house_data()
    print('finish house')
    get_land_data()
    print('finish land')
    get_townhouse_data()
    print('finish th')
    print('finish')

scrapelvisd()
schedule.every(30).days.do(scrapelvisd)

while True:
    schedule.run_pending()
    time.sleep(1)
