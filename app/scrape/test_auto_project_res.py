from re import T
import requests
from bs4 import BeautifulSoup
# import csv
# import xlsxwriter
# import itertools
# import pymysql as m
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
import requests
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo
import undetected_chromedriver as uc
from bson.objectid import ObjectId
import datetime

class scraperes:
    def __init__(self):
        self.mode = 0o666
        self.ch_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_residences"
        self.sys_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_residences/imgs_{}"
        self.img_xpath = '//*[@id="galleria"]/div/div[2]/div[2]/div/div[{}]/img'

        self.hpp_url = "https://www.residences.in.th{}"
        self.ap_url = "https://www.residences.in.th/%E0%B8%AD%E0%B8%9E%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%97%E0%B9%80%E0%B8%A1%E0%B9%89%E0%B8%99%E0%B8%97%E0%B9%8C-%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%9E%E0%B8%B1%E0%B8%81-%E0%B8%AB%E0%B8%AD%E0%B8%9E%E0%B8%B1%E0%B8%81/{}"
        self.cp_url = "https://www.residences.in.th/%E0%B8%AD%E0%B8%9E%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%97%E0%B9%80%E0%B8%A1%E0%B9%89%E0%B8%99%E0%B8%97%E0%B9%8C-%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%9E%E0%B8%B1%E0%B8%81-%E0%B8%AB%E0%B8%AD%E0%B8%9E%E0%B8%B1%E0%B8%81/{}/%E0%B8%84%E0%B8%AD%E0%B8%99%E0%B9%82%E0%B8%94%E0%B8%A1%E0%B8%B4%E0%B9%80%E0%B8%99%E0%B8%B5%E0%B8%A2%E0%B8%A1"

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        # options.headless = True
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized")
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        # options.add_argument(f'user-agent={user_agent}')
        # global wd
        # self.wd = uc.Chrome(use_subprocess=True, service=Service(ChromeDriverManager().install()), options = options)
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)
        print(self.wd)
        self.wait = WebDriverWait(self.wd, 60)

        # ##Start of db zone###
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-92a15e6e.mongo.ondigitalocean.com:27017/devXInvester?replicaSet=realty-plenty-db&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        
        mydb = myclient["devXInvester"]
        # self.mycol = mydb["watchLists_residences"]
        self.mycol = mydb["watchLists_res"]
        self.mycol_c = mydb["watchLists_cresidences"]
        # self.mycol_c = mydb["watchLists_cres"]

        self.provinces = ['กรุงเทพมหานคร', 'นนทบุรี']
        # self.provinces = ['กรุงเทพมหานคร', 'นนทบุรี', 'สมุทรปราการ', 'ปทุมธานี', 'สมุทรสาคร', 'สมุทรสงคราม', 'นครปฐม', 'เชียงใหม่', 'พิษณุโลก', 'ตาก', 'ลำปาง', 'อุตรดิตถ์', 'แพร่', 'น่าน', 'พะเยา', 'เชียงราย', 'แม่ฮ่องสอน', 'กำแพงเพชร', 'สุโขทัย', 'พิจิตร', 'เพชรบูรณ์', 'ลำพูน', 'เพชรบุรี', 'ประจวบคีรีขันธ์', 'กาญจนบุรี', 'ราชบุรี', 'นครราชสีมา', 'อุบลราชธานี', 'ร้อยเอ็ด', 'กาฬสินธุ์', 'สกลนคร', 'มุกดาหาร', 'บุรีรัมย์', 'สุรินทร์', 'บึงกาฬ', 'ศรีสะเกษ', 'ยโสธร', 'ชัยภูมิ', 'อำนาจเจริญ', 'หนองบัวลำภู', 'ขอนแก่น', 'อุดรธานี', 'เลย', 'หนองคาย', 'มหาสารคาม', 'นครพนม', 'สุพรรณบุรี', 'นครสวรรค์', 'สระบุรี', 'ลพบุรี', 'อุทัยธานี', 'พระนครศรีอยุธยา', 'อ่างทอง', 'สิงห์บุรี', 'ชัยนาท', 'นครนายก', 'ชลบุรี', 'สระแก้ว', 'ระยอง', 'จันทบุรี', 'ตราด', 'ฉะเชิงเทรา', 'ปราจีนบุรี', 'นครศรีธรรมราช', 'กระบี่', 'ภูเก็ต', 'ยะลา', 'ระนอง', 'สงขลา', 'สตูล', 'ตรัง', 'พัทลุง', 'ปัตตานี', 'พังงา', 'สุราษฎร์ธานี', 'ชุมพร', 'นราธิวาส']
        self.a_urls = []
        self.c_urls = []
        self.a_post_urls = []
        self.c_post_urls = []
        self.condo_name_url =[]
        self.cannot_open_condo_name_url = []
        self.projects_name_th = []
        self.projects_name_en = []

    def insert_data_mongo(condo_project):

        mydict = condo_project
        
        # ##Start of db zone###
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        mydb = myclient["devXInvester"]
        # mycol = mydb["watchLists_residences"]
        mycol = mydb["watchLists_res"]
        
        print(condo_project)

        # x = mycol.insert_one(mydict)

    def insert_fails(self, raw_data_home):
        mydict = raw_data_home

        # self.mycol_c.insert_one(mydict)
        
    def chromedriver():
        # options = Options()
        # options.add_argument('--no-sandbox')
        # # options.add_argument("--headless")
        # options.headless = True
        # options.add_argument('--disable-dev-shm-usage')
        # options.add_argument("--remote-debugging-port=9230")  # this

        # options.add_argument("--disable-dev-shm-using") 
        # options.add_argument("--disable-extensions") 
        # options.add_argument("--disable-gpu") 
        # options.add_argument("start-maximized")
        # user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        # # options.add_argument(f'user-agent={user_agent}')
        # # global wd
        # self.wd = uc.Chrome(use_subprocess=True, service=Service(ChromeDriverManager().install()), options = options)
        # self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)
        print('')#,self.wd)
    # chromedriver()

    def get_urls(self):
        for p in self.provinces:
            self.a_urls.append(self.ap_url.format(p))
            self.c_urls.append(self.cp_url.format(p))

    def ap_urls(self):

        for a_url in self.a_urls:
            self.wd.get(a_url)
            time.sleep(1)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            print(soup)
            try:
                find_page = soup.find("ul",{"class":"pagination pagination"}).text.split(" ")#['text']
                print('find_page',find_page)
                last_page = find_page[-2]
                int_last_page = int(last_page)
                print(int_last_page)
            except:
                print('ex 139')
                int_last_page = 1
            crp = self.wd.current_url
            # for cpage in range(int_last_page):
            for cpage in range(2):
                print('page',cpage+1)
                # crp = self.wd.current_url+'?page='+str(cpage+1)
                print(crp+'?page='+str(cpage+1))
                self.wd.get(crp+'?page='+str(cpage+1))
                soup = BeautifulSoup(self.wd.page_source, 'html.parser')

                for post in soup.findAll("div",{'class':'residences-name'}):
                    print(post)
                    for a in post.findAll("a"):
                        ahref = a['href']
                        if ahref != 'javascript:void(0);':
                            self.a_post_urls.append(self.hpp_url.format(ahref))
                            print(self.hpp_url.format(ahref))

        print(self.a_post_urls)

    def cd_urls(self):

        for c_url in self.c_urls:
            self.wd.get(c_url)
            # time.sleep(10)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="master-navbar-brand"]')))
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            try:
                find_page = soup.find("ul",{"class":"pagination pagination"}).text.split(" ")#['text']
                last_page = find_page[-2]
                int_last_page = int(last_page)
                print(int_last_page)
            except:
                int_last_page = 1
            crp = self.wd.current_url
            for cpage in range(int_last_page):
                print('page',cpage+1)
                # crp = self.wd.current_url+'?page='+str(cpage+1)
                print(crp+'?page='+str(cpage+1))
                self.wd.get(crp+'?page='+str(cpage+1))
                soup = BeautifulSoup(self.wd.page_source, 'html.parser')

                for post in soup.findAll("div",{'class':'residences-name'}):
                    for a in post.findAll("a"):
                        ahref = a['href']
                        if ahref != 'javascript:void(0);':
                            self.c_post_urls.append(self.hpp_url.format(ahref))
                            print(self.hpp_url.format(ahref))
    # cd_urls()

    def get_a_post_url(self):
        for a_post_url in self.a_post_urls:
            details = []
            items = []
            img_path = []

            print(a_post_url)

            def one_page():

                try:
                    self.wd.get(a_post_url)
                    soup = BeautifulSoup(self.wd.page_source,'html.parser')
                    try:                    
                        find_h1 = soup.find("h1").text
                        print(find_h1)
                    except:
                            self.wd.get(a_post_url)
                            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                            find_h1 = soup.find('h1').text
                            print(find_h1)                    
                    try:
                    
                        find_url = a_post_url
                        find_web = 'residences'
                        find_name = soup.find("h1")
                        find_location = soup.find("p")
                        find_iframe = soup.find("iframe",{"id":"gmap_canvas"})["src"]
                        find_latlong = find_iframe.split('=')[1].split('&')[0]
                        lat = find_latlong.split(',')[0]
                        long = find_latlong.split(',')[1]
                            # find_address = soup.find_all("div",{"class":"property-value"})
                        find_price = soup.find("span",{"class":"price"})
                        find_description = soup.find("div",{"class":"description col-md-12"}).text
                        find_details = soup.find_all("tr")
                            # find_size = soup.find_all("div",{"class":"property-value"})
                        # print('----------------------------------------------------------------')
                        cut_find_name = find_name.text
                        cut_find_location = find_location.text
                        cut_find_price = find_price.text
                        cut_find_details = find_details[1].text.strip()

                        ##findmap
                        start_tab = self.wd.current_window_handle
                        self.wd.execute_script("window.open('');")
                        # self.wd.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 't')
                        self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                        self.wd.get('https://www.google.co.th/maps')
                        self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_name.text)
                        self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                        print('clicksearch')
                        time.sleep(3)
                        find_map = self.wd.current_url
                        print(find_map)
                        try:
                            latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                            lat = latlong[0]
                            long = latlong[1]
                        except:
                            lat = find_map
                            long = find_map
                        self.wd.close()
                        self.wd.switch_to.window(self.wd.window_handles[0])#เปลี่ยนแท็ป

                        for detail in find_details:

                            detail_datas = {
                                # detail.text.split('-')[0]:detail.text.split('-')[1]
                                detail.text.replace('\n','@|@')

                            }
                            details.append(detail_datas)

                        for item in details:
                            str_item = ','.join(item)
                            str_item1 = str_item.replace('@|@@|@@|@','#@#').replace('@|@@|@','#@#').replace('@|@','#@#')
                            print('----------------------------------------------------------------')
                            print(str_item1)
                            print('ประเภท : '+str_item1.split('#@#')[1])
                            print('รูปแบบห้อง : '+str_item1.split('#@#')[2])
                            print('ขนาด : '+str_item1.split('#@#')[3])
                            print('เช่ารายเดือน	: '+str_item1.split('#@#')[4])
                            print('เช่ารายวัน : '+str_item1.split('#@#')[5])

                            l_item ={
                                'ประเภท':str_item1.split('#@#')[1],
                                'รูปแบบห้อง':str_item1.split('#@#')[2],
                                'ขนาด':str_item1.split('#@#')[3],
                                'เช่ารายเดือน':str_item1.split('#@#')[4],
                                'เช่ารายวััน':str_item1.split('#@#')[5]
                            }

                            items.append(l_item)
                        
                        # no = soup.find('h3').text.split(': ')[1]
                        # save_path = self.sys_xpath.format(no)
                        # try:
                        #     os.mkdir(save_path, self.mode)
                        #     os.chdir(os.path.join(os.getcwd(), save_path))
                        # except:
                        #     os.chdir(os.path.join(os.getcwd(), save_path))

                        # try:
                        #     n_p = soup.find('span',{'class':'galleria-total'}).text
                        #     for c_p in range(int(n_p)):
                        #         # elems = WebDriverWait(self.wd,10).until(EC.presence_of_all_elements_located((By.XPATH, "//div[@class='galleria-image' and style='overflow: hidden; position: relative; visibility: visible; width: 62px; height: 42px;']")))
                        #         c_p += 1
                        #         elems = self.wd.find_element(By.XPATH, self.img_xpath.format(c_p)).get_attribute('src')
                        #         image_url = elems.replace('/thumbnail_','/')
                        #         file_name = str(c_p) + ".jpg"
                        #         try:
                        #             image_content = requests.get(image_url).content
                        #             image_file = io.BytesIO(image_content)
                        #             image = Image.open(image_file)
                        #             file_path = save_path +'/' + file_name

                        #             img_path.append(file_path)

                        #             with open(file_path, "wb") as f:
                        #                 image.save(f, "PNG")

                        #             print("Success")

                        #         except Exception as e:
                        #             print('FAILED -', e)
                        # except:
                        #     img_path.append('no img')

                        raw_data_home = {
                            'cut_find_web': find_web,
                            'cut_find_url': a_post_url,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'เช่า',
                            'propertyTypes': 'อพาร์ทเม้นท์',
                            'totalBedroom': 'no data',
                            'totalToilet': 'no data',
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'minimumArea': 'no data',
                            'minimumArea': 'no data',
                            'description': find_description,
                            'cut_find_location': cut_find_location,
                            'cut_find_map': find_map,
                            'latitude' : lat,
                            'longitude' : long,
                            'date': datetime.datetime.utcnow()
                        }                    
                        raw_data_homeu = {
                            'cut_find_web': find_web,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'เช่า',
                            'propertyTypes': 'อพาร์ทเม้นท์',
                            'totalBedroom': 'no data',
                            'totalToilet': 'no data',
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'minimumArea': 'no data',
                            'minimumArea': 'no data',
                            'description': find_description,
                            'cut_find_location': cut_find_location,
                            'cut_find_map': find_map,
                            'latitude' : lat,
                            'longitude' : long,
                            'date': datetime.datetime.utcnow()
                        }
                        def update_data_mongo(a_post_url ,raw_data_homeu):
                            myquery = {"cut_find_url": a_post_url}
                            mydoc = self.mycol.find(myquery)

                            update_data = {"$set":raw_data_homeu}

                            self.mycol.update_one(myquery,update_data)
                        print(raw_data_home)
                        try:
                            scraperes.insert_data_mongo(raw_data_home)
                        except:
                            update_data_mongo(a_post_url ,raw_data_homeu)
                    except:    
                        raw_data_home = {
                            'cut_find_web': 'residences',
                            'cannot_open_url': a_post_url,
                        }
                        print(raw_data_home)
                        scraperes.insert_fails(raw_data_home)
                except:
                    raw_data_home = {
                        'cut_find_web': 'residences',
                        'cannot_open_url': a_post_url,
                    }
                    print(raw_data_home)
                    scraperes.insert_fails(raw_data_home)
            one_page()

    def get_c_post_url(self):
        for c_post_url in self.c_post_urls:
            details = []
            items = []
            img_path = []

            print(c_post_url)

            def one_page():

                try:
                    self.wd.get(c_post_url)
                    soup = BeautifulSoup(self.wd.page_source,'html.parser')
                    try:                    
                        find_h1 = soup.find("h1").text
                        print(find_h1)
                    except:
                            self.wd.get(c_post_url)
                            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                            find_h1 = soup.find('h1').text
                            print(find_h1)                    
                    try:
                        
                        find_url = c_post_url
                        find_web = 'residences'
                        find_name = soup.find("h1")
                        find_location = soup.find("p")
                        find_iframe = soup.find("iframe",{"id":"gmap_canvas"})["src"]
                        find_latlong = find_iframe.split('=')[1].split('&')[0]
                        lat = find_latlong.split(',')[0]
                        long = find_latlong.split(',')[1]
                            # find_address = soup.find_all("div",{"class":"property-value"})
                        find_price = soup.find("span",{"class":"price"})
                        find_description = soup.find("div",{"class":"description col-md-12"}).text
                        find_details = soup.find_all("tr")
                            # find_size = soup.find_all("div",{"class":"property-value"})
                        # print('----------------------------------------------------------------')
                        cut_find_name = find_name.text
                        cut_find_location = find_location.text
                        cut_find_price = find_price.text
                        cut_find_details = find_details[1].text.strip()

                        ##findmap
                        start_tab = self.wd.current_window_handle
                        self.wd.execute_script("window.open('');")
                        self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                        self.wd.get('https://www.google.co.th/maps')
                        self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_name.text)
                        self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                        print('clicksearch')
                        time.sleep(3)
                        find_map = self.wd.current_url
                        print(find_map)
                        try:
                            latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                            lat = latlong[0]
                            long = latlong[1]
                        except:
                            lat = find_map
                            long = find_map
                        self.wd.close()
                        self.wd.switch_to.window(self.wd.window_handles[0])#เปลี่ยนแท็ป

                        for detail in find_details:

                            detail_datas = {
                                # detail.text.split('-')[0]:detail.text.split('-')[1]
                                detail.text.replace('\n','@|@')

                            }
                            details.append(detail_datas)

                        for item in details:
                            str_item = ','.join(item)
                            str_item1 = str_item.replace('@|@@|@@|@','#@#').replace('@|@@|@','#@#').replace('@|@','#@#')
                            print('----------------------------------------------------------------')
                            print(str_item1)
                            print('ประเภท : '+str_item1.split('#@#')[1])
                            print('รูปแบบห้อง : '+str_item1.split('#@#')[2])
                            print('ขนาด : '+str_item1.split('#@#')[3])
                            print('เช่ารายเดือน	: '+str_item1.split('#@#')[4])
                            print('เช่ารายวัน : '+str_item1.split('#@#')[5])

                            l_item ={
                                'ประเภท':str_item1.split('#@#')[1],
                                'รูปแบบห้อง':str_item1.split('#@#')[2],
                                'ขนาด':str_item1.split('#@#')[3],
                                'เช่ารายเดือน':str_item1.split('#@#')[4],
                                'เช่ารายวััน':str_item1.split('#@#')[5]
                            }

                            items.append(l_item)
                        
                        # no = soup.find('h3').text.split(': ')[1]
                        # save_path = self.sys_xpath.format(no)
                        # try:
                        #     os.mkdir(save_path, self.mode)
                        #     os.chdir(os.path.join(os.getcwd(), save_path))
                        # except:
                        #     os.chdir(os.path.join(os.getcwd(), save_path))

                        # try:
                        #     n_p = soup.find('span',{'class':'galleria-total'}).text
                        #     for c_p in range(int(n_p)):
                        #         # elems = WebDriverWait(self.wd,10).until(EC.presence_of_all_elements_located((By.XPATH, "//div[@class='galleria-image' and style='overflow: hidden; position: relative; visibility: visible; width: 62px; height: 42px;']")))
                        #         c_p += 1
                        #         elems = self.wd.find_element(By.XPATH, self.img_xpath.format(c_p)).get_attribute('src')
                        #         image_url = elems.replace('/thumbnail_','/')
                        #         file_name = str(c_p) + ".jpg"
                        #         try:
                        #             image_content = requests.get(image_url).content
                        #             image_file = io.BytesIO(image_content)
                        #             image = Image.open(image_file)
                        #             file_path = save_path +'/' + file_name

                        #             img_path.append(file_path)

                        #             with open(file_path, "wb") as f:
                        #                 image.save(f, "PNG")

                        #             print("Success")

                        #         except Exception as e:
                        #             print('FAILED -', e)
                        # except:
                        #     img_path.append('no img')

                        raw_data_home = {
                            'cut_find_web': find_web,
                            'cut_find_url': c_post_url,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'เช่า',
                            'propertyTypes': ObjectId('62bd7104f0521b8890fe9a0f'),
                            'totalBedroom': 'no data',
                            'totalToilet': 'no data',
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'minimumArea': 'no data',
                            'minimumArea': 'no data',
                            'description': find_description,
                            'cut_find_location': cut_find_location,
                            'cut_find_map': find_map,
                            'latitude' : lat,
                            'longitude' : long,
                            'date': datetime.datetime.utcnow()
                        }                    
                        raw_data_homeu = {
                            'cut_find_web': find_web,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'เช่า',
                            'propertyTypes': ObjectId('62bd7104f0521b8890fe9a0f'),
                            'totalBedroom': 'no data',
                            'totalToilet': 'no data',
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'minimumArea': 'no data',
                            'minimumArea': 'no data',
                            'description': find_description,
                            'cut_find_location': cut_find_location,
                            'cut_find_map': find_map,
                            'latitude' : lat,
                            'longitude' : long,
                            'date': datetime.datetime.utcnow()
                        }
                        def update_data_mongo(c_post_url ,raw_data_homeu):
                            myquery = {"cut_find_url": c_post_url}
                            mydoc = self.mycol.find(myquery)

                            update_data = {"$set":raw_data_homeu}

                            self.mycol.update_one(myquery,update_data)
                        print(raw_data_home)
                        try:
                            scraperes.insert_data_mongo(raw_data_home)
                        except:
                            update_data_mongo(c_post_url ,raw_data_homeu)
                    except:    
                        raw_data_home = {
                            'cut_find_web': 'residences',
                            'cannot_open_url': c_post_url,
                        }
                        print(raw_data_home)
                        scraperes.insert_fails(raw_data_home)
                except:
                    raw_data_home = {
                        'cut_find_web': 'residences',
                        'cannot_open_url': c_post_url,
                    }
                    print(raw_data_home)
                    scraperes.insert_fails(raw_data_home)
            one_page()

    def quit(self):
        self.wd.quit()
# try:
#     get_urls()
#     ap_urls()
#     cd_urls()
#     get_a_post_url()
#     # get_c_post_url()
#     quit()
#     print('finish')

# except Exception as e:
#     print('error:', e)
#     quit()
class startres:
    def startres(self):

        fres = scraperes()

        try:

            fres.get_urls()
            fres.ap_urls()
            # fres.cd_urls()
            fres.get_a_post_url()
            # fres.get_c_post_url()
            fres.quit()
            print('finish')
            return True

        except Exception as e:
            print("Error",e)
            fres.quit()
            return False
        
b = startres()

b.startres()