from re import T
import requests
from bs4 import BeautifulSoup
# import csv
# import xlsxwriter
# import itertools
# import pymysql as m
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
import requests
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo


class scrape_project_ppth:
    def __init__(self):
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        
        mydb = myclient["devXInvester"]
        self.mycol = mydb["propoty_project_urls"]

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)
        self.wait = WebDriverWait(self.wd, 60)

        self.p_xpath = '//*[text() = "{}"]'

        self.province = []
        self.province_url = []
        self.condo_zone_url = []
        self.condo_name_url =[]
        self.cannot_open_condo_name_url = []
        self.projects_name_th = []
        self.projects_name_en = []

    def insert_data_mongo(self, condo_project):

        mydict = condo_project

        x = self.mycol.insert_one(mydict)

    def click(self):
        link = self.wd.find_element(By.LINK_TEXT, "ถัดไป")
        link.click()
        print("Click Next")


    def get_province(self):
        hpp_url = "https://www.propertyhub.in.th/"

        self.wd.get(hpp_url)
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div/div[4]/button').click()
        time.sleep(5)

        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        for p_list in soup.find_all("li",{"class":"kna4ye-4 iHVBaZ"}):
            self.province.append(p_list.text)

        for p_list in soup.find_all("li",{"class":"kna4ye-3 iHNewQ"}):
            self.province.append(p_list.text)

    def get_condo_zone_url(self):
    #self.province
        for n_prv in range(len(self.province)):
        # for n_prv in range(1):
            self.wd.find_element(By.XPATH, self.p_xpath.format(self.province[n_prv])).click()
            # self.wd.find_element(By.XPATH, '//*[text() = "ปทุมธานี"]').click()
            # print(self.p_xpath.format(self.province[n_prv]))
            print('click'+self.province[n_prv])
            time.sleep(2)
            #get_condo_zone_url#50
            # zone_element = self.wait.until(EC.visibility_of((By.CLASS_NAME, "zoneTypeStyle")))
            for condo_zone in self.wd.find_elements(By.CLASS_NAME,'zoneTypeStyle'):
                self.condo_zone_url.append(condo_zone.get_attribute('href'))
            n_prv += 1

        print(len(self.condo_zone_url))

    def get_project_url(self):
        cpage = 1
        for open_condo_zone_url in range(len(self.condo_zone_url)):
            self.wd.get(self.condo_zone_url[open_condo_zone_url])
            print(self.condo_zone_url[open_condo_zone_url])
            time.sleep(5)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            header = soup.find('h1')
            try:
                n_post = soup.find('div',{'class':'m8nysy-0 eqlIve'}).text
                print(n_post)
                try:
                    find_page = soup.find_all("a",{"role":"button"})
                    last_page = find_page[-2].text
                    int_last_page = int(last_page)
                    for page in range(int_last_page):
                        for condo_name in self.wd.find_elements(By.XPATH, '//*[@class="col-xs sc-1qj7qf1-2 elaovb-2 jNTgvW"]/a'):
                            print('---------------------------------')
                            self.condo_name_url.append(condo_name.get_attribute('href'))
                        # for project_name in soup.find_all('p',{'class':'elaovb-3 izRiRZ'}):
                        #     print('++++++++++++++++++++++++++++++++++')
                        #     projects_name.append(project_name.text)
                        if cpage <= int_last_page-1:
                            scrape_project_ppth.click()

                        cpage += 1
                        time.sleep(5)
                        # print(self.condo_name_url)
                except:
                    print("1 page")
                    for condo_name in self.wd.find_elements(By.XPATH, '//*[@class="col-xs sc-1qj7qf1-2 elaovb-2 jNTgvW"]/a'):
                        print('---------------------------------')
                        self.condo_name_url.append(condo_name.get_attribute('href'))
                        time.sleep(5)
                        # print(self.condo_name_url)
            except:
                print("No project")

        for n_pj in range(len(self.condo_name_url)):
            # if cpage <= len(self.condo_name_url)-1:
            # self.wd.get(self.condo_name_url[n_pj])
            # time.sleep(5)
            #try
            # print(self.condo_name_url[n_pj])
            try:
                web_data = requests.get(self.condo_name_url[n_pj])
                time.sleep(2)
                print('requests:'+self.condo_name_url[n_pj])
                soup_project = BeautifulSoup(web_data.text, 'html.parser')
                try:
                    en_name = soup_project.find("h1").text
                except:
                    en_name = 'No en_name'
                # print(en_name)
                try:
                    th_name = soup_project.find("span",{"class":"sc-6ejxki-8 fxyPXk"}).text
                except:
                    th_name = 'No th_name'
                try:
                    address = soup_project.find("td",{"class":"sc-eyfspo-16 llUiHq"}).text
                except:
                    address = 'No address'
                print(address)
                try:
                    city = address.split(' ')[-2]
                except:
                    city = 'No city'
                print(city)
                try:
                    state = address.split(' ')[-1]
                except:
                    state = 'No state'
                print(state)
                
                ppth_tab = self.wd.current_window_handle
                self.wd.execute_script("window.open()")
                self.wd.switch_to.window(self.wd.window_handles[1])
                self.wd.get('https://www.google.com/maps')
                self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(th_name)
                self.wd.find_element(By.XPATH,'//*[@id="searchbox-searchbutton"]').click()
                time.sleep(3)
                map_url = self.wd.current_url
                print(map_url)
                latlong = map_url.split('@')[1].split('/')[0].split(',')[:2]
                lat = latlong[0]
                long = latlong[1]
                self.wd.close()
                self.wd.switch_to.window(self.wd.window_handles[0])

                self.projects_name_th.append(th_name)
                self.projects_name_en.append(en_name)

            except:
                print('cannot requests:'+self.condo_name_url[n_pj])
                self.cannot_open_condo_name_url.append(self.condo_name_url[n_pj])


            condo_project = {
            'self.condo_name_url': self.condo_name_url[n_pj],
            'nameTH' : th_name,
            'nameEN' : en_name,
            'address' : address,
            'city' : city,
            'state' : state,
            'latitude' : lat,
            'longitude' : long,
            'self.cannot_open_condo_name_url' : self.cannot_open_condo_name_url
            }
            # print(condo_project)
            scrape_project_ppth.insert_data_mongo(condo_project)
            time.sleep(1)

            n_pj += 1

        open_condo_zone_url += 1

    # print(self.projects_name_th)
    # print(self.projects_name_en)
    # insert_data_mongo(self.projects_name_th)
    # insert_data_mongo(self.projects_name_en)

    # print(condo_project)
    def quit(self):
        self.wd.quit()
        
    get_province()
    get_condo_zone_url()
    get_project_url()
    quit()
    print('finish')

