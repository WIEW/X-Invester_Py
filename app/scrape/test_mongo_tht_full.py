from re import T
import requests
from bs4 import BeautifulSoup
from email.mime import image
# import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import requests
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo
from dotenv import load_dotenv
from collections import ChainMap
from bson.objectid import ObjectId
import datetime

class scrapetht:
    def __init__(self):
        self.mode = 0o666
        self.ch_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_tht"
        self.sys_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_tht/imgs_{}"

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        options.add_argument(f'user-agent={user_agent}')
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)

        self.wait = WebDriverWait(self.wd, 60)


        # ##Start of db zone###
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")

        mydb = myclient["devXInvester"]
        # self.mycol = mydb["watchLists_tht"]
        # self.mycol_c = mydb["watchLists_ctht"]
        self.mycol = mydb["watchLists"]
        self.mycol_c = mydb["watchLists"]

        # mydb = myclient["devXInvester"]
        # self.mycol = mydb["propoty_project_urls"]

        self.cd_page_url = []
        self.hs_page_url = []
        self.ld_page_url = []
        self.th_page_url = []
        self.sh_page_url = []
        self.rt_page_url = []
        self.of_page_url = []
        self.ho_page_url = []
        self.fw_page_url = []
        self.sr_page_url = []
        self.bs_page_url = []
        self.condo_list_url = []
        self.house_list_url = []
        self.land_list_url = []
        self.townhouse_list_url = []
        self.shophouse_list_url = []
        self.retail_list_url = []
        self.office_list_url = []
        self.homeoffice_list_url = []
        self.fw_list_url = []
        self.sr_list_url = []
        self.bs_list_url = []
        
    def insert_data_mongo(self, raw_data_home):
        print('insert_data_mongo',raw_data_home)

        mydict = raw_data_home

        x = self.mycol.insert_one(mydict)

    def query_data_mongo(self, condo_data, house_data ):
        myquery = {"cut_find_url": condo_data}
        mydoc = self.mycol.find(myquery)

    def update_data_mongo(self, post_number, raw_data_homeu):
        myquery = {"post_no": post_number}
        mydoc = self.mycol.find(myquery)

        update_data = {"$set":raw_data_homeu}

        self.mycol.update_one(myquery,update_data)
    
    def insert_fails(self, raw_data_home):
        mydict = raw_data_home
        print(mydict)
        # x = self.mycol_c.insert_one(mydict)

##End of db zone###

    def get_condo_data(self):
        i = 0

        self.wd.get('https://www.thaihometown.com/%E0%B8%84%E0%B8%AD%E0%B8%99%E0%B9%82%E0%B8%94.html')
        web_data = requests.get('https://www.thaihometown.com/%E0%B8%84%E0%B8%AD%E0%B8%99%E0%B9%82%E0%B8%94.html')
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        try:
            find_condo_lists = soup.find_all('a', {'class' : 'namelink_pre'})#['href']
            for find_condo_list in find_condo_lists:
                self.condo_list_url.append(find_condo_list['href'])
        except:
            print('no namelink_pre')

        try:    
            find_condo_lists = soup.find_all('a', {'class' : 'namelink_recom'})#['href']
            for find_condo_list in find_condo_lists:
                self.condo_list_url.append(find_condo_list['href'])
        except:
            print('no namelink_recom')

        cd_p_url = 'https://www.thaihometown.com/condo/?page=2'
        self.wd.get(cd_p_url)
        web_data = requests.get(cd_p_url)
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        find_page = soup.find("span",{"class":"f3"}).text
        last_page = find_page.split('/')[-1]
        cd_int_last_page = int(last_page)
        # cd_int_last_page = int(3)
        print('cd_int_last_page',cd_int_last_page)

        for page in range(cd_int_last_page):
            page += 1
            cd_url = "https://www.thaihometown.com/condo/?page="+str(page)
            self.wd.get(cd_url)
            web_data = requests.get(cd_url)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            self.cd_page_url.append(cd_url)
            find_condo_lists = soup.find_all('a', {'class' : 'namelink'})#['href']
            for find_condo_list in find_condo_lists:
                self.condo_list_url.append(find_condo_list['href'])

        print(self.cd_page_url)

        ct_condo_list_url = len(self.condo_list_url)
        print(self.condo_list_url)
        # print('self.condo_list_url')
        print(ct_condo_list_url)

        for condo_data in self.condo_list_url:

            try:

                details = []

                self.wd.get(condo_data)

                print(condo_data)

                # web_data = requests.get(self.wd.page_source)
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="Mdetail"]/div/h1')))
                soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                find_url = condo_data
                find_web = 'thaihometown'
                find_name = soup.find("h1").text
                
                find_price = soup.find("a", {"class": "linkprice"}).text
                if 'ขาย' in find_price:
                    find_list_type = 'sell'
                elif 'เช่า' in find_price:
                    find_list_type = 'rent'
                find_linkcitys = soup.find_all('a',{'class':'linkcity'})
                find_size = find_linkcitys[0].text
                find_province = find_linkcitys[-2].text
                find_district = find_linkcitys[-1].text
                
                if find_linkcitys[1].text != find_linkcitys[-2].text:
                    max_area = find_linkcitys[1].text
                else:
                    max_area = find_size
                
                
                find_description = soup.find('div',{'class':'headtitle2'}).text
                find_table_details = soup.find_all('td',{'class':'table_set3'})
                find_table_header = soup.find_all('td',{'class':'Settable1'})
                try:
                    for n_fth in range(len(find_table_header)):
                        if 'จำนวนห้อง' in find_table_header[n_fth].text : #totalBedroom#totaltoilet
                            print('จำนวนห้อง',find_table_header[n_fth].text)
                            if 'สตูดิโอ' in find_table_details[n_fth].text:
                                find_bedroom = 'ห้องสตูดิโอ'
                                print('find_bedroom_if',n_fth,find_bedroom)
                                print('find_bathroom_if',n_fth,find_bathroom)
                            else:
                                try:
                                    find_bedroom = find_table_details[n_fth].text.split(' ห้องนอน')[0]
                                    print('find_bedroom1',n_fth,find_bedroom)
                                except:
                                    find_bedroom = find_table_details[n_fth].text
                                    print('find_bedroom2',n_fth,find_bedroom)
                                try:
                                    find_bathroom = find_table_details[n_fth].text.split('ห้องนอน ')[1].split('ห้องน้ำ')[0]
                                    print('find_bathroom1',n_fth,find_bathroom)
                                except:
                                    find_bathroom = find_table_details[n_fth].text
                                    print('find_bathroom2',n_fth,find_bathroom)


                except:
                    find_bedroom = ' ห้องนอน'
                    find_bathroom = ' ห้องน้ำ'
                    find_size = 'พื้นที่'
                    find_province = 'จังหวัด'
                    find_district = 'อำเภอ'
                try :
                    find_location = find_district+' '+find_province
                except :
                    find_location = 'ไม่มีข้อมูล'
                    
                ##findmap
                start_tab = self.wd.current_window_handle
                self.wd.execute_script("window.open('');")
                self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                self.wd.get('https://www.google.co.th/maps')
                self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_name)
                self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                print('clicksearch')
                time.sleep(3)
                find_map = self.wd.current_url
                print(find_map)
                try:
                    latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                    lat = latlong[0]
                    long = latlong[1]
                except:
                    lat = find_map
                    long = find_map
                self.wd.close()
                self.wd.switch_to.window(self.wd.window_handles[0])#เปลี่ยนแท็ป
                
                img_path = []

                p_id = condo_data.split('/')[4]
                save_path = self.sys_xpath.format(p_id)
                print(save_path)
                try:
                    os.mkdir(save_path, self.mode)
                    os.chdir(os.path.join(os.getcwd(), save_path))
                except:
                    os.chdir(os.path.join(os.getcwd(), save_path))

                try:
                    nc_p = 1
                    for c_p in soup.find_all('img',{'class':'style_img'}):
                        image_url = c_p['src'].replace('_small','')
                        file_name = str(nc_p) + ".jpg"
                        try:
                            image_content = requests.get(image_url).content
                            image_file = io.BytesIO(image_content)
                            image = Image.open(image_file)
                            file_path = save_path +'/' + file_name

                            img_path.append(file_path)

                            with open(file_path, "wb") as f:
                                image.save(f, "PNG")

                            print("Success")

                        except Exception as e:
                            print('FAILED -', e)

                        nc_p += 1 
                except:
                    img_path.append('no picture')

                post_number = 'tht_c_'+p_id

                raw_data_home = {
                    'cut_find_web': find_web,
                    'cut_find_url': condo_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': find_name,
                    'listType': find_list_type,
                    'propertyTypes': ObjectId('62bd7104f0521b8890fe9a0f'),
                    'totalBedroom': find_bedroom,
                    'totalToilet': find_bathroom,
                    'startingPrice': find_price,
                    'finalPrice': find_price,
                    'minimumArea': find_size,
                    'maximumArea': max_area,
                    'description': find_description,
                    'cut_find_location': find_location,
                    'cut_find_map': find_map,
                    'latitude': lat,
                    'longitude': long,
                    'date': datetime.datetime.utcnow(),
                }
                # print(raw_data_home)
                raw_data_homeu = {
                    'cut_find_web': find_url,
                    'cut_find_url': condo_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': find_name,
                    'listType': find_list_type,
                    'propertyTypes': ObjectId('62bd7104f0521b8890fe9a0f'),
                    'totalBedroom': find_bedroom,
                    'totalToilet': find_bathroom,
                    'startingPrice': find_price,
                    'finalPrice': find_price,
                    'minimumArea': find_size,
                    'maximumArea': max_area,
                    'description': find_description,
                    'cut_find_location': find_location,
                    'cut_find_map': find_map,
                    'latitude': lat,
                    'longitude': long,
                    'date': datetime.datetime.utcnow(),
                }

                # def update_data_mongo(post_number ,raw_data_homeu):
                #     myquery = {"post_no": post_number}
                #     mydoc = self.mycol.find(myquery)

                #     update_data = {"$set":raw_data_homeu}

                #     self.mycol.update_one(myquery,update_data)

                try:
                    print('try before insert')
                    try:
                        print('try try before insert')
                        scrapetht.insert_data_mongo(self, raw_data_home)
                        print('try af before insert')
                    except Exception as e:
                        print('insert error:',e)
                        scrapetht.insert_data_mongo(raw_data_home)
                    print('try after insert')
                except:
                    print('try before update', condo_data)
                    scrapetht.update_data_mongo(self, post_number ,raw_data_homeu)
                    print('try after update', condo_data)

                print('finish :', condo_data)
                print('finish i =', i)

                i += 1

            except Exception as e:
                print(condo_data, e)
                raw_data_home = {
                    'cannot_open_url': condo_data
                }
                print('cannot_open_url')
                scrapetht.insert_fails(self, raw_data_home)

    def get_house_data(self):
        i = 0

        self.wd.get('https://www.thaihometown.com/%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99.html')
        web_data = requests.get('https://www.thaihometown.com/%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99.html')
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        try:
            find_house_lists = soup.find_all('a', {'class' : 'namelink_pre'})#['href']
            for find_house_list in find_house_lists:
                self.house_list_url.append(find_house_list['href'])
        except:
            print('no namelink_pre')

        try:    
            find_house_lists = soup.find_all('a', {'class' : 'namelink_recom'})#['href']
            for find_house_list in find_house_lists:
                self.house_list_url.append(find_house_list['href'])
        except:
            print('no namelink_recom')

        hs_p_url = 'https://www.thaihometown.com/home/?page=2'
        web_data = requests.get(hs_p_url)
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        find_page = soup.find("span",{"class":"f3"}).text
        last_page = find_page.split('/')[-1]
        house_int_last_page = int(last_page)
        # house_int_last_page = int(3)
        print(house_int_last_page)

        for page in range(house_int_last_page):
            page += 1    
            hs_url = "https://www.thaihometown.com/home/?page="+str(page)
            self.wd.get(hs_url)
            web_data = requests.get(hs_url)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            self.hs_page_url.append(hs_url)
            find_house_lists = soup.find_all('a', {'class' : 'namelink'})#['href']
            for find_house_list in find_house_lists:
                self.house_list_url.append(find_house_list['href'])

        print(self.cd_page_url)
        print(house_int_last_page)

        ct_house_list_url = len(self.house_list_url)
        print(self.house_list_url)
        # print('self.condo_list_url')
        print(ct_house_list_url)


    ############################################################################################
        i = 0

        self.wd.get('https://www.thaihometown.com/%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99%E0%B9%80%E0%B8%94%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7.html')
        web_data = requests.get('https://www.thaihometown.com/%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99%E0%B9%80%E0%B8%94%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7.html')
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        try:
            find_house_lists = soup.find_all('a', {'class' : 'namelink_pre'})#['href']
            for find_house_list in find_house_lists:
                self.house_list_url.append(find_house_list['href'])
        except:
            print('no namelink_pre')

        try:    
            find_house_lists = soup.find_all('a', {'class' : 'namelink_recom'})#['href']
            for find_house_list in find_house_lists:
                self.house_list_url.append(find_house_list['href'])
        except:
            print('no namelink_recom')

        hs_p_url = 'https://www.thaihometown.com/singlehouse/?page=2'
        web_data = requests.get(hs_p_url)
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        find_page = soup.find("span",{"class":"f3"}).text
        last_page = find_page.split('/')[-1]
        house_int_last_page = int(last_page)
        # house_int_last_page = int(3)
        print(house_int_last_page)

        for page in range(house_int_last_page):
            page += 1
            hs_url = "https://www.thaihometown.com/home/?page="+str(page)
            self.wd.get(hs_url)
            web_data = requests.get(hs_url)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            self.hs_page_url.append(hs_url)
            find_house_lists = soup.find_all('a', {'class' : 'namelink'})#['href']
            for find_house_list in find_house_lists:
                self.house_list_url.append(find_house_list['href'])

        print(self.cd_page_url)
        print(house_int_last_page)

        ct_house_list_url = len(self.house_list_url)
        print(self.house_list_url)
        # print('self.condo_list_url')
        print(ct_house_list_url)

        for house_data in self.house_list_url:

            try:

                details = []

                self.wd.get(house_data)

                # web_data = requests.get(self.wd.page_source)
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="Mdetail"]/div/h1')))
                soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                find_url = house_data
                find_web = 'thaihometown'
                find_name = soup.find("h1").text
                
                find_price = soup.find("a", {"class": "linkprice"}).text
                if 'ขาย' in find_price:
                    find_list_type = 'sell'
                elif 'เช่า' in find_price:
                    find_list_type = 'rent'
                find_linkcitys = soup.find_all('a',{'class':'linkcity'})
                find_size = find_linkcitys[0].text
                find_province = find_linkcitys[-2].text
                find_district = find_linkcitys[-1].text
                
                if find_linkcitys[1].text != find_linkcitys[-2].text:
                    max_area = find_linkcitys[1].text
                else:
                    max_area = find_size
                
                
                find_description = soup.find('div',{'class':'headtitle2'}).text
                find_table_details = soup.find_all('td',{'class':'table_set3'})
                find_table_header = soup.find_all('td',{'class':'Settable1'})
                try:
                    for n_fth in range(len(find_table_header)):
                        if 'จำนวนห้อง' in find_table_header[n_fth].text : #totalBedroom#totaltoilet
                            print('จำนวนห้อง',find_table_header[n_fth].text)
                            try:
                                find_bedroom = find_table_details[n_fth].text.split(' ห้องนอน')[0]
                                print('find_bedroom1',n_fth,find_bedroom)
                            except:
                                find_bedroom = find_table_details[n_fth].text
                                print('find_bedroom2',n_fth,find_bedroom)
                            try:
                                find_bathroom = find_table_details[n_fth].text.split('ห้องนอน ')[1].split('ห้องน้ำ')[0]
                                print('find_bathroom1',n_fth,find_bathroom)
                            except:
                                find_bathroom = find_table_details[n_fth].text
                                print('find_bathroom2',n_fth,find_bathroom)


                except:
                    find_bedroom = ' ห้องนอน'
                    find_bathroom = ' ห้องน้ำ'
                    find_size = 'พื้นที่'
                    find_province = 'จังหวัด'
                    find_district = 'อำเภอ'
                try :
                    find_location = find_district+' '+find_province
                except :
                    find_location = 'ไม่มีข้อมูล'
                    
                ##findmap
                start_tab = self.wd.current_window_handle
                self.wd.execute_script("window.open('');")
                self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                self.wd.get('https://www.google.co.th/maps')
                self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_name)
                self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                print('clicksearch')
                time.sleep(3)
                find_map = self.wd.current_url
                print(find_map)
                try:
                    latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                    lat = latlong[0]
                    long = latlong[1]
                except:
                    lat = find_map
                    long = find_map
                self.wd.close()
                self.wd.switch_to.window(self.wd.window_handles[0])#เปลี่ยนแท็ป
                
                img_path = []

                p_id = house_data.split('/')[4]
                save_path = self.sys_xpath.format(p_id)
                print(save_path)
                try:
                    os.mkdir(save_path, self.mode)
                    os.chdir(os.path.join(os.getcwd(), save_path))
                except:
                    os.chdir(os.path.join(os.getcwd(), save_path))

                try:
                    nc_p = 1
                    for c_p in soup.find_all('img',{'class':'style_img'}):
                        image_url = c_p['src'].replace('_small','')
                        file_name = str(nc_p) + ".jpg"
                        try:
                            image_content = requests.get(image_url).content
                            image_file = io.BytesIO(image_content)
                            image = Image.open(image_file)
                            file_path = save_path +'/' + file_name

                            img_path.append(file_path)

                            with open(file_path, "wb") as f:
                                image.save(f, "PNG")

                            print("Success")

                        except Exception as e:
                            print('FAILED -', e)

                        nc_p += 1 
                except:
                    img_path.append('no picture')

                post_number = 'tht_h_'+p_id

                raw_data_home = {
                    'cut_find_web': find_web,
                    'cut_find_url': house_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': find_name,
                    'listType': find_list_type,
                    'propertyTypes': ObjectId('62bd7104f0521b8890fe9a11'),
                    'totalBedroom': find_bedroom,
                    'totalToilet': find_bathroom,
                    'startingPrice': find_price,
                    'finalPrice': find_price,
                    'minimumArea': find_size,
                    'maximumArea': max_area,
                    'description': find_description,
                    'cut_find_location': find_location,
                    'cut_find_map': find_map,
                    'latitude': lat,
                    'longitude': long,
                    'date': datetime.datetime.utcnow(),
                }
                raw_data_homeu = {
                    'cut_find_web': find_web,
                    'cut_find_url': house_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': find_name,
                    'listType': find_list_type,
                    'propertyTypes': ObjectId('62bd7104f0521b8890fe9a11'),
                    'totalBedroom': find_bedroom,
                    'totalToilet': find_bathroom,
                    'startingPrice': find_price,
                    'finalPrice': find_price,
                    'minimumArea': find_size,
                    'maximumArea': find_size,
                    'description': find_description,
                    'cut_find_location': find_location,
                    'cut_find_map': find_map,
                    'latitude': lat,
                    'longitude': long,
                    'date': datetime.datetime.utcnow(),
                    }

                # def update_data_mongo(post_number, raw_data_homeu):
                #     myquery = {"post_no": post_number}
                #     mydoc = self.mycol.find(myquery)

                #     update_data = {"$set":raw_data_homeu}

                #     self.mycol.update_one(myquery,update_data)

                try:
                    print('try before insert')
                    try:
                        scrapetht.insert_data_mongo(self, raw_data_home)
                    except Exception as e:
                        print('insert error:',e)
                        scrapetht.insert_data_mongo(self, raw_data_home)
                    print('try after insert')
                except:
                    print('try before update', house_data)
                    scrapetht.update_data_mongo(self, post_number ,raw_data_homeu)
                    print('try after update', house_data)

                print(house_data)

                i += 1
            
            except Exception as e:
                print(house_data, e)
                raw_data_home = {
                    'cannot_open_url': house_data
                }
                scrapetht.insert_fails(self, raw_data_home)


    def get_land_data(self):
        i = 0
        
        self.wd.get('https://www.thaihometown.com/%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%94%E0%B8%B4%E0%B8%99.html')
        web_data = requests.get('https://www.thaihometown.com/%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%94%E0%B8%B4%E0%B8%99.html')
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        try:
            find_land_lists = soup.find_all('a', {'class' : 'namelink_pre'})#['href']
            for find_land_list in find_land_lists:
                self.land_list_url.append(find_land_list['href'])
        except:
            print('no namelink_pre')

        try:    
            find_land_lists = soup.find_all('a', {'class' : 'namelink_recom'})#['href']
            for find_land_list in find_land_lists:
                self.land_list_url.append(find_land_list['href'])
        except:
            print('no namelink_recom')

        ld_p_url = 'https://www.thaihometown.com/land/?page=2'
        web_data = requests.get(ld_p_url)
        soup = BeautifulSoup(web_data.text, 'html.parser')
        find_page = soup.find("span",{"class":"f3"}).text
        last_page = find_page.split('/')[-1]
        print(last_page)
        land_int_last_page = int(last_page)
        # land_int_last_page = int(3)
        print(land_int_last_page)

        for page in range(land_int_last_page):
            page += 1

            ld_url = "https://www.thaihometown.com/land/?page="+str(page)
            self.wd.get(ld_url)
            web_data = requests.get(ld_url)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            self.ld_page_url.append(ld_url)
            find_land_lists = soup.find_all('a', {'class' : 'namelink'})#['href']
            for find_land_list in find_land_lists:
                self.land_list_url.append(find_land_list['href'])
                
        print(self.ld_page_url)

        ct_land_list_url = len(self.land_list_url)
        print(self.land_list_url)
        # print('self.condo_list_url')
        print(ct_land_list_url)

        for land_data in self.land_list_url:

            try:

                details = []

                self.wd.get(land_data)

                # web_data = requests.get(land_data)
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="Mdetail"]/div/h1')))
                soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                find_url = land_data
                find_web = 'thaihometown'
                find_name = soup.find("h1").text
                
                find_price = soup.find("a", {"class": "linkprice"}).text
                if 'ขาย' in find_price:
                    find_list_type = 'sell'
                elif 'เช่า' in find_price:
                    find_list_type = 'rent'

                
                if find_linkcitys[1].text != find_linkcitys[-2].text:
                    max_area = find_linkcitys[1].text
                else:
                    max_area = find_size
                
                
                find_description = soup.find('div',{'class':'headtitle2'}).text
                find_table_details = soup.find_all('td',{'class':'table_set3'})
                find_table_header = soup.find_all('td',{'class':'Settable1'})
                try:
                    find_linkcitys = soup.find_all('a',{'class':'linkcity'})
                    find_size = find_linkcitys[0].text
                    find_province = find_linkcitys[-2].text
                    find_district = find_linkcitys[-1].text

                except:

                    find_size = 'พื้นที่'
                    find_province = 'จังหวัด'
                    find_district = 'อำเภอ'
                try :
                    find_location = find_district+' '+find_province
                except :
                    find_location = 'ไม่มีข้อมูล'
                    
                ##findmap
                start_tab = self.wd.current_window_handle
                self.wd.execute_script("window.open('');")
                self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                self.wd.get('https://www.google.co.th/maps')
                self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_name)
                self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                print('clicksearch')
                time.sleep(3)
                find_map = self.wd.current_url
                print(find_map)
                try:
                    latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                    lat = latlong[0]
                    long = latlong[1]
                except:
                    lat = find_map
                    long = find_map
                self.wd.close()
                self.wd.switch_to.window(self.wd.window_handles[0])#เปลี่ยนแท็ป
                
                img_path = []

                p_id = land_data.split('/')[4]
                save_path = self.sys_xpath.format(p_id)
                print(save_path)
                try:
                    os.mkdir(save_path, self.mode)
                    os.chdir(os.path.join(os.getcwd(), save_path))
                except:
                    os.chdir(os.path.join(os.getcwd(), save_path))

                try:
                    nc_p = 1
                    for c_p in soup.find_all('img',{'class':'style_img'}):
                        image_url = c_p['src'].replace('_small','')
                        file_name = str(nc_p) + ".jpg"
                        try:
                            image_content = requests.get(image_url).content
                            image_file = io.BytesIO(image_content)
                            image = Image.open(image_file)
                            file_path = save_path +'/' + file_name

                            img_path.append(file_path)

                            with open(file_path, "wb") as f:
                                image.save(f, "PNG")

                            print("Success")

                        except Exception as e:
                            print('FAILED -', e)

                        nc_p += 1 
                except:
                    img_path.append('no picture')

                post_number = 'tht_l_'+p_id

                raw_data_home = {
                    'cut_find_web': find_web,
                    'cut_find_url': land_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': find_name,
                    'listType': find_list_type,
                    'propertyTypes': 'land',
                    'startingPrice': find_price,
                    'finalPrice': find_price,
                    'minimumArea': find_size,
                    'maximumArea': max_area,
                    'description': find_description,
                    'cut_find_location': find_location,
                    'cut_find_map': find_map,
                    'latitude': lat,
                    'longitude': long,
                    'date': datetime.datetime.utcnow(),
                }

                raw_data_homeu = {
                    'cut_find_web': land_data,
                    'galleryUrls': img_path,
                    'propertyTypes': 'land',
                    'startingPrice': find_price,
                    'finalPrice': find_price,
                    'minimumArea': find_size,
                    'maximumArea': max_area,
                    'description': find_description,
                    'cut_find_location': find_location,
                    'cut_find_map': find_map,
                    'latitude': lat,
                    'longitude': long,
                    'date': datetime.datetime.utcnow(),
                }

                # def update_data_mongo(post_number ,raw_data_homeu):
                #     myquery = {"post_no": post_number}
                #     mydoc = self.mycol.find(myquery)

                #     update_data = {"$set":raw_data_homeu}

                #     self.mycol.update_one(myquery,update_data)

                try:
                    print('try before insert')
                    try:
                        scrapetht.insert_data_mongo(self, raw_data_home)
                    except Exception as e:
                        print('insert error:',e)
                        scrapetht.insert_data_mongo(self, raw_data_home)
                    print('try after insert')
                except:
                    print('try before update', land_data)
                    scrapetht.update_data_mongo(self, post_number ,raw_data_homeu)
                    print('try after update', land_data)

                print(land_data)

            except Exception as e:
                print(land_data, e)
                raw_data_home = {
                    'cannot_open_url': land_data
                }
                scrapetht.insert_fails(self, raw_data_home)

    def get_townhouse_data(self):
        i = 0

        self.wd.get('https://www.thaihometown.com/%E0%B8%97%E0%B8%B2%E0%B8%A7%E0%B8%99%E0%B9%8C%E0%B9%82%E0%B8%AE%E0%B8%A1.html')
        web_data = requests.get('https://www.thaihometown.com/%E0%B8%97%E0%B8%B2%E0%B8%A7%E0%B8%99%E0%B9%8C%E0%B9%82%E0%B8%AE%E0%B8%A1.html')
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        try:
            find_townhouse_lists = soup.find_all('a', {'class' : 'namelink_pre'})#['href']
            for find_townhouse_list in find_townhouse_lists:
                self.townhouse_list_url.append(find_townhouse_list['href'])
        except:
            print('no namelink_pre')

        try:    
            find_townhouse_lists = soup.find_all('a', {'class' : 'namelink_recom'})#['href']
            for find_townhouse_list in find_townhouse_lists:
                self.townhouse_list_url.append(find_townhouse_list['href'])
        except:
            print('no namelink_recom')
            
        th_p_url = 'https://www.thaihometown.com/townhome/?page=2'
        web_data = requests.get(th_p_url)
        soup = BeautifulSoup(web_data.text, 'html.parser')
        find_page = soup.find("span",{"class":"f3"}).text
        last_page = find_page.split('/')[-1]
        print(last_page)
        townhouse_int_last_page = int(last_page)
        # townhouse_int_last_page = int(3)
        print(townhouse_int_last_page)

        for page in range(townhouse_int_last_page):
            page += 1    
            th_url = "https://www.thaihometown.com/townhome/?page="+str(page)
            self.wd.get(th_url)
            web_data = requests.get(th_url)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            self.th_page_url.append(th_url)
            find_th_lists = soup.find_all('a', {'class' : 'namelink'})#['href']
            for find_th_list in find_th_lists:
                self.townhouse_list_url.append(find_th_list['href'])

        print(self.th_page_url)

        ct_land_list_url = len(self.townhouse_list_url)
        print(self.townhouse_list_url)
        # print('self.condo_list_url')
        print(ct_land_list_url)

    #######################################################################

        i = 0

        self.wd.get('https://www.thaihometown.com/%E0%B8%97%E0%B8%B2%E0%B8%A7%E0%B8%99%E0%B9%8C%E0%B9%80%E0%B8%AE%E0%B9%89%E0%B8%B2%E0%B8%AA%E0%B9%8C.html')
        web_data = requests.get('https://www.thaihometown.com/%E0%B8%97%E0%B8%B2%E0%B8%A7%E0%B8%99%E0%B9%8C%E0%B9%80%E0%B8%AE%E0%B9%89%E0%B8%B2%E0%B8%AA%E0%B9%8C.html')
        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
        try:
            find_townhouse_lists = soup.find_all('a', {'class' : 'namelink_pre'})#['href']
            for find_townhouse_list in find_townhouse_lists:
                self.townhouse_list_url.append(find_townhouse_list['href'])
        except:
            print('no namelink_pre')

        try:    
            find_townhouse_lists = soup.find_all('a', {'class' : 'namelink_recom'})#['href']
            for find_townhouse_list in find_townhouse_lists:
                self.townhouse_list_url.append(find_townhouse_list['href'])
        except:
            print('no namelink_recom')
            
        th_p_url = 'https://www.thaihometown.com/townhouse/?page=2'
        web_data = requests.get(th_p_url)
        soup = BeautifulSoup(web_data.text, 'html.parser')
        find_page = soup.find("span",{"class":"f3"}).text
        last_page = find_page.split('/')[-1]
        print(last_page)
        townhouse_int_last_page = int(last_page)
        # townhouse_int_last_page = int(3)
        print(townhouse_int_last_page)

        for page in range(townhouse_int_last_page):
            page += 1    
            th_url = "https://www.thaihometown.com/townhouse/?page="+str(page)
            self.wd.get(th_url)
            web_data = requests.get(th_url)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            self.th_page_url.append(th_url)
            find_th_lists = soup.find_all('a', {'class' : 'namelink'})#['href']
            for find_th_list in find_th_lists:
                self.townhouse_list_url.append(find_th_list['href'])

        print(self.th_page_url)

        ct_land_list_url = len(self.townhouse_list_url)
        print(self.townhouse_list_url)
        # print('self.condo_list_url')
        print(ct_land_list_url)

        for townhouse_data in self.townhouse_list_url:

            try:

                details = []

                self.wd.get(townhouse_data)

                # web_data = requests.get(townhouse_data)
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="Mdetail"]/div/h1')))
                soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                find_url = townhouse_data
                find_web = 'thaihometown'
                find_name = soup.find("h1").text
                
                find_price = soup.find("a", {"class": "linkprice"}).text
                if 'ขาย' in find_price:
                    find_list_type = 'sell'
                elif 'เช่า' in find_price:
                    find_list_type = 'rent'
                find_linkcitys = soup.find_all('a',{'class':'linkcity'})
                find_size = find_linkcitys[0].text
                find_province = find_linkcitys[-2].text
                find_district = find_linkcitys[-1].text
                
                if find_linkcitys[1].text != find_linkcitys[-2].text:
                    max_area = find_linkcitys[1].text
                else:
                    max_area = find_size
                
                
                find_description = soup.find('div',{'class':'headtitle2'}).text
                find_table_details = soup.find_all('td',{'class':'table_set3'})
                find_table_header = soup.find_all('td',{'class':'Settable1'})
                try:
                    for n_fth in range(len(find_table_header)):
                        if 'จำนวนห้อง' in find_table_header[n_fth].text : #totalBedroom#totaltoilet
                            print('จำนวนห้อง',find_table_header[n_fth].text)
                            try:
                                find_bedroom = find_table_details[n_fth].text.split(' ห้องนอน')[0]
                                print('find_bedroom1',n_fth,find_bedroom)
                            except:
                                find_bedroom = find_table_details[n_fth].text
                                print('find_bedroom2',n_fth,find_bedroom)
                            try:
                                find_bathroom = find_table_details[n_fth].text.split('ห้องนอน ')[1].split('ห้องน้ำ')[0]
                                print('find_bathroom1',n_fth,find_bathroom)
                            except:
                                find_bathroom = find_table_details[n_fth].text
                                print('find_bathroom2',n_fth,find_bathroom)


                except:
                    find_bedroom = ' ห้องนอน'
                    find_bathroom = ' ห้องน้ำ'
                    find_size = 'พื้นที่'
                    find_province = 'จังหวัด'
                    find_district = 'อำเภอ'
                try :
                    find_location = find_district+' '+find_province
                except :
                    find_location = 'ไม่มีข้อมูล'
                    
                ##findmap
                start_tab = self.wd.current_window_handle
                self.wd.execute_script("window.open('');")
                self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                self.wd.get('https://www.google.co.th/maps')
                self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_name)
                self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                print('clicksearch')
                time.sleep(3)
                find_map = self.wd.current_url
                print(find_map)
                try:
                    latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                    lat = latlong[0]
                    long = latlong[1]
                except:
                    lat = find_map
                    long = find_map
                self.wd.close()
                self.wd.switch_to.window(self.wd.window_handles[0])#เปลี่ยนแท็ป
                
                img_path = []

                p_id = townhouse_data.split('/')[4]
                save_path = self.sys_xpath.format(p_id)
                print(save_path)
                try:
                    os.mkdir(save_path, self.mode)
                    os.chdir(os.path.join(os.getcwd(), save_path))
                except:
                    os.chdir(os.path.join(os.getcwd(), save_path))

                try:
                    nc_p = 1
                    for c_p in soup.find_all('img',{'class':'style_img'}):
                        image_url = c_p['src'].replace('_small','')
                        file_name = str(nc_p) + ".jpg"
                        try:
                            image_content = requests.get(image_url).content
                            image_file = io.BytesIO(image_content)
                            image = Image.open(image_file)
                            file_path = save_path +'/' + file_name

                            img_path.append(file_path)

                            with open(file_path, "wb") as f:
                                image.save(f, "PNG")

                            print("Success")

                        except Exception as e:
                            print('FAILED -', e)

                        nc_p += 1 
                except:
                    img_path.append('no picture')

                post_number = 'tht_th_'+p_id

                raw_data_home = {
                    'cut_find_web': find_web,
                    'cut_find_url': townhouse_data,
                    'post_no': post_number,
                    'galleryUrls': img_path,
                    'name': find_name,
                    'listType': find_list_type,
                    'propertyTypes': ObjectId('62bd7104f0521b8890fe9a15'),
                    'totalBedroom': find_bedroom,
                    'totalToilet': find_bathroom,
                    'startingPrice': find_price,
                    'finalPrice': find_price,
                    'minimumArea': find_size,
                    'maximumArea': max_area,
                    'description': find_description,
                    'cut_find_location': find_location,
                    'cut_find_map': find_map,
                    'latitude': lat,
                    'longitude': long,
                    'date': datetime.datetime.utcnow(),
                }

                raw_data_homeu = {
                    'cut_find_web': find_web,
                    'galleryUrls': img_path,
                    'name': find_name,
                    'listType': find_list_type,
                    'propertyTypes': ObjectId('62bd7104f0521b8890fe9a15'),
                    'totalBedroom': find_bedroom,
                    'totalToilet': find_bathroom,
                    'startingPrice': find_price,
                    'finalPrice': find_price,
                    'minimumArea': find_size,
                    'maximumArea': max_area,
                    'description': find_description,
                    'cut_find_location': find_location,
                    'cut_find_map': find_map,
                    'latitude': lat,
                    'longitude': long,
                    'date': datetime.datetime.utcnow(),
                }

                # def update_data_mongo(post_number ,raw_data_homeu):
                #     myquery = {"post_no": post_number}
                #     mydoc = self.mycol.find(myquery)

                #     update_data = {"$set":raw_data_homeu}

                #     self.mycol.update_one(myquery,update_data)

                try:
                    print('try before insert')
                    try:
                        scrapetht.insert_data_mongo(self, raw_data_home)
                    except Exception as e:
                        print('insert error:',e)
                        scrapetht.insert_data_mongo(self, raw_data_home)
                    print('try after insert')
                except:
                    print('try before update', townhouse_data)
                    scrapetht.update_data_mongo(self, post_number ,raw_data_homeu)
                    print('try after update', townhouse_data)

                print('finish townhouse_url :',townhouse_data)       

                print('townhouse_data :', i)

                i += 1

            except Exception as e:
                print(e)
                raw_data_home = {
                    'cannot_open_url': townhouse_data
                }
                scrapetht.insert_fails(self, raw_data_home)

    def quit(self):
        self.wd.quit()
class starttht:
    def starttht(self):

        ft = scrapetht()

        try:
            try:
                ft.get_condo_data()
                print('finish get_condo_data')
            except Exception as e:
                print('fail get_condo_data :',e)
            try:    
                ft.get_house_data()
                print('finish get_house_data')
            except Exception as e:
                print('fail get_house_data :',e)                
            try: 
                ft.get_land_data()
                print('finish get_land_data')
            except Exception as e:
                print('fail get_land_data :',e)
            try:    
                ft.get_townhouse_data()
                print('finish get_townhouse_data')
            except Exception as e:
                print('fail get_townhouse_data :',e)                
            print('finish tht')
            ft.quit()
        except Exception as e:
            print('fail tht error:', e)
            ft.quit()


d = starttht()
d.starttht()