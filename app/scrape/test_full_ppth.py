from pickle import GLOBAL
import requests
from bs4 import BeautifulSoup

from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait,Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import io
from PIL import Image
import time
import os
from pprint import pprint
from pymongo import MongoClient
import pymongo
from dotenv import load_dotenv
from collections import ChainMap
from bson.objectid import ObjectId
import datetime

class scrapeppth:
    def __init__(self):
        self.mode = 0o666
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")

        mydb = myclient["devXInvester"]
        self.mycol = mydb["ppth_propoty_project_urls_unscrape"]
        # self.mycol_ppth = mydb["watchLists_ppth"]
        self.mycol_ppth = mydb["watchLists"]
        self.mycol_c = mydb["watchLists_cppth"]

        self.propoty_project_urls = []

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)
        
        self.wait = WebDriverWait(self.wd, 10)

        self.img_xpath = '//*[@id="__next"]/div/div[1]/div/div/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div/a[{}]/div/img'
        self.ch_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_propertyhub"
        self.sys_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_propertyhub/imgs_{}"


    def click_next(self):
        link = self.wd.find_element(By.LINK_TEXT, "ถัดไป")
        link.click()
        print("Click Next")

    def insert_data_mongo(raw_data_home):
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        
        print('1l insert')
        mydb = myclient["devXInvester"]

        # mycol_ppth = mydb["watchLists_ppth"]
        mycol_ppth = mydb["watchLists"]

        mydict = raw_data_home
        
        mycol_ppth.insert_one(mydict)
        print('ll insert')


    def update_data_mongo(post_number ,raw_data_homeu):
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")

        mydb = myclient["devXInvester"]

        # mycol_ppth = mydb["watchLists_ppth"]
        mycol_ppth = mydb["watchLists"]
        myquery = {"post_no": post_number}
        mydoc = mycol_ppth.find(myquery)

        update_data = {"$set":raw_data_homeu}

        mycol_ppth.update_one(myquery,update_data)
    def insert_fails(self, raw_data_home):
        mydict = raw_data_home

        self.mycol_c.insert_one(mydict)

    def get_urls(self):
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        mydb = myclient["devXInvester"]
        
        mycol = mydb["propoty_project_urls"]

        for x in mycol.find():
            urls = x.get('condo_name_url')
            # print('urls',urls)
            self.propoty_project_urls.append(urls)
        print('self.propoty_project_urls',self.propoty_project_urls)

    def get_urls_from_ppth(self):
        for cd_url in self.propoty_project_urls:
            self.wd.get(cd_url)

            # condo_list_url = []
            page_list = []
            rent_list_urls = []
            sell_list_urls = []
            web_data = requests.get(cd_url)
            soup = BeautifulSoup(web_data.text, 'html.parser')
# 
            try:
                rent_link = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div/div[2]/div[1]/a').get_attribute('href')
                print(rent_link)
                rent_list_urls.append(rent_link)
            except:
                print('no rent')
            try:
                sell_link = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div/div[3]/div[1]/a').get_attribute('href')
                print(sell_link)
                sell_list_urls.append(sell_link)
            except:
                print('no sell')

            for rent_list_url in rent_list_urls:
                self.wd.get(rent_list_url)

                condo_rent_list_url = []
                page_list = []
                web_data = requests.get(rent_list_url)
                soup = BeautifulSoup(web_data.text, 'html.parser')

                try:
                    find_page = soup.find_all("a",{"role":"button"})
                    last_page = find_page[-2].text
                    int_last_page = int(last_page)
                except:
                    int_last_page = 1
                    print('except int_last_page')

                cpage = 1

                for page in range(int_last_page):
                    
                    print('page' , cpage)

                    # web_data = requests.get(self.wd.page_source)

                    soup = BeautifulSoup(self.wd.page_source, 'html.parser')

                    find_condo_post_url = soup.find_all("a",{"class":"sc-152o12i-9 fhmSYQ"})

                    for condo_data in find_condo_post_url:
                        condo_rent_list_url.append(condo_data.get('href'))
                    print('condo_list_url',condo_rent_list_url)

                    time.sleep(5)

                    # for condo_data in self.wd.find_elements(By.XPATH, "//div[@class = 'col-xs sc-1qj7qf1-2 sc-1d6w2u0-1 sc-10ubcco-0 dItFoB']//div[@class = 'sc-1jlq1rl-2 hRFWep']//div[@class = 'sc-1jlq1rl-3 hROsIy']//div[@class = 'sc-5fg8ty-9 FHJJA']//div[@class = 'm8nysy-3 bedAIM listing-list-view']//div[@class = 'm8nysy-4 m8nysy-5 bUJBVL']//div[@class = 'sc-152o12i-0 iWSTG i5hg7z-1 lipDww']//div[@class = 'sc-152o12i-7 dWBnGa']//div[@class = 'i5hg7z-2 eGZDxx']//span[@class = 'i5hg7z-3 eHiabG']//a[@class = 'sc-152o12i-1 eVFiiC']"):
                    #     condo_list_url.append(condo_data.get_attribute('href'))

                    if cpage <= int_last_page-1:
                        # scrapeppth.click_next()
                        link = self.wd.find_element(By.LINK_TEXT, "ถัดไป")
                        link.click()
                        print("Click Next")

                    cpage += 1

                    # print("cpage",cpage)

                    www = len(condo_rent_list_url)
                    # print(www)
                    # print(condo_list_url)
                    # for condo_data in condo_list_url:
                    #     print(cpage)
                    # #####################################
                    #     scrap1page()
                    #     click()
                    ####################################


                # eee = len(condo_list_url)

                # print(www)

                # print(eee)

                # scrap1page()
                for condo_data in condo_rent_list_url:

                    details = []
                    img_path = []

                    try:
                        
                        cut_find_url = 'https://propertyhub.in.th'+condo_data
                        print(cut_find_url)

                        self.wd.get(cut_find_url)
                        web_data = requests.get(cut_find_url)
                        time.sleep(3)
                        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                        print('soup',soup)
                        post_no = soup.find('div',{'class':'sc-ogfj7g-6'}).text.split(' : ')[1]
                        save_path = self.sys_xpath.format(post_no)

                        print('save_path',save_path)

                        os.chdir(os.path.join(os.getcwd(), self.ch_xpath))

                        try:
                            os.mkdir(save_path, self.mode)
                            os.chdir(os.path.join(os.getcwd(), save_path))
                        except:
                            os.chdir(os.path.join(os.getcwd(), save_path))

                        try:            
                            n_p = soup.find('span',{'class':'image-gallery-index-total'}).text
                            for c_p in range(int(n_p)):
                                c_p+=1
                                elems = self.wd.find_element(By.XPATH, self.img_xpath.format(c_p)).get_attribute('src')
                                image_url = elems.replace('eb260bec','be7210af')
                                file_name = str(c_p) + ".jpg"
                                try:
                                    try:
                                        image_content = requests.get(image_url).content
                                        image_file = io.BytesIO(image_content)
                                        image = Image.open(image_file)
                                        file_path = save_path +'/' + file_name
                                        # file_path = save_path +'\\' + file_name

                                        img_path.append(file_path)

                                        with open(file_path, "wb") as f:
                                            image.save(f, "PNG" ,optimize=True,quality=75)
            
                                        # print("Success",img_path)
                                    except:
                                        image_content = requests.get(elems).content
                                        image_file = io.BytesIO(image_content)
                                        image = Image.open(image_file)
                                        file_path = save_path +'/' + file_name
                                        # file_path = save_path +'\\' + file_name

                                        img_path.append(file_path)

                                        with open(file_path, "wb") as f:
                                            image.save(f, "PNG" ,optimize=True,quality=75)
            
                                        # print("Success",img_path)
                                except Exception as e:
                                    print('FAILED -', e)

                        except:
                            img_path.append('no img')
                        # soup = BeautifulSoup(web_data.text, 'html.parser')
                        # find_web = condo_list_url[i].split(".")[0]
                        find_name = soup.find("h1").text
                        find_location = soup.find("span",{"class":"sc-8sst81-4"}).text
                        find_location_details = find_location.split(' ')
                        find_address = soup.find("h2",{"class":"sc-p50xz0-5"}).text
                        find_price = soup.find("div",{"class":"sc-s9r052-4"}).text
                        find_details = soup.find_all("li",{"class":"sc-s9r052-1"})
                        find_description = soup.select_one("#__next > div > div:nth-of-type(1) > div > div > div:nth-of-type(1) > div:nth-of-type(3) > div:nth-of-type(1)")

                        if find_description is not None:
                            find_description_data = find_description.text
                            # print('find_description',find_description_data)
                        else:
                            print("Description not found.")

                        try:
                            try:
                                find_size = soup.select_one('#__next > div > div:nth-of-type(1) > div > div > div:nth-of-type(1) > div:nth-of-type(3) > div:nth-of-type(2) > ul:nth-of-type(2) > li:last-child > span')
                                find_size_data = find_size.text
                                print('find_size',find_size_data)
                            except:
                                find_size = soup.select_one("#__next > div > div:nth-of-type(1) > div > div > div:nth-of-type(1) > div:nth-of-type(3) > div:nth-of-type(2) > ul > li:last-child > span")
                                find_size_data = find_size.text
                                print('find_size',find_size_data)
                        except:
                            find_size_data = 'find_size_data not found.'
                            

                        find_bedroom = soup.select_one("#__next > div > div:nth-of-type(1) > div > div > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1)")
                        if find_bedroom is not None:
                            find_bedroom_data = find_bedroom.text
                            print('find_bedroom',find_bedroom_data)
                        else:
                            print("find_bedroom_data not found.")

                        ##findmap
                        start_tab = self.wd.current_window_handle
                        # self.wd.execute_script("window.open('');")
                        # self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                        self.wd.get('https://www.google.co.th/maps')
                        self.wait.until(EC.element_to_be_clickable((By.ID, 'searchboxinput')))
                        self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_address)
                        self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                        print('clicksearch')
                        time.sleep(3)
                        find_map = self.wd.current_url
                        try:
                            latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                            lat = latlong[0]
                            long = latlong[1]
                        except:
                            lat = find_map
                            long = find_map

                        # find_bathroom = soup.find_all("span",{"class":"lv-detail-font"})#ไม่มี

                        # if len(find_name) == 1:
                            # cut_find_url = find_url
                        cut_find_web = 'propotyhub'
                        cut_find_name = find_name
                        cut_find_location = find_location
                        cut_find_city = find_location_details[-2]
                        cut_find_state = find_location_details[-1]
                        cut_find_address = find_address
                        cut_find_price = find_price
                        cut_find_details = find_details
                        post_number = 'propotyhub_'+post_no

                        for detail in find_details:
                            # print(detail.text)
                            detail_datas = {
                                detail.text.split(':')[0]:detail.text.split(':')[1]
                            }

                            # print(datas)
                            details.append(detail_datas)


                        raw_data_home = {
                            'cut_find_web': cut_find_web,
                            'cut_find_url': cut_find_url,
                            'post_no': post_number,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'rent',
                            'propertyTypes': ObjectId('62bd7104f0521b8890fe9a0f'),
                            'totalBedroom': find_bedroom_data,
                            'minimumArea': find_size_data,
                            'maximumArea': find_size_data,
                            'description': find_description_data,
                            'cut_find_location': cut_find_location,
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'SubCity':'-',
                            'City': cut_find_city,
                            'State': cut_find_state,
                            'cut_find_address': cut_find_address,
                            'cut_find_details': details,
                            'map' : find_map,
                            'latitude': lat,
                            'longitude': long,
                            'date': datetime.datetime.utcnow(),
                        }
                        raw_data_homeu = {
                            'cut_find_web': cut_find_web,
                            'cut_find_url': cut_find_url,
                            'post_no': post_number,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'rent',
                            'propertyTypes': ObjectId('62bd7104f0521b8890fe9a0f'),
                            'totalBedroom': find_bedroom_data,
                            'minimumArea': find_size_data,
                            'maximumArea': find_size_data,
                            'description': find_description_data,
                            'cut_find_location': cut_find_location,
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'SubCity':'-',
                            'City': cut_find_city,
                            'State': cut_find_state,
                            'cut_find_address': cut_find_address,
                            'cut_find_details': details,
                            'map' : find_map,
                            'latitude': lat,
                            'longitude': long,
                            'date': datetime.datetime.utcnow(),
                        }
                        # print(raw_data_home)
                        # scrapeppth.insert_data_mongo(raw_data_home)

                        try:
                            print('try before insert')
                            scrapeppth.insert_data_mongo(raw_data_home)
                            print('try after insert')
                        except Exception as e:
                            print(e)
                            print('try before update')
                            scrapeppth.update_data_mongo(post_number ,raw_data_homeu)
                            print('try after update')



                    ########################################################################################
                            # if condo_list_url[i] not in {"cut_find_url": condo_list_url} :
                            #     insert_data_mongo(raw_data_home)

                            # else :
                            #     update_data_mongo(raw_data_home)
                    ########################################################################################

                        print(condo_data,'finish')
                            
                    except:
                        raw_data_home = {
                            'cannot requests': condo_data,
 
                        }
                        # print(raw_data_home)
                        # scrapeppth.insert_fails(self, raw_data_home)
                    # self.wd.close()


            for sell_list_url in sell_list_urls:
                self.wd.get(sell_list_url)

                condo_sell_list_url = []
                page_list = []
                web_data = requests.get(sell_list_url)
                soup = BeautifulSoup(web_data.text, 'html.parser')

                try:
                    find_page = soup.find_all("a",{"role":"button"})
                    last_page = find_page[-2].text
                    int_last_page = int(last_page)
                except:
                    int_last_page = 1
                    print('except int_last_page')

                cpage = 1

                for page in range(int_last_page):

                    print('page' , cpage)

                    soup = BeautifulSoup(self.wd.page_source, 'html.parser')

                    find_condo_post_url = soup.find_all("a",{"class":"sc-152o12i-9 fhmSYQ"})

                    for condo_data in find_condo_post_url:
                        condo_sell_list_url.append(condo_data.get('href'))
                    print('condo_list_url',condo_sell_list_url)

                    time.sleep(5)

                    # for condo_data in self.wd.find_elements(By.XPATH, "//div[@class = 'col-xs sc-1qj7qf1-2 sc-1d6w2u0-1 sc-10ubcco-0 dItFoB']//div[@class = 'sc-1jlq1rl-2 hRFWep']//div[@class = 'sc-1jlq1rl-3 hROsIy']//div[@class = 'sc-5fg8ty-9 FHJJA']//div[@class = 'm8nysy-3 bedAIM listing-list-view']//div[@class = 'm8nysy-4 m8nysy-5 bUJBVL']//div[@class = 'sc-152o12i-0 iWSTG i5hg7z-1 lipDww']//div[@class = 'sc-152o12i-7 dWBnGa']//div[@class = 'i5hg7z-2 eGZDxx']//span[@class = 'i5hg7z-3 eHiabG']//a[@class = 'sc-152o12i-1 eVFiiC']"):
                    #     condo_list_url.append(condo_data.get_attribute('href'))

                    if cpage <= int_last_page-1:
                        # scrapeppth.click_next()
                        link = self.wd.find_element(By.LINK_TEXT, "ถัดไป")
                        link.click()
                        print("Click Next")
                    cpage += 1

                    # print("cpage",cpage)

                    www = len(condo_sell_list_url)
                    # print(www)
                    # print(condo_list_url)
                    # for condo_data in condo_list_url:
                    #     print(cpage)
                    # #####################################
                    #     scrap1page()
                    #     click()
                    ####################################


                eee = len(condo_sell_list_url)

                # print(condo_list_url)

                print(www)

                print(eee)

                # scrap1page()
                for condo_data in condo_sell_list_url:
                    
                    details = []
                    img_path = []

                    try:

                        cut_find_url = 'https://propertyhub.in.th'+condo_data
                        print(cut_find_url)
                        
                        self.wd.get(cut_find_url)
                        web_data = requests.get(cut_find_url)
                        time.sleep(3)
                        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                        post_no = soup.find('div',{'class':'sc-ogfj7g-6'}).text.split(' : ')[1]
                        save_path = self.sys_xpath.format(post_no)
                        
                        print('save_path',save_path)

                        os.chdir(os.path.join(os.getcwd(), self.ch_xpath))

                        try:
                            os.mkdir(save_path, self.mode)
                            os.chdir(os.path.join(os.getcwd(), save_path))
                        except:
                            os.chdir(os.path.join(os.getcwd(), save_path))

                        try:            
                            n_p = soup.find('span',{'class':'image-gallery-index-total'}).text
                            for c_p in range(int(n_p)):
                                c_p+=1
                                elems = self.wd.find_element(By.XPATH, self.img_xpath.format(c_p)).get_attribute('src')
                                image_url = elems.replace('eb260bec','be7210af')
                                file_name = str(c_p) + ".jpg"
                                try:
                                    try:
                                        image_content = requests.get(image_url).content
                                        image_file = io.BytesIO(image_content)
                                        image = Image.open(image_file)
                                        file_path = save_path +'/' + file_name
                                        # file_path = save_path +'\\' + file_name

                                        img_path.append(file_path)

                                        with open(file_path, "wb") as f:
                                            image.save(f, "PNG" ,optimize=True,quality=75)
            
                                        # print("Success",img_path)
                                    except:
                                        image_content = requests.get(elems).content
                                        image_file = io.BytesIO(image_content)
                                        image = Image.open(image_file)
                                        file_path = save_path +'/' + file_name
                                        # file_path = save_path +'\\' + file_name

                                        img_path.append(file_path)

                                        with open(file_path, "wb") as f:
                                            image.save(f, "PNG" ,optimize=True,quality=75)
            
                                        # print("Success",img_path)
                                except Exception as e:
                                    print('FAILED -', e)

                        except:
                            img_path.append('no img')
                        # soup = BeautifulSoup(web_data.text, 'html.parser')
                        # find_web = condo_list_url[i].split(".")[0]
                        find_name = soup.find("h1").text
                        find_location = soup.find("span",{"class":"sc-8sst81-4"}).text
                        find_location_details = find_location.split(' ')
                        find_address = soup.find("h2",{"class":"sc-p50xz0-5"}).text
                        find_price = soup.find("div",{"class":"sc-s9r052-4"}).text
                        find_details = soup.find_all("li",{"class":"sc-s9r052-1"})
                        find_description = soup.select_one("#__next > div > div:nth-of-type(1) > div > div > div:nth-of-type(1) > div:nth-of-type(3) > div:nth-of-type(1)")

                        if find_description is not None:
                            find_description_data = find_description.text
                            # print('find_description',find_description_data)
                        else:
                            print("Description not found.")

                        try:
                            try:
                                find_size = soup.select_one('#__next > div > div:nth-of-type(1) > div > div > div:nth-of-type(1) > div:nth-of-type(3) > div:nth-of-type(2) > ul:nth-of-type(2) > li:last-child > span')
                                find_size_data = find_size.text
                                print('find_size',find_size_data)
                            except:
                                find_size = soup.select_one("#__next > div > div:nth-of-type(1) > div > div > div:nth-of-type(1) > div:nth-of-type(3) > div:nth-of-type(2) > ul > li:last-child > span")
                                find_size_data = find_size.text
                                print('find_size',find_size_data)
                        except:
                            find_size_data = 'find_size_data not found.'
                            

                        find_bedroom = soup.select_one("#__next > div > div:nth-of-type(1) > div > div > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1)")
                        if find_bedroom is not None:
                            find_bedroom_data = find_bedroom.text
                            print('find_bedroom',find_bedroom_data)
                        else:
                            print("find_bedroom_data not found.")

                        ##findmap
                        start_tab = self.wd.current_window_handle
                        # self.wd.execute_script("window.open('');")
                        # self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                        self.wd.get('https://www.google.co.th/maps')
                        self.wait.until(EC.element_to_be_clickable((By.ID, 'searchboxinput')))
                        self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_address)
                        self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                        print('clicksearch')
                        time.sleep(3)
                        find_map = self.wd.current_url
                        try:
                            latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                            lat = latlong[0]
                            long = latlong[1]
                        except:
                            lat = find_map
                            long = find_map

                        # find_bathroom = soup.find_all("span",{"class":"lv-detail-font"})#ไม่มี

                        # if len(find_name) == 1:
                            # cut_find_url = find_url
                        cut_find_web = 'propotyhub'
                        cut_find_name = find_name
                        cut_find_location = find_location
                        cut_find_city = find_location_details[-2]
                        cut_find_state = find_location_details[-1]
                        cut_find_address = find_address
                        cut_find_price = find_price
                        cut_find_details = find_details
                        post_number = 'propotyhub_'+post_no

                        for detail in find_details:
                            # print(detail.text)
                            detail_datas = {
                                detail.text.split(':')[0]:detail.text.split(':')[1]
                            }

                            # print(datas)
                            details.append(detail_datas)

                        raw_data_home = {
                            'cut_find_web': cut_find_web,
                            'cut_find_url': cut_find_url,
                            'post_no': post_number,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'sell',
                            'propertyTypes': ObjectId('62bd7104f0521b8890fe9a0f'),
                            'totalBedroom': find_bedroom_data,
                            'minimumArea': find_size_data,
                            'maximumArea': find_size_data,
                            'description': find_description_data,
                            'cut_find_location': cut_find_location,
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'SubCity':'-',
                            'City': cut_find_city,
                            'State': cut_find_state,
                            'cut_find_address': cut_find_address,
                            'cut_find_details': details,
                            'map' : find_map,
                            'latitude': lat,
                            'longitude': long,
                            'date': datetime.datetime.utcnow(),
                        }
                        raw_data_homeu = {
                            'cut_find_web': cut_find_web,
                            'cut_find_url': cut_find_url,
                            'post_no': post_number,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'sell',
                            'propertyTypes': ObjectId('62bd7104f0521b8890fe9a0f'),
                            'totalBedroom': find_bedroom_data,
                            'minimumArea': find_size_data,
                            'maximumArea': find_size_data,
                            'description': find_description_data,
                            'cut_find_location': cut_find_location,
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'SubCity':'-',
                            'City': cut_find_city,
                            'State': cut_find_state,
                            'cut_find_address': cut_find_address,
                            'cut_find_details': details,
                            'map' : find_map,
                            'latitude': lat,
                            'longitude': long,
                            'date': datetime.datetime.utcnow(),
                        }
                        # print(raw_data_home)
                        # scrapeppth.insert_data_mongo(self, raw_data_home)

                        try:
                            print('try before insert')
                            scrapeppth.insert_data_mongo(raw_data_home)
                            print('try after insert')
                        except Exception as e:
                            print(e)
                            print('try before update')
                            scrapeppth.update_data_mongo(post_number ,raw_data_homeu)
                            print('try after update')



                    ########################################################################################
                            # if condo_list_url[i] not in {"cut_find_url": condo_list_url} :
                            #     insert_data_mongo(raw_data_home)

                            # else :
                            #     update_data_mongo(raw_data_home)
                    ########################################################################################

                        print(condo_data,'finish')
                            
                    except:
                        raw_data_home = {
                            'cannot requests': condo_data,
 
                        }
                        # print(raw_data_home)
                        # scrapeppth.insert_fails(self, raw_data_home)
                    # self.wd.close()
        print('finish')
        # self.wd.close()
    
    def quit_ppth(self):
        self.wd.quit()



class startppth:
    def startppth(self):

        fn = scrapeppth()

        try:
            fn.get_urls()
            fn.get_urls_from_ppth()
            print('finish get_urls_from_ppth')
            fn.quit_ppth()
            print('finish')

                # fn.download_imgs()
        except Exception as e:
            print('error:', e)
            fn.quit_ppth()

c = startppth()
c.startppth()
