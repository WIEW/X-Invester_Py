from re import T
import requests
from bs4 import BeautifulSoup
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
import requests
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo
from collections import ChainMap
from bson.objectid import ObjectId

class scraperenthub:
    def __init__(self):

        self.mode = 0o666
        self.ch_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_renthub"
        self.sys_xpath = "/root/X-Invester_Py/app/public/api_imgs/test_renthub/imgs_{}"
        self.img_xpath = '//*[@id="__next"]/div[2]/div/div/div[1]/div[2]/div[2]/div/div[1]/div/div[4]/div/div/div[{}]/img'
        self.imgclick_xpath = '//*[@id="__next"]/div[2]/div/div/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/div/div[{}]/img'

        # ##Start of db zone###
        # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        
        mydb = myclient["devXInvester"]
        self.mycol = mydb["watchLists_renthub"]

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        options.add_argument(f'user-agent={user_agent}')
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)
        self.wait = WebDriverWait(self.wd, 60)

        self.ltr_urls = []
        self.str_urls = []
        self.ltr_post_urls = []
        self.str_post_urls = []
        self.condo_name_url =[]
        self.cannot_open_condo_name_url = []
        self.projects_name_th = []
        self.projects_name_en = []

    def insert_data_mongo(self, raw_data_home):

        mydict = raw_data_home

        x = self.mycol.insert_one(mydict)

    def click(self):
        link = self.wd.find_element(By.LINK_TEXT, "ถัดไป")
        link.click()
        print("Click Next")

    def get_urls(self):
        ltr_url = "https://www.renthub.in.th/%E0%B8%AD%E0%B8%9E%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%97%E0%B9%80%E0%B8%A1%E0%B9%89%E0%B8%99%E0%B8%97%E0%B9%8C-%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%9E%E0%B8%B1%E0%B8%81-%E0%B8%AB%E0%B8%AD%E0%B8%9E%E0%B8%B1%E0%B8%81/{}"
        str_url = "https://www.renthub.in.th/%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%9E%E0%B8%B1%E0%B8%81%E0%B8%A3%E0%B8%B2%E0%B8%A2%E0%B8%A7%E0%B8%B1%E0%B8%99/%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%9E%E0%B8%B1%E0%B8%81%E0%B8%A3%E0%B8%B2%E0%B8%A2%E0%B8%A7%E0%B8%B1%E0%B8%99-{}"
        provinces = ['กรุงเทพมหานคร', 'นนทบุรี', 'สมุทรปราการ', 'ปทุมธานี', 'สมุทรสาคร', 'สมุทรสงคราม', 'นครปฐม', 'เชียงใหม่', 'พิษณุโลก', 'ตาก', 'ลำปาง', 'อุตรดิตถ์', 'แพร่', 'น่าน', 'พะเยา', 'เชียงราย', 'แม่ฮ่องสอน', 'กำแพงเพชร', 'สุโขทัย', 'พิจิตร', 'เพชรบูรณ์', 'ลำพูน', 'เพชรบุรี', 'ประจวบคีรีขันธ์', 'กาญจนบุรี', 'ราชบุรี', 'นครราชสีมา-โคราช', 'อุบลราชธานี', 'ร้อยเอ็ด', 'กาฬสินธุ์', 'สกลนคร', 'มุกดาหาร', 'บุรีรัมย์', 'สุรินทร์', 'บึงกาฬ', 'ศรีสะเกษ', 'ยโสธร', 'ชัยภูมิ', 'อำนาจเจริญ', 'หนองบัวลำภู', 'ขอนแก่น', 'อุดรธานี', 'เลย', 'หนองคาย', 'มหาสารคาม', 'นครพนม', 'สุพรรณบุรี', 'นครสวรรค์', 'สระบุรี', 'ลพบุรี', 'อุทัยธานี', 'พระนครศรีอยุธยา', 'อ่างทอง', 'สิงห์บุรี', 'ชัยนาท', 'นครนายก', 'ชลบุรี', 'สระแก้ว', 'ระยอง', 'จันทบุรี', 'ตราด', 'ฉะเชิงเทรา', 'ปราจีนบุรี', 'นครศรีธรรมราช', 'กระบี่', 'ภูเก็ต', 'ยะลา', 'ระนอง', 'สงขลา', 'สตูล', 'ตรัง', 'พัทลุง', 'ปัตตานี', 'พังงา', 'สุราษฎร์ธานี', 'ชุมพร', 'นราธิวาส']
        for p in provinces:
            self.ltr_urls.append(ltr_url.format(p))
            self.str_urls.append(str_url.format(p))

    def get_ltr(self):
        hpp_url = "https://www.renthub.in.th{}"
        # cpage = 1

        for l_url in self.ltr_urls:
            self.wd.get(l_url)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            try:
                find_page = soup.find_all("a",{"role":"button"})
                last_page = find_page[-2].text
                int_last_page = int(last_page)
                print(int_last_page)
            except:
                int_last_page = 1
            try:
                self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[3]/button').click()
            except:
                print('no pop up')

            for cpage in range(int_last_page):
                print('page',cpage+1)
                soup = BeautifulSoup(self.wd.page_source, 'html.parser')

                for apartment_post in soup.find_all("a",{'class':'css-10i63lj'}):
                    self.ltr_post_urls.append(hpp_url.format(apartment_post['href']))
                    print(hpp_url.format(apartment_post['href']))

                try:
                    if cpage < int_last_page-1:
                        self.wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "ถัดไป")))

                        try:
                            scraperenthub.click()
                        except:
                            time.sleep(2)
                            print('c2')
                            scraperenthub.click()
                        print(cpage+1,'to',cpage+2,'of',int_last_page) 
                # print(self.ltr_post_urls)
                except:
                    print('1page')

        print('finish self.ltr_post_urls.append')

        for ltr_post_url in self.ltr_post_urls:
            details = []
            img_path = []

            print(ltr_post_url)


            def one_page():

                try:

                    self.wd.get(ltr_post_url)

                    soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                    try:                    
                        find_h1 = soup.find("h1").text
                        print(find_h1)
                    except:
                            self.wd.get(ltr_post_url)
                            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                            find_h1 = soup.find('h1').text
                            print(find_h1)
                    try:

                        find_url = ltr_post_url
                        find_name = soup.find("h1")
                        find_location = soup.find("span",{"class":"css-kbs8tw"})
                        try:
                            find_price = soup.find("div",{"class":"price css-k5exox e16r65b90"}).text
                        except:
                            find_price = soup.find("div",{"class":"price css-k5exox e16r65b90"})
                        find_details = soup.find_all("li",{"class":"css-7fau71 e4yj7n60"})

                        ##findmap
                        start_tab = self.wd.current_window_handle
                        self.wd.execute_script("window.open('');")
                        self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                        self.wd.get('https://www.google.co.th/maps')
                        self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_name.text)
                        self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                        print('clicksearch')
                        time.sleep(3)
                        find_map = self.wd.current_url
                        print(find_map)
                        try:
                            latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                            lat = latlong[0]
                            long = latlong[1]
                        except:
                            lat = find_map
                            long = find_map
                        self.wd.close()
                        self.wd.switch_to.window(self.wd.window_handles[0])#เปลี่ยนแท็ป

                        if len(find_name) == 1:
                            print("------------------------------------------")
                            cut_find_name = find_name.text
                            cut_find_location = find_location.text
                            cut_find_price = find_price
                            cut_find_details = find_details[0].text.strip()
                            try:
                                find_description = soup.find("div",{"class":"css-zwxq9d"}).text
                            except:
                                find_description = 'no description'
                            # cut_find_bedroom = find_bedroom[1].text
                            # cut_find_bathroom = '\n'+find_bathroom[3].text.replace('\n                                                              ','').replace('\n',' ').replace('                                                           ','')
                            # cut_find_map = '\n'+find_map

                            # print(type(find_details))

                            for detail in find_details:
                                # print(type(detail))#.text)
                                detail_datas = {
                                    detail.text.split(':')[0]:detail.text.split(':')[1]
                                }

                                # print(datas)
                                details.append(detail_datas)

                        else:
                            print('error')


                        p_id = soup.find('span',{'class': 'css-1qahy2z'}).text.split(' : ')[1]
                        save_path = self.sys_xpath.format(p_id)
                        print(save_path)
                        try:
                            os.mkdir(save_path, self.mode)
                            os.chdir(os.path.join(os.getcwd(), save_path))
                        except:
                            os.chdir(os.path.join(os.getcwd(), save_path))

                        try:
                            try:
                                n_pic = soup.find('span',{'class':'css-15dx7ok'}).text.split('/')[-1]
                                for c_pic in range(int(n_pic)):
                                    c_pic+=1
                                    elems = self.wd.find_element(By.XPATH, self.img_xpath.format(c_pic)).get_attribute('src')
                                    image_url = elems.replace('width=100&aspect_ratio=1:1&auto_optimize=low','class=doptimized')
                                    file_name = str(c_pic) + ".jpg"
                                    try:
                                        image_content = requests.get(image_url).content
                                        image_file = io.BytesIO(image_content)
                                        image = Image.open(image_file)
                                        file_path = save_path +'/' + file_name

                                        img_path.append(file_path)

                                        with open(file_path, "wb") as f:
                                            image.save(f, "PNG")

                                        print("Success")

                                    except Exception as e:
                                        print('FAILED -', e)
                            except:
                                self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div/div[1]/div[2]/div[2]/ul/li[2]').click()
                                soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                                n_p = soup.find('span',{'class':'css-15dx7ok'}).text.split('/')[-1]
                                print(n_p)

                                for c_p in range(int(n_p)):
                                    c_p+=1
                                    elems = self.wd.find_element(By.XPATH, self.imgclick_xpath.format(c_p)).get_attribute('src')
                                    image_url = elems.replace('width=100&aspect_ratio=1:1&auto_optimize=low','class=doptimized')
                                    file_name = str(c_p) + ".jpg"
                                    try:
                                        image_content = requests.get(image_url).content
                                        image_file = io.BytesIO(image_content)
                                        image = Image.open(image_file)
                                        file_path = save_path +'/' + file_name

                                        img_path.append(file_path)

                                        with open(file_path, "wb") as f:
                                            image.save(f, "PNG")

                                        print("Success")

                                    except Exception as e:
                                        print('FAILED -', e)
                        except:
                            img_path.append('no picture')


                        raw_data_home = {
                            'cut_find_web': 'renthub',
                            'cut_find_url': ltr_post_url,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'เช่า',
                            'propertyTypes': 'อพาร์ทเม้นท์',
                            'totalBedroom': 'no data',
                            'totalToilet': 'no data',
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'minimumArea': 'no data',
                            'minimumArea': 'no data',
                            'description': find_description,
                            'cut_find_location': cut_find_location,
                            'cut_find_map': find_map,
                            'latitude' : lat,
                            'longitude' : long,
                        }
                        raw_data_homeu = {
                            'cut_find_web': 'renthub',
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'เช่า',
                            'propertyTypes': 'อพาร์ทเม้นท์',
                            'totalBedroom': 'no data',
                            'totalToilet': 'no data',
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'minimumArea': 'no data',
                            'minimumArea': 'no data',
                            'description': find_description,
                            'cut_find_location': cut_find_location,
                            'cut_find_map': find_map,
                            'latitude' : lat,
                            'longitude' : long,
                        }
                        def update_data_mongo(ltr_post_url ,raw_data_homeu):
                            myquery = {"cut_find_url": ltr_post_url}
                            mydoc = self.mycol.find(myquery)

                            update_data = {"$set":raw_data_homeu}

                            self.mycol.update_one(myquery,update_data)
                        print(raw_data_home)
                        try:
                            scraperenthub.insert_data_mongo(raw_data_home)
                        except:
                            update_data_mongo(ltr_post_url ,raw_data_homeu)

                    except:
                        raw_data_home = {
                            'cut_find_web': 'renthub',
                            'cannot_open_url': ltr_post_url,
                        }
                        print(raw_data_home)
                        scraperenthub.insert_data_mongo(raw_data_home)
                except:
                    raw_data_home = {
                        'cut_find_web': 'renthub',
                        'cannot_open_url': ltr_post_url,
                    }
                    print(raw_data_home)
                    scraperenthub.insert_data_mongo(raw_data_home)
            one_page()


    def get_str(self):
        hpp_url = "https://www.renthub.in.th{}"
        # cpage = 1

        for s_url in self.str_urls:
            self.wd.get(s_url)
            soup = BeautifulSoup(self.wd.page_source, 'html.parser')
            try:
                find_page = soup.find_all("a",{"role":"button"})
                last_page = find_page[-2].text
                int_last_page = int(last_page)
                print(int_last_page)
            except:
                int_last_page = 1
            try:
                self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[3]/button').click()
            except:
                print('no pop up')

            for cpage in range(int_last_page):
                print('page',cpage+1)
                soup = BeautifulSoup(self.wd.page_source, 'html.parser')

                for apartment_post in soup.find_all("a",{'class':'css-10i63lj'}):
                    self.str_post_urls.append(hpp_url.format(apartment_post['href']))
                    print(hpp_url.format(apartment_post['href']))

                try:
                    if cpage < int_last_page-1:
                        self.wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "ถัดไป")))

                        try:
                            scraperenthub.click()
                        except:
                            time.sleep(2)
                            print('c2')
                            scraperenthub.click()
                        print(cpage+1,'to',cpage+2,'of',int_last_page) 
                # print(self.ltr_post_urls)
                except:
                    print('1page')

        print('finish self.str_post_urls.append')

        for str_post_url in self.str_post_urls:
            details = []
            img_path = []

            print(str_post_url)


            def one_page():

                try:

                    self.wd.get(str_post_url)

                    soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                    try:
                        
                        find_h1 = soup.find("h1").text
                        print(find_h1)
                    except:
                        self.wd.get(str_post_url)
                        soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                        find_h1 = soup.find('h1').text
                        print(find_h1)
                    try:

                        find_url = str_post_url
                        find_name = soup.find("h1")
                        find_location = soup.find("span",{"class":"css-kbs8tw"})
                        try:
                            find_price = soup.find("div",{"class":"price css-k5exox e16r65b90"}).text
                        except:
                            find_price = soup.find("div",{"class":"price css-k5exox e16r65b90"})
                        find_details = soup.find_all("li",{"class":"css-7fau71 e4yj7n60"})

                        ##findmap
                        start_tab = self.wd.current_window_handle
                        self.wd.execute_script("window.open('');")
                        self.wd.switch_to.window(self.wd.window_handles[1])#เปลี่ยนแท็ป
                        self.wd.get('https://www.google.co.th/maps')
                        self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(find_name.text)
                        self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
                        print('clicksearch')
                        time.sleep(3)
                        find_map = self.wd.current_url
                        print(find_map)
                        try:
                            latlong = find_map.split('/@')[1].split('/')[0].split(',')[:2]
                            lat = latlong[0]
                            long = latlong[1]
                        except:
                            lat = find_map
                            long = find_map
                        self.wd.close()
                        self.wd.switch_to.window(self.wd.window_handles[0])#เปลี่ยนแท็ป

                        if len(find_name) == 1:
                            print("------------------------------------------")
                            cut_find_name = find_name.text
                            cut_find_location = find_location.text
                            cut_find_price = find_price
                            cut_find_details = find_details[0].text.strip()
                            try:
                                find_description = soup.find("div",{"class":"css-zwxq9d"}).text
                            except:
                                find_description = 'no description'
                            # cut_find_bedroom = find_bedroom[1].text
                            # cut_find_bathroom = '\n'+find_bathroom[3].text.replace('\n                                                              ','').replace('\n',' ').replace('                                                           ','')
                            # cut_find_map = '\n'+find_map

                            # print(type(find_details))

                            for detail in find_details:
                                # print(type(detail))#.text)
                                detail_datas = {
                                    detail.text.split(':')[0]:detail.text.split(':')[1]
                                }

                                # print(datas)
                                details.append(detail_datas)

                        else:
                            print('error')

                        p_id = soup.find('span',{'class': 'css-1qahy2z'}).text.split(' : ')[1]
                        save_path = self.sys_xpath.format(p_id)
                        print(save_path)
                        try:
                            os.mkdir(save_path, self.mode)
                            os.chdir(os.path.join(os.getcwd(), save_path))
                        except:
                            os.chdir(os.path.join(os.getcwd(), save_path))

                        try:
                            try:
                                n_pic = soup.find('span',{'class':'css-15dx7ok'}).text.split('/')[-1]
                                for c_pic in range(int(n_pic)):
                                    c_pic+=1
                                    elems = self.wd.find_element(By.XPATH, self.img_xpath.format(c_pic)).get_attribute('src')
                                    image_url = elems.replace('width=100&aspect_ratio=1:1&auto_optimize=low','class=doptimized')
                                    file_name = str(c_pic) + ".jpg"
                                    try:
                                        image_content = requests.get(image_url).content
                                        image_file = io.BytesIO(image_content)
                                        image = Image.open(image_file)
                                        file_path = save_path +'/' + file_name

                                        img_path.append(file_path)

                                        with open(file_path, "wb") as f:
                                            image.save(f, "PNG")

                                        print("Success")

                                    except Exception as e:
                                        print('FAILED -', e)
                            except:
                                self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div/div[1]/div[2]/div[2]/ul/li[2]').click()
                                soup = BeautifulSoup(self.wd.page_source, 'html.parser')
                                n_p = soup.find('span',{'class':'css-15dx7ok'}).text.split('/')[-1]
                                print(n_p)

                                for c_p in range(int(n_p)):
                                    c_p+=1
                                    elems = self.wd.find_element(By.XPATH, self.imgclick_xpath.format(c_p)).get_attribute('src')
                                    image_url = elems.replace('width=100&aspect_ratio=1:1&auto_optimize=low','class=doptimized')
                                    file_name = str(c_p) + ".jpg"
                                    try:
                                        image_content = requests.get(image_url).content
                                        image_file = io.BytesIO(image_content)
                                        image = Image.open(image_file)
                                        file_path = save_path +'/' + file_name

                                        img_path.append(file_path)

                                        with open(file_path, "wb") as f:
                                            image.save(f, "PNG")

                                        print("Success")

                                    except Exception as e:
                                        print('FAILED -', e)
                        except:
                            img_path.append('no picture')
                        

                        raw_data_home = {
                            'cut_find_web': 'renthub',
                            'cut_find_url': str_post_url,
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'เช่า',
                            'propertyTypes': 'ห้องเช่ารายวัน',
                            'totalBedroom': 'no data',
                            'totalToilet': 'no data',
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'minimumArea': 'no data',
                            'minimumArea': 'no data',
                            'description': find_description,
                            'cut_find_location': cut_find_location,
                            'cut_find_map': find_map,
                            'latitude' : lat,
                            'longitude' : long,
                        }
                        raw_data_homeu = {
                            'cut_find_web': 'renthub',
                            'galleryUrls': img_path,
                            'name': cut_find_name,
                            'listType': 'เช่า',
                            'propertyTypes': 'ห้องเช่ารายวัน',
                            'totalBedroom': 'no data',
                            'totalToilet': 'no data',
                            'startingPrice': cut_find_price,
                            'finalPrice': cut_find_price,
                            'minimumArea': 'no data',
                            'minimumArea': 'no data',
                            'description': find_description,
                            'cut_find_location': cut_find_location,
                            'cut_find_map': find_map,
                            'latitude' : lat,
                            'longitude' : long,
                        }
                        print(raw_data_home)
                        def update_data_mongo(str_post_url ,raw_data_homeu):
                            myquery = {"cut_find_url": str_post_url}
                            mydoc = self.mycol.find(myquery)

                            update_data = {"$set":raw_data_homeu}

                            self.mycol.update_one(myquery,update_data)
                        print(raw_data_home)
                        try:
                            scraperenthub.insert_data_mongo(raw_data_home)
                        except:
                            update_data_mongo(str_post_url ,raw_data_homeu)

                    except:
                        raw_data_home = {
                            'cut_find_web': 'renthub',
                            'cannot_open_url': str_post_url,
                        }
                        print(raw_data_home)
                        scraperenthub.insert_data_mongo(raw_data_home)

                except:
                    raw_data_home = {
                        'cut_find_web': 'renthub',
                        'cannot_open_url': str_post_url,
                    }
                    print(raw_data_home)
                    scraperenthub.insert_data_mongo(raw_data_home)
            one_page()

    def quit(self):
        self.wd.quit()
class startrenthub:
    def startrenthub(self):
        frh = scraperenthub()
        print('frh = scraperenthub()')
        try:
            frh.get_urls()
            frh.get_ltr()
            frh.get_str()
            # get_project_url()
            frh.quit()
            print('finish')
            return True

        except Exception as e:
            print("Error",e)
            frh.quit()
            return False

# get_urls()
# get_ltr()
# get_str()
# # get_project_url()
# self.wd.quit()
# print('finish')