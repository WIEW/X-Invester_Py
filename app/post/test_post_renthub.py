from pickle import GLOBAL
import requests
from bs4 import BeautifulSoup
import csv
import itertools
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait,Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo
from collections import ChainMap
from bson.objectid import ObjectId
import pyperclip
import random

################################################################

class renthub:
    def __init__(self):

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        # options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        options.add_argument(f'user-agent={user_agent}')
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)


        self.wd.maximize_window()
        ####url################################
        url = "https://www.renthub.in.th/"

        self.wd.get(url)
        self.wait = WebDriverWait(self.wd, 10)
        ####url################################

        ##Start of db zone###
        # self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        self.mydb = self.myclient["devXInvester"]
        self.mycol = self.mydb["posts"]
        self.mycol_properties = self.mydb["properties"]
        self.mycol_uw = self.mydb["websites"]

        # self.mydb = self.myclient["test_X-invester"]
        # self.mycol = self.mydb["test_X-invester_ppth_newlo2"]

        self.i=0
        self.cpost = 1
        self.n_pic = 1
        self.post_topic = []
        self.list_type = []
        self.bedroom = []
        self.toilet = []
        self.s_price = []
        self.f_price = []
        self.min_area = []
        self.max_area = []
        self.description = []
        self.property_type = []
        self.watchlistPropertyTypes =[]
        self.property = []
        self.imgs = []
        self.state = []
        self.city = []
        self.subcity = []
        self.address = []
        self.addressEN = []
        self.imgs_path = []
        self.img_ad_path = []
        self.img_and_path = []

        self.JS_ADD_TEXT_TO_INPUT = """
            var elm = arguments[0], txt = arguments[1];
            elm.value += txt;
            elm.dispatchEvent(new Event('change'));
            """
        self.province_xpath = '//*[text()="{}"]'
        self.district_xpath = '//*[text()="{}"]'
        self.ubdistrict_xpath = '//*[text()="{}"]'
        # ad_title = ['test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718']
##########################query################################################################
    def query(self,id):
        print(id)
        myquery = {"_id":ObjectId(id)}

        mydoc = self.mycol.find(myquery)
        global galleryUrls
        global ImgsPath

        def lookup():
            stage_lookup_property = {
            "$lookup": {
            "from": "properties", 
            "localField": "property", 
            "foreignField": "_id", 
            "as": "data_property",
            }
            }

            stage_lookup_subCity = {
            "$lookup": {
            "from": "subCities", 
            "localField": "subCity", 
            "foreignField": "_id", 
            "as": "data_subCity",
            }
            }
            stage_lookup_city = {
            "$lookup": {
            "from": "cities", 
            "localField": "city", 
            "foreignField": "_id", 
            "as": "data_city",
            }
            }
            stage_lookup_state = {
            "$lookup": {
            "from": "states", 
            "localField": "state", 
            "foreignField": "_id", 
            "as": "data_state",
            }
            }
            # Limit to the first 5 documents:
            stage_limit_5 = { "$limit": 1 }

            pipeline_property = [
            stage_lookup_property,
            #    stage_add_comment_count,
            #    stage_match_with_comments,
            stage_limit_5,
            ]

            pipeline_subCity = [
            stage_lookup_subCity,
            #    stage_add_comment_count,
            #    stage_match_with_comments,
            stage_limit_5,
            ]
            pipeline_city = [
            stage_lookup_city,
            #    stage_add_comment_count,
            #    stage_match_with_comments,
            stage_limit_5,
            ]
            pipeline_state = [
            stage_lookup_state,
            #    stage_add_comment_count,
            #    stage_match_with_comments,
            stage_limit_5,
            ]

            # self.properties = self.mycol.aggregate(pipeline_property)
            # self.subCities = self.mycol_properties.aggregate(pipeline_subCity)
            # self.Cities = self.mycol_properties.aggregate(pipeline_city)
            # self.States = self.mycol_properties.aggregate(pipeline_state)

            # for property in self.properties:
            #     # pprint(type(property))
            #     data_property = property.get('data_property')
            #     data_property_dict = (dict(ChainMap(*data_property)))
            #     nameTH = data_property_dict.get('nameTH')
            #     self.address.append(nameTH)
            #     # city = data_property_dict.get('city')
            #     for subCity in self.subCities:
            #         # print(type(subCity))
            #         data_subCity = subCity.get('data_subCity')
            #         data_subCity_dict = (dict(ChainMap(*data_subCity)))
            #         subCity = data_subCity_dict.get('name')
            #         print(subCity)
            #         self.subcity.append(subCity)
            #         # print((subCity))
            #     for City in self.Cities:
            #         data_city = City.get('data_city')
            #         data_city_dict = (dict(ChainMap(*data_city)))
            #         City = data_city_dict.get('name')
            #         print(City)
            #         self.city.append(City)
            #     for State in self.States:
            #         data_State = State.get('data_state')
            #         data_State_dict = (dict(ChainMap(*data_State)))
            #         State = data_State_dict.get('name')
            #         print(State)
            #         self.state.append(State)
            print(id)


        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')
                Img_ad_Path = x.get('img_ad_path')
                Img_and_Path = x.get('img_and_path')

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)
                self.img_ad_path.append(Img_ad_Path)
                self.img_and_path.append(Img_and_Path)



            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')
                Img_ad_Path = x.get('img_ad_path')
                Img_and_Path = x.get('img_and_path')
                self.img_ad_path.append(Img_ad_Path)
                self.img_and_path.append(Img_and_Path)

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)
                Img_ad_Path = x.get('img_ad_path')
                Img_and_Path = x.get('img_and_path')
                self.img_ad_path.append(Img_ad_Path)
                self.img_and_path.append(Img_and_Path)

        mycol_properties_query = {"_id":ObjectId(self.property[0])}
        mydoc_mycol_properties = self.mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('district')
            Province = x.get('province')
            print(nameEN,nameTH,City)

            self.address.append(nameTH)
            self.addressEN.append(nameEN)
            self.city.append(City)
            self.state.append(Province)


        def pnt():
            print('pnt',self.address)
        pnt()
        # lat_long.append(Latitude)
        # lat_long.append(Longitude)
        # if ['rent'] in self.list_type or ['เช่า'] in self.list_type:


    ##########################query################################################################
    def query_uw(self):
        myquery = {'name':'renthub'}
        myuws = self.mycol_uw.find(myquery)
        for uws in myuws:
            users = uws['users']

        global user
        global passwd

        user = users[0]['username']
        passwd = users[0]['password']

        r = random.choice(users)

        u = r['username']
        p = r['password']

    ##End of db zone###
    ################################################################
    ##########################click_login################################################################
    def click_login(self):
        self.wd.implicitly_wait(10)
        self.wd.get('https://dashboard.renthub.in.th/login')
        print("Click Login_Button")

    # click_login()

    ##########################click_login##########################################
    ###############################fill_login##########################################
    def fill_login(self):
        self.wd.implicitly_wait(0.5)
        # self.wait.until(EC.element_to_be_clickable((By.NAME, 'email'))).send_keys("jatossob@gmail.com")
        # self.wait.until(EC.element_to_be_clickable((By.NAME, 'email'))).send_keys("fahfah.prapapan@gmail.com")
        self.wait.until(EC.element_to_be_clickable((By.NAME, 'email'))).send_keys(user)

        # self.wait.until(EC.element_to_be_clickable((By.NAME, "password"))).send_keys("qwerty123456Zx")
        # self.wait.until(EC.element_to_be_clickable((By.NAME, "password"))).send_keys("Prapapan1")
        self.wait.until(EC.element_to_be_clickable((By.NAME, "password"))).send_keys(passwd)

        link = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[2]/div/form/div/button')
        link.click()
        print("Click Login_Button finish")

    ###############################fill_login##########################################

    #######################click_createpost#######################################
    def click_createpost(self):
        time.sleep(5)
        self.wd.get('https://dashboard.renthub.in.th/members/listings/new')
        
    ########################click_createpost######################################

    ################################fill_data####################################
    def fill_data(self):
        time.sleep(5)
        # close_button = self.wd.find_element(By.XPATH, '/html/body/reach-portal/div/div/div[2]/div/div/form/button')
        # close_button.click()
        # print("close_announce_button")

    #################ข้อมูลที่พัก#######################################
        self.wait.until(EC.element_to_be_clickable((By.NAME, "name.th")))#.self.post_topic)

        def headtitle_th():
            headtitle_th = self.wd.find_element(By.NAME, "name.th")
            headtitle_th.send_keys(self.post_topic[0])
            # pyperclip.copy(str(self.post_topic[0]))
            # act = ActionChains(self.wd)
            # act.send_keys_to_element(headtitle_th).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
            # self.wd.find_element(By.NAME,'name.th').send_keys(4)

        headtitle_th()

        def headtitle_en():
            headtitle_en = self.wd.find_element(By.NAME,'title.en')
            pyperclip.copy(str(self.post_topic[0]))
            act = ActionChains(self.wd)
            act.send_keys_to_element(headtitle_en).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
            self.wd.find_element(By.NAME,'name.en').send_keys(4)
        # headtitle_en()
        # # self.wait.until(EC.element_to_be_clickable((By.NAME, "name.th"))).send_keys('ประกาศขาย 26/8/65')
        # self.wait.until(EC.element_to_be_clickable((By.NAME, "name.en"))).self.post_topic)
        # # self.wait.until(EC.element_to_be_clickable((By.NAME, "name.en"))).send_keys('sell post at 26/8/65')
        self.wait.until(EC.element_to_be_clickable((By.NAME, "houseNumber.th"))).send_keys("0123")
        self.wait.until(EC.element_to_be_clickable((By.NAME, "road.th"))).send_keys("ถนนกก")
        self.wait.until(EC.element_to_be_clickable((By.NAME, "street.th"))).send_keys("ซอยกก")
        # self.wait.until(EC.element_to_be_clickable((By.NAME, "houseNumber.en"))).send_keys("test(English)12345657891011121311415161718")
        # self.wait.until(EC.element_to_be_clickable((By.NAME, "road.en"))).send_keys("test(English)12345657891011121311415161718")
        # self.wait.until(EC.element_to_be_clickable((By.NAME, "street.en"))).send_keys("test(English)12345657891011121311415161718")

    #จังหวัด
        self.wd.find_element(By.XPATH,'//*[@class = "css-1k30fl1"][1]').click()

        # self.wd.find_element(By.XPATH,'//span[contains(text(),"เลือกจังหวัด")]').click()

        # self.wd.find_element(By.XPATH,'//*[@id="button--listbox-input--2"]').click()
        def list_provinces():
            KBI=self.wd.find_element(By.XPATH,'//*[@id="option-81--listbox-input--2"]')#กระบี่
            BKK=self.wd.find_element(By.XPATH,'//*[@id="option-10--listbox-input--2"]')#กรุงเทพมหานคร
            KRI=self.wd.find_element(By.XPATH,'//*[@id="option-71--listbox-input--2"]')#กาญจนบุรี
            KSN=self.wd.find_element(By.XPATH,'//*[@id="option-46--listbox-input--2"]')#กาฬสินธุ์
            KPT=self.wd.find_element(By.XPATH,'//*[@id="option-62--listbox-input--2"]')#กำแพงเพชร
            KKN=self.wd.find_element(By.XPATH,'//*[@id="option-40--listbox-input--2"]')#ขอนแก่น
            CTI=self.wd.find_element(By.XPATH,'//*[@id="option-22--listbox-input--2"]')#จันทบุรี
            CCO=self.wd.find_element(By.XPATH,'//*[@id="option-24--listbox-input--2"]')#ฉะเชิงเทรา
            CBI=self.wd.find_element(By.XPATH,'//*[@id="option-20--listbox-input--2"]')#ชลบุรี
            CNT=self.wd.find_element(By.XPATH,'//*[@id="option-18--listbox-input--2"]')#ชัยนาท
            CPM=self.wd.find_element(By.XPATH,'//*[@id="option-36--listbox-input--2"]')#ชัยภูมิ
            CRI=self.wd.find_element(By.XPATH,'//*[@id="option-57--listbox-input--2"]')#เชียงราย
            CMI=self.wd.find_element(By.XPATH,'//*[@id="option-50--listbox-input--2"]')#เชียงใหม่
            CPN=self.wd.find_element(By.XPATH,'//*[@id="option-86--listbox-input--2"]')#ชุมพร
            TRG=self.wd.find_element(By.XPATH,'//*[@id="option-92--listbox-input--2"]')#ตรัง
            TRT=self.wd.find_element(By.XPATH,'//*[@id="option-23--listbox-input--2"]')#ตราด
            TAK=self.wd.find_element(By.XPATH,'//*[@id="option-63--listbox-input--2"]')#ตาก
            NYK=self.wd.find_element(By.XPATH,'//*[@id="option-26--listbox-input--2"]')#นครนายก
            NPT=self.wd.find_element(By.XPATH,'//*[@id="option-73--listbox-input--2"]')#นครปฐม
            NPM=self.wd.find_element(By.XPATH,'//*[@id="option-48--listbox-input--2"]')#นครพนม
            NMA=self.wd.find_element(By.XPATH,'//*[@id="option-30--listbox-input--2"]')#นครราชสีมา
            NST=self.wd.find_element(By.XPATH,'//*[@id="option-80--listbox-input--2"]')#นครศรีธรรมราช
            NSN=self.wd.find_element(By.XPATH,'//*[@id="option-60--listbox-input--2"]')#นครสวรรค์
            NBI=self.wd.find_element(By.XPATH,'//*[@id="option-12--listbox-input--2"]')#นนทบุรี
            NWT=self.wd.find_element(By.XPATH,'//*[@id="option-96--listbox-input--2"]')#นราธิวาส
            NAN=self.wd.find_element(By.XPATH,'//*[@id="option-55--listbox-input--2"]')#น่าน
            BRM=self.wd.find_element(By.XPATH,'//*[@id="option-31--listbox-input--2"]')#บุรีรัมย์
            BKN=self.wd.find_element(By.XPATH,'//*[@id="option-38--listbox-input--2"]')#บึงกาฬ
            PTE=self.wd.find_element(By.XPATH,'//*[@id="option-13--listbox-input--2"]')#ปทุมธานี
            PKN=self.wd.find_element(By.XPATH,'//*[@id="option-77--listbox-input--2"]')#ประจวบคีรีขันธ์
            PRI=self.wd.find_element(By.XPATH,'//*[@id="option-25--listbox-input--2"]')#ปราจีนบุรี
            PTN=self.wd.find_element(By.XPATH,'//*[@id="option-94--listbox-input--2"]')#ปัตตานี
            PBI=self.wd.find_element(By.XPATH,'//*[@id="option-76--listbox-input--2"]')#เพชรบุรี
            PNB=self.wd.find_element(By.XPATH,'//*[@id="option-67--listbox-input--2"]')#เพชรบูรณ์
            AYA=self.wd.find_element(By.XPATH,'//*[@id="option-14--listbox-input--2"]')#พระนครศรีอยุธยา
            PRE=self.wd.find_element(By.XPATH,'//*[@id="option-54--listbox-input--2"]')#แพร่
            PYO=self.wd.find_element(By.XPATH,'//*[@id="option-56--listbox-input--2"]')#พะเยา
            PNA=self.wd.find_element(By.XPATH,'//*[@id="option-82--listbox-input--2"]')#พังงา
            PLG=self.wd.find_element(By.XPATH,'//*[@id="option-93--listbox-input--2"]')#พัทลุง
            PCT=self.wd.find_element(By.XPATH,'//*[@id="option-66--listbox-input--2"]')#พิจิตร
            PLK=self.wd.find_element(By.XPATH,'//*[@id="option-65--listbox-input--2"]')#พิษณุโลก
            PKT=self.wd.find_element(By.XPATH,'//*[@id="option-83--listbox-input--2"]')#ภูเก็ต
            MKM=self.wd.find_element(By.XPATH,'//*[@id="option-44--listbox-input--2"]')#มหาสารคาม
            MDH=self.wd.find_element(By.XPATH,'//*[@id="option-49--listbox-input--2"]')#มุกดาหาร
            MSN=self.wd.find_element(By.XPATH,'//*[@id="option-58--listbox-input--2"]')#แม่ฮ่องสอน
            YST=self.wd.find_element(By.XPATH,'//*[@id="option-35--listbox-input--2"]')#ยโสธร
            YLA=self.wd.find_element(By.XPATH,'//*[@id="option-95--listbox-input--2"]')#ยะลา
            RET=self.wd.find_element(By.XPATH,'//*[@id="option-45--listbox-input--2"]')#ร้อยเอ็ด
            RNG=self.wd.find_element(By.XPATH,'//*[@id="option-85--listbox-input--2"]')#ระนอง
            RYG=self.wd.find_element(By.XPATH,'//*[@id="option-21--listbox-input--2"]')#ระยอง
            RBR=self.wd.find_element(By.XPATH,'//*[@id="option-70--listbox-input--2"]')#ราชบุรี
            LRI=self.wd.find_element(By.XPATH,'//*[@id="option-16--listbox-input--2"]')#ลพบุรี
            LEI=self.wd.find_element(By.XPATH,'//*[@id="option-42--listbox-input--2"]')#เลย
            LPG=self.wd.find_element(By.XPATH,'//*[@id="option-52--listbox-input--2"]')#ลำปาง
            LPN=self.wd.find_element(By.XPATH,'//*[@id="option-51--listbox-input--2"]')#ลำพูน
            SSK=self.wd.find_element(By.XPATH,'//*[@id="option-33--listbox-input--2"]')#ศรีสะเกษ
            SNK=self.wd.find_element(By.XPATH,'//*[@id="option-47--listbox-input--2"]')#สกลนคร
            SKA=self.wd.find_element(By.XPATH,'//*[@id="option-90--listbox-input--2"]')#สงขลา
            STN=self.wd.find_element(By.XPATH,'//*[@id="option-91--listbox-input--2"]')#สตูล
            SPK=self.wd.find_element(By.XPATH,'//*[@id="option-11--listbox-input--2"]')#สมุทรปราการ
            SKM=self.wd.find_element(By.XPATH,'//*[@id="option-75--listbox-input--2"]')#สมุทรสงคราม
            SKN=self.wd.find_element(By.XPATH,'//*[@id="option-74--listbox-input--2"]')#สมุทรสาคร
            SKW=self.wd.find_element(By.XPATH,'//*[@id="option-27--listbox-input--2"]')#สระแก้ว
            SRI=self.wd.find_element(By.XPATH,'//*[@id="option-19--listbox-input--2"]')#สระบุรี
            SBR=self.wd.find_element(By.XPATH,'//*[@id="option-17--listbox-input--2"]')#สิงห์บุรี
            STI=self.wd.find_element(By.XPATH,'//*[@id="option-64--listbox-input--2"]')#สุโขทัย
            SPB=self.wd.find_element(By.XPATH,'//*[@id="option-72--listbox-input--2"]')#สุพรรณบุรี
            SNI=self.wd.find_element(By.XPATH,'//*[@id="option-84--listbox-input--2"]')#สุราษฎร์ธานี
            SRN=self.wd.find_element(By.XPATH,'//*[@id="option-32--listbox-input--2"]')#สุรินทร์
            NKI=self.wd.find_element(By.XPATH,'//*[@id="option-43--listbox-input--2"]')#หนองคาย
            NBP=self.wd.find_element(By.XPATH,'//*[@id="option-39--listbox-input--2"]')#หนองบัวลำภู
            ATG=self.wd.find_element(By.XPATH,'//*[@id="option-15--listbox-input--2"]')#อ่างทอง
            ACR=self.wd.find_element(By.XPATH,'//*[@id="option-37--listbox-input--2"]')#อำนาจเจริญ
            UDN=self.wd.find_element(By.XPATH,'//*[@id="option-41--listbox-input--2"]')#อุดรธานี
            UTT=self.wd.find_element(By.XPATH,'//*[@id="option-53--listbox-input--2"]')#อุตรดิตถ์
            UTI=self.wd.find_element(By.XPATH,'//*[@id="option-61--listbox-input--2"]')#อุทัยธานี
            UBN=self.wd.find_element(By.XPATH,'//*[@id="option-34--listbox-input--2"]')#อุบลราชธานี

        # if province == 'กรุงเทพมหานคร':
        #     BKK.click()
        self.wd.find_element(By.XPATH,self.province_xpath.format(self.state[0])).click()
        
    #จังหวัด
    #อำเภอ//*[@id="button--listbox-input--154"]
        # self.wd.find_element(By.XPATH,'//span[contains(text(),"เขต")]').click()
        # self.wd.find_element(By.CLASS_NAME,'css-1k30fl1').click()
        # self.wd.find_element(By.XPATH,'//*[@html-for="districtCode"]/div[1]').click()
        # select_district = self.wd.find_element(By.XPATH,'//span[contains(text(),"เลือกจังหวัด")]').send_keys(Keys.TAB)
        time.sleep(1)
        self.wd.find_element(By.XPATH,'/html/body/div[1]/div[2]/div/div[1]/form/div/div[1]/div/div[3]/div[8]/div/div/span').click()
    #//*[@class = "css-1g2auqy]/div[8]/div/div/span
    #//*[@id="districtCode"]/div/span
        time.sleep(1)
        self.wd.find_element(By.XPATH,self.district_xpath.format(self.city[0])).click()

        # time.sleep(30)
        # self.wd.find_element(By.XPATH,'//ul[li/@data-value=1001]').click()

        # self.wd.find_element(By.XPATH,'//li[contains(text(),"ดุสิต")]').click()

    #อำเภอ
    #ตำบล
        time.sleep(2)
        self.wd.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/form/div/div[1]/div/div[3]/div[9]/div/div/span').click()
        time.sleep(2)
        self.wd.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/form/div/div[1]/div/div[3]/div[9]/div/div/div[2]/ul').click()

        # self.wd.find_element(By.XPATH,'//span[contains(text(),"เลือกตำบล")]').click()
        # # self.wd.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/form/div/div[1]/div/div[3]/div[9]/div/div/span').click()
        # try:
        #     self.wd.find_element(By.XPATH,subdistrict_xpath.format(city[0])).click()
        # except:
        #     self.wd.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/form/div/div[1]/div/div[3]/div[9]/div/div/div[2]/ul').click()

        # self.wd.find_element(By.XPATH,'//li[contains(text(),"ดุสิต")]').click()

    #ตำบล
        # time.sleep(5)

        # self.wait.until(EC.element_to_be_clickable((By.NAME, "postCode"))).send_keys("10001")
        self.wd.find_element(By.XPATH,'//*[@id="__next"]/div[2]/div/div[1]/form/div/div[1]/div/div[3]/div[11]/div[1]/button').click()
    #################ข้อมูลที่พัก#######################################
    #################สิ่งอำนวยควมสะดวก#######################################
        def facilities():
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasAir"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasAir"]').click()

            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasFurniture"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.allowPet"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasWaterHeater"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasFan"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasTV"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasRefrigerator"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.allowSmoking"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasPhone"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasParking"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasBicycleParking"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasLift"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasPool"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasFitness"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasInternet"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasCableTV"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasKeyCardAccess"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasFingerPrintAccess"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasCCTV"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasSecurity"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasRestaurant"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasShop"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasLaundry"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasSalon"]').click()
            self.wd.find_element(By.XPATH, '//*[@id="amenities.hasEvCharger"]').click()

    #################สิ่งอำนวยควมสะดวก#######################################
    ##########################################check_ifรูปแบบห้อง#########################################################
        if self.property_type[0] == [ObjectId('62bd7104f0521b8890fe9a0f')]:
            self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[3]/div/table/tbody/tr/td[1]/div[1]/div/input').send_keys('คอนโด')
        else:
            self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[3]/div/table/tbody/tr/td[1]/div[1]/div/input').send_keys('ที่พัก')

        print(self.property_type)
        print(self.bedroom)
        
        self.wd.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div[1]/form/div/div[3]/div/table/tbody/tr/td[2]/div/div/span').click()
        if self.property_type[0] == ObjectId('62c3b1f6dbf2ee974adf6a03'):
            self.wd.find_element(By.XPATH, '//*[@id="option-STUDIO--listbox-input--5"]').click()#
            print("if property_type == ['สตูดิโอ']:")
        elif self.bedroom[0] == 1 or self.bedroom[0] == ['1 ห้อง']:
            self.wd.find_element(By.XPATH, '//*[@id="option-ONE_BED_ROOM--listbox-input--5"]').click()#
            print("[1] in self.bedroom[0]:")
        elif self.bedroom[0] == 2 or self.bedroom[0] == ['2 ห้อง']:
            self.wd.find_element(By.XPATH, '//*[@id="option-TWO_BED_ROOM--listbox-input--5"]').click()#
            print("[2] in self.bedroom[0]:")
        elif self.bedroom[0] == 3 or self.bedroom[0] == ['3 ห้อง']:
            self.wd.find_element(By.XPATH, '//*[@id="option-THREE_BED_ROOM--listbox-input--5"]').click()#
            print("[3] in self.bedroom[0]:")
        elif self.bedroom[0] == 4 or self.bedroom[0] == ['4 ห้อง']:
            self.wd.find_element(By.XPATH, '//*[@id="option-FOUR_BED_ROOM--listbox-input--5"]').click()#
            print("[4] in self.bedroom[0]:")
        elif self.bedroom[0] == 5 or self.bedroom[0] == ['5 ห้อง']:
            self.wd.find_element(By.XPATH, '//*[@id="option-FIVE_BED_ROOM--listbox-input--5"]').click()#
            print("[5] in self.bedroom[0]:")
        else:
            self.wd.find_element(By.XPATH, '//*[@id="option-STUDIO--listbox-input--5"]').click()#
            print("[else] in self.bedroom:")


    ##########################################################################################################
    ##########################################check_ifขนาดห้อง#########################################################
        print(self.max_area[0])
        try:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "rooms[0].minSize"))).send_keys(self.max_area[0].split(' ')[0])
        except:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "rooms[0].minSize"))).send_keys(self.max_area[0])


    ##########################################check_ifขนาดห้อง#########################################################
    ##########################################check_ifเช่ารายเดือน (บาท/เดือน)#########################################################
        self.wd.find_element(By.ID, 'button--listbox-input--6').click()
        self.wait.until(EC.element_to_be_clickable((By.ID, 'option-AMOUNT--listbox-input--6')))
        self.wd.find_element(By.ID, 'option-AMOUNT--listbox-input--6').click()

        print(self.s_price[0])
        try:
            try:
                self.wait.until(EC.element_to_be_clickable((By.NAME, "rooms[0].price.monthly.minPrice"))).send_keys(self.s_price[0].replace('ราคาเช่า','').replace('บาท/เดือน',''))
            except:
                self.wait.until(EC.element_to_be_clickable((By.NAME, "rooms[0].price.monthly.minPrice"))).send_keys(self.s_price[0])
        except:
            try:
                self.wait.until(EC.element_to_be_clickable((By.NAME, "rooms[0].price.monthly.minPrice"))).send_keys(self.f_price[0].replace('ราคาเช่า','').replace('บาท/เดือน',''))
            except:
                self.wait.until(EC.element_to_be_clickable((By.NAME, "rooms[0].price.monthly.minPrice"))).send_keys(self.f_price[0])

        try:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "rooms[0].price.monthly.maxPrice"))).send_keys(self.f_price[0].replace('ราคาเช่า','').replace('บาท/เดือน',''))
        except:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "rooms[0].price.monthly.maxPrice"))).send_keys(self.f_price[0])

    ##########################################check_ifเช่ารายเดือน (บาท/เดือน)#########################################################
    ##########################################check_ifเช่ารายวัน#########################################################
        self.wd.find_element(By.ID, 'button--listbox-input--7').click()
        self.wait.until(EC.element_to_be_clickable((By.ID, 'option-CALL--listbox-input--7')))
        self.wd.find_element(By.ID, 'option-CALL--listbox-input--7').click()
    ##########################################check_ifเช่ารายวัน#########################################################
    #################ค่าใช้จ่าย#######################################
    #ค่าน้ำ
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[4]/div/div[1]/label[6]/input').click()
    #ค่าไฟ
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[4]/div/div[2]/label[4]/input').click()
    #ค่าบริการอื่นๆ
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[4]/div/div[3]/label[3]/input').click()
    #เงินมัดจำ/ประกัน
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[4]/div/div[4]/label[4]/input').click()
    #จ่ายล่วงหน้า
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[4]/div/div[5]/label[4]/input').click()
    #ค่าโทรศัพท์
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[4]/div/div[6]/label[4]/input').click()
    #อินเทอร์เน็ต
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[4]/div/div[7]/label[4]/input').click()

    #################ค่าใช้จ่าย#######################################
    #################รายละเอียด#######################################
        # self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[5]/div/div[1]/div/div/div/div[2]/div[1]').send_keys(description[0])
        # self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[5]/div/div[2]/div/div/div/div[2]/div[1]').send_keys('test')
        def ad_title_th():
            print('ad_title_th')
            ad_title_th = self.wd.find_element(By.XPATH,'//*[@id="__next"]/div[2]/div/div[1]/form/div/div[5]/div/div[1]/div/div/div/div[2]/div[1]')
            ad_title_th.send_keys(self.description[0])
            # pyperclip.copy(str(self.description[0]))
            # act = ActionChains(self.wd)
            # act.send_keys_to_element(ad_title_th).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
        ad_title_th()

        def ad_title_en():
            ad_title_en = self.wd.find_element(By.XPATH,'//*[@id="__next"]/div[2]/div/div[1]/form/div/div[5]/div/div[1]/div/div/div/div[2]/div[1]')
            pyperclip.copy(str(self.description[0]))
            act = ActionChains(self.wd)
            act.send_keys_to_element(ad_title_en).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
        # ad_title_en()
    #################รายละเอียด#######################################
    #################รูปภาพ#######################################
        try:
            for n_img in range(int(len(galleryUrls))):
                self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[6]/div/div/input').send_keys(ImgsPath[n_img])
        except:
            print('no img insert')

    #################รูปภาพ#######################################
    #################โปรโมชั่น#######################################
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[7]/div/div[1]/label[3]/input').click()

    #################โปรโมชั่น#######################################
    #################ข้อมูลสำหรับติดต่อ#######################################
        self.wd.find_element(By.NAME, 'contactInformation[0].name').send_keys('test')
        self.wd.find_element(By.NAME, 'contactInformation[0].phone[0]').send_keys('029009000-4 ต่อ 0')
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/div[8]/div/div/div[3]/button').click()
        self.wd.find_element(By.NAME, 'contactInformation[0].phone[1]').send_keys('029009000-4 ต่อ 0')
        # self.wd.find_element(By.CLASS_NAME, 'css-89q0b2 e1661y350').click()
        # self.wd.find_element(By.NAME, 'contactInformation[0].phone[2]').send_keys('029009000-4 ต่อ 0')
        self.wd.find_element(By.NAME, 'contactInformation[0].email').send_keys('mailusedfortest@gmail.com')
        self.wd.find_element(By.NAME, 'contactInformation[0].lineId').send_keys('029009000-4 ต่อ 0')

    #################ข้อมูลสำหรับติดต่อ#######################################
    #################ลงประกาศ#######################################
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/button').click()
        time.sleep(1)
        self.wd.find_element(By.XPATH, '//*[@id="checkNameEn"]/div/div/label').click()

        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div/div[1]/form/div/button').click()
        print("click Posted")
        time.sleep(1)
        if self.wd.current_url == 'https://dashboard.renthub.in.th/members/listings':
            try:
                # for n_img in range(int(len(galleryUrls))):#เอกสารที่พัก
                self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div[2]/ul/li[1]/div/div[2]/div[3]/ul/li[1]/div[2]/form/div/div/input').send_keys(self.img_ad_path[0])
                print('เอกสารที่พัก')
            # for n_img in range(int(len(galleryUrls))):#รูปถ่ายเลขที่บ้าน
                self.wd.find_element(By.XPATH, '//*[@id="__next"]/div[2]/div[2]/ul/li[1]/div/div[2]/div[3]/ul/li[2]/div[2]/form/div/div/input').send_keys(self.img_and_path[0])
                print('รูปถ่ายเลขที่บ้าน')
                print("finish")
            except:
                print("finish")
            print("post")
        elif self.wd.current_url == 'https://dashboard.renthub.in.th/members/listings/new':
            print("cannot post")

    #################ลงประกาศ#######################################
    ################################fill_data####################################

        
    ###########################loop###############################################
    # for data in doc:
    #     clickpost()
    #     fill_data()

    ###########################loop###############################################
################################################################
    def quit(self):
        self.wd.quit()

class startrenthub:
    def startbotrenthub(self,id):
        try:
            fn = renthub()
            fn.click_login()
            fn.query(id)
            fn.query_uw()
            fn.fill_login()
            fn.click_createpost()
            fn.fill_data()
            fn.quit()
            return True

        except Exception as e:
            print(e)
            fn.quit()
            return False



################################################################

