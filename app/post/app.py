import requests
from flask import Flask,url_for
from test_post_lvisd import startlvisd
from test_post_ppth import startppth
from test_post_renthub import startrenthub
from test_post_thaihometown import starttht
from test_post_residences import startresidences
from dl_pic_f_api import update_path
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from bson.objectid import ObjectId
from collections import ChainMap
import pymongo
import os
import schedule
import time
from datetime import datetime

app = Flask(__name__)

@app.route('/flask', methods=['GET'])
def index():
    app.run('app.py')
    return "Flask server"

@app.route('/test_bot/<id>', methods=['POST','GET'])
def bot_post(id):
    # myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
    myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")

    mydb = myclient["devXInvester"]
    mycol = mydb["posts"]
    mycol_properties = mydb["properties"]
    mycol_cp = mydb["cannot_posts"]
    # mycol_user = mydb["user"]

    dt_object = datetime.now()

    print(id)
    data = []
    log_lvisd = []
    log_tht = []
    log_ppth = []
    log_res = []
    log_renthub = []
    post_topic = []
    list_type = []
    bedroom = []
    toilet = []
    s_price = []
    f_price = []
    min_area = []
    max_area = []
    description = []
    property_type = []
    watchlistPropertyTypes =[]
    property = []
    imgs = []
    state = []
    city = []
    subcity = []
    address = []
    addressEN = []

    def install_wd():
        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        # options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        options.add_argument(f'user-agent={user_agent}')
        wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)

        # self.wd = webdriver.Chrome(chrome_options=chromeOptions) 
        # self.wd = webdriver.Chrome(PATH, chrome_options=chrome_options)
        wd.maximize_window()
        wd.quit()
    
    install_wd()

    def query(id):

        myquery = {"_id":ObjectId(id)}

        mydoc = mycol.find(myquery)

        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # address.append(cut_find_address)

            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # address.append(cut_find_address)

        mycol_properties_query = {"_id":ObjectId(property[0])}
        mydoc_mycol_properties = mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            print(nameEN,nameTH,City)

            address.append(nameTH)
            addressEN.append(nameEN)
            city.append(City)


        def pnt():
            print('pnt',address)
        pnt()

    query(id)
#repost sucsess

    def up_cp_a(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'livinginsider_status':'cannot post'}}
        mycol.update_one(myquery ,newvalues)

    def up_cp_b(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'propotyhub_status':'cannot post'}}
        mycol.update_one(myquery ,newvalues)

    def up_cp_c(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'renthub_status':'cannot post'}}
        mycol.update_one(myquery ,newvalues)

    def up_cp_d(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'thaihometown_status':'cannot post'}}
        mycol.update_one(myquery ,newvalues)

    def up_cp_f(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'residences_status':rtf}}
        mycol.update_one(myquery ,newvalues)
    
    def up_np_b(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'propotyhub_status':'no project'}}
        mycol.update_one(myquery ,newvalues)

    def up_np_f(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'residences_status':'no project'}}
        mycol.update_one(myquery ,newvalues)
    
    def up_p_a(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'livinginsider_status':'posted'}}
        mycol.update_one(myquery ,newvalues)

    def up_p_b(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'propotyhub_status':'posted'}}
        mycol.update_one(myquery ,newvalues)

    def up_p_c(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'renthub_status':'posted'}}
        mycol.update_one(myquery ,newvalues)

    def up_p_d(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'thaihometown_status':'posted'}}
        mycol.update_one(myquery ,newvalues)

    def up_p_f(id):
        
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'residences_status':'posted'}}
        mycol.update_one(myquery ,newvalues)

    def insert_cp_a(id):
        
        mydict = {'cannot_post':ObjectId(id), 'weba': '1','time':dt_object}

        x = mycol_cp.insert_one(mydict)

    def insert_cp_b(id):
        
        mydict = {'cannot_post':ObjectId(id), 'webb': '1','time':dt_object}

        x = mycol_cp.insert_one(mydict)

    def insert_cp_c(id):
        
        mydict = {'cannot_post':ObjectId(id), 'webc': '1','time':dt_object}

        x = mycol_cp.insert_one(mydict)

    def insert_cp_d(id):
        
        mydict = {'cannot_post':ObjectId(id), 'webd': '1','time':dt_object}

        x = mycol_cp.insert_one(mydict)

    def insert_cp_f(id):
        
        mydict = {'cannot_post':ObjectId(id), 'webf': '1','time':dt_object}

        x = mycol_cp.insert_one(mydict)


#repost unsucsess

    z = update_path()
    z.start_update_path(id)

    # try:
    a = startlvisd()
    print('startbotlvisd')
    rta = a.startbotlvisd(id)
    print('finishlvisd')
    up_p_a(id)
    if rta != True:
    # except Exception as e:
        data.append('ไม่สามารถโพสลงเว็บ livinginsider')
        insert_cp_a(id)
        up_cp_a(id)
    #     # log_lvisd.append(e)
    
    # # try:
    d = starttht()
    print('startbotthaihometown')
    rtd = d.startbotthaihometown(id)
    print('finishbotthaihometown')
    up_p_d(id)
    print(rtd)
    if rtd != True:
    # except Exception as e:
        data.append('ไม่สามารถโพสลงเว็บ thaihometown')
        insert_cp_d(id)
        up_cp_d(id)
        # log_tht.append(e)
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'thaihometown_status':rtd}}
        mycol.update_one(myquery ,newvalues)


    print(property_type)
    if property_type == [ObjectId('62bd7104f0521b8890fe9a0f')] or property_type == [ObjectId('62c3b1f6dbf2ee974adf6a03')]:
        print('คอนโด')

        # try:
        b = startppth()
        print('startbotppth')
        rtb = b.startbotppth(id)
        print('finishbotppth')
        up_p_b(id)
        if rtb != True:
        # except Exception as e:
            data.append('ไม่สามารถโพสลงเว็บ propertyhub')
            insert_cp_b(id)
            up_cp_b(id)
            # log_ppth.append(e)
        if rtb == "ไม่มีโปรเจค":
            data.append('propertyhub ไม่มีโครงการ')
            up_np_b(id)

        if 'rent' in list_type:
            # try:
            print(list_type)
            f = startresidences()
            print('startbotres')
            rtf = f.startbotresidences(id)
            print('finishbotres')
            up_p_f(id)
            if rtf != True:
            # except Exception as e:
                data.append('ไม่สามารถโพสลงเว็บ residences')
                insert_cp_f(id)
                up_cp_f(id)
                # log_res.append(e)
            if rtf == "ไม่มีโปรเจค":
                data.append('residences ไม่มีโครงการ')
                up_np_f(id)
                print('residences ไม่มีโครงการ')
        
        
        else:
            print('residences for rent')
            data.append('residences for rent')
            myquery = {"_id":ObjectId(id)}
            rentresnewvalues = {'$set': {'residences_status':'residences เฉพาะคอนโด เช่า'}}
            mycol.update_one(myquery ,rentresnewvalues)
    else:
        print('propotyhub, residences เฉพาะคอนโด')
        data.append('propotyhub, residences เฉพาะคอนโด')

        myquery = {"_id":ObjectId(id)}
        ppthnewvalues = {'$set': {'propotyhub_status':'Propotyhub can post only condos, only for sale and rent.'}}
        mycol.update_one(myquery ,ppthnewvalues)
        resnewvalues = {'$set': {'residences_status':'Residents can post only specific condos for rent.'}}
        mycol.update_one(myquery ,resnewvalues)

    if 'rent' in list_type and property_type == 'อพาร์ทเม้นท์':
        # try:
        c = startrenthub()
        print('startbotrenthub')
        trc = c.startbotrenthub(id)
        print('finishbotrenthub')
        up_p_c(id)
        if trc != True:
        # except Exception as e:
            data.append('ไม่สามารถโพสลงเว็บ renthub')
            insert_cp_c(id)
            up_cp_c(id)
            # log_renthub.append(e)
    else:
        print('renthub for rent arpartment')
        data.append('renthub for rent arpartment')
        myquery = {"_id":ObjectId(id)}
        renthubnewvalues = {'$set': {'renthub_status':'Renthub can only post apartments.'}}
        mycol.update_one(myquery ,renthubnewvalues)

    # app.run('app.py')
    if data == []:
        print('FINISH')
        return 'FINISH'
    else:
        print(data)
        return data
    # return "FINISH"

if __name__ == "__main__":
    app.run(port=5005, debug=True, host='0.0.0.0')