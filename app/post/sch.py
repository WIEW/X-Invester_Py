import requests
from flask import Flask,url_for
from test_post_lvisd import startlvisd
from test_post_ppth import startppth
from test_post_renthub import startrenthub
from test_post_thaihometown import starttht
from test_post_residences import startresidences
from dl_pic_f_api import update_path
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from bson.objectid import ObjectId
from collections import ChainMap
import pymongo
import os
import schedule
import time
from datetime import datetime

dt_object = datetime.now()

# myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")


mydb = myclient["devXInvester"]
mycol = mydb["posts"]
mycol_properties = mydb["properties"]

mycol_cp = mydb["cannot_posts"]

pa_ids = []
pb_ids = []
pc_ids = []
pd_ids = []
pf_ids = []
data = []
log_lvisd = []
log_tht = []
log_ppth = []
log_res = []
log_renthub = []
post_topic = []
list_type = []
bedroom = []
toilet = []
s_price = []
f_price = []
min_area = []
max_area = []
description = []
property_type = []
watchlistPropertyTypes =[]
property = []
imgs = []
state = []
city = []
subcity = []
address = []
addressEN = []

def install_wd():
    options = Options()
    options.add_argument('--no-sandbox')
    options.add_argument("--headless")
    options.add_argument('--disable-dev-shm-usage')
    # options.add_argument("--remote-debugging-port=9230")  # this

    options.add_argument("--disable-dev-shm-using") 
    options.add_argument("--disable-extensions") 
    options.add_argument("--disable-gpu") 
    options.add_argument("start-maximized") 
    options.add_argument('--ignore-ssl-errors=yes')
    options.add_argument('--ignore-certificate-errors')
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
    options.add_argument(f'user-agent={user_agent}')
    wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)

    # self.wd = webdriver.Chrome(chrome_options=chromeOptions) 
    # self.wd = webdriver.Chrome(PATH, chrome_options=chrome_options)
    wd.maximize_window()
    wd.quit()

def q_up():
    up_qa = {'weba':'1'}
    up_doca = mycol_cp.find(up_qa)

    for x in up_doca:
        # try:
        pa_id = x.get('cannot_post')
        pa_ids.append(pa_id)

    up_qb = {'webb':'1'}
    up_docb = mycol_cp.find(up_qb)

    for x in up_docb:
        # try:
        pb_id = x.get('cannot_post')
        pb_ids.append(pb_id)

    up_qc = {'webc':'1'}
    up_docc = mycol_cp.find(up_qc)

    for x in up_docc:
        # try:
        pc_id = x.get('cannot_post')
        pc_ids.append(pc_id)
        
    up_qd = {'webd':'1'}
    up_docd = mycol_cp.find(up_qd)

    for x in up_docd:
        # try:
        pd_id = x.get('cannot_post')
        pd_ids.append(pd_id)

    up_qf = {'webf':'1'}
    up_docf = mycol_cp.find(up_qf)

    for x in up_docf:
        # try:
        pf_id = x.get('cannot_post')
        pf_ids.append(pf_id)

def update_cp_a(id):
    
    mydict = {'cannot_post':ObjectId(id), 'weba': '1','time':dt_object}

    x = mycol_cp.update_one(mydict, mydict)

def update_cp_b(id):
    
    mydict = {'cannot_post':ObjectId(id), 'webb': '1','time':dt_object}

    x = mycol_cp.update_one(mydict, mydict)

def update_cp_c(id):
    
    mydict = {'cannot_post':ObjectId(id), 'webc': '1','time':dt_object}

    x = mycol_cp.update_one(mydict, mydict)

def update_cp_d(id):
    
    mydict = {'cannot_post':ObjectId(id), 'webd': '1','time':dt_object}

    x = mycol_cp.update_one(mydict, mydict)

def update_cp_f(id):
    
    mydict = {'cannot_post':ObjectId(id), 'webf': '1','time':dt_object}

    x = mycol_cp.update_one(mydict, mydict)

def query(id):

    myquery = {"_id":ObjectId(id)}

    mydoc = mycol.find(myquery)

    for x in mydoc:
        try:
            lat_long = []
            topic = x.get('name')
            galleryUrls = x.get('galleryUrls')
            listType = x.get('listType')
            PropertyType = x.get('propertyType')
            watchListPropertyTypes = x.get('watchListPropertyTypes')
            Property = x.get('property')
            details_array = x.get('cut_find_details')
            details_dict = dict(ChainMap(*details_array))
            totalBedroom = details_dict.get('จำนวนห้องนอน')
            totalToilet = x.get('totalToilet')
            startingPrice = x.get('startingPrice')
            finalPrice = x.get('finalPrice')
            minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
            maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
            # TH_Name = x.get('TH_Name')
            # EN_Name = x.get('EN_Name')
            # Address = x.get('Address')
            # Latitude = x.get('Latitude')
            # Longitude = x.get('Longitude')
            Description = x.get('description')
            # State = x.get('State')
            # City = x.get('City')
            # SubCity = x.get('SubCity')
            # cut_find_address = x.get('cut_find_address')

            post_topic.append(topic)
            imgs.append(galleryUrls)
            list_type.append(listType)
            property_type.append(PropertyType)
            property.append(Property)
            bedroom.append(totalBedroom)
            toilet.append(totalToilet)
            s_price.append(startingPrice)
            f_price.append(finalPrice)
            min_area.append(minimumArea)
            max_area.append(maximumArea)
            description.append(Description)
            watchlistPropertyTypes.append(watchListPropertyTypes)
            # state.append(State)
            # city.append(City)
            # subcity.append(SubCity)
            # address.append(cut_find_address)

        except:
            topic = x.get('name')
            galleryUrls = x.get('galleryUrls')
            listType = x.get('listType')
            PropertyType = x.get('propertyType')
            watchListPropertyTypes = x.get('watchListPropertyTypes')
            Property = x.get('property')
            totalBedroom = x.get('totalBedroom')
            totalToilet = x.get('totalToilet')
            startingPrice = x.get('startingPrice')
            finalPrice = x.get('finalPrice')
            minimumArea = x.get('minimumArea')
            maximumArea = x.get('maximumArea')
            Description = x.get('description')
            # TH_Name = x.get('TH_Name')
            # EN_Name = x.get('EN_Name')
            # Address = x.get('Address')
            # Latitude = x.get('Latitude')
            # Longitude = x.get('Longitude')
            # State = x.get('State')
            # City = x.get('City')
            # SubCity = x.get('SubCity')
            # cut_find_address = x.get('cut_find_address')

            post_topic.append(topic)
            imgs.append(galleryUrls)
            list_type.append(listType)
            property_type.append(PropertyType)
            property.append(Property)
            bedroom.append(totalBedroom)
            toilet.append(totalToilet)
            s_price.append(startingPrice)
            f_price.append(finalPrice)
            min_area.append(minimumArea)
            max_area.append(maximumArea)
            description.append(Description)
            watchlistPropertyTypes.append(watchListPropertyTypes)
            # state.append(State)
            # city.append(City)
            # subcity.append(SubCity)
            # lat_long.append(Latitude)
            # lat_long.append(Longitude)
            # address.append(cut_find_address)

    mycol_properties_query = {"_id":ObjectId(property[0])}
    mydoc_mycol_properties = mycol_properties.find(mycol_properties_query)

    for x in mydoc_mycol_properties:
        nameTH = x.get('nameTH')
        nameEN = x.get('nameEN')
        City = x.get('address')
        print(nameEN,nameTH,City)

        address.append(nameTH)
        addressEN.append(nameEN)
        city.append(City)


    def pnt():
        print('pnt',address)
    pnt()

def d_cp_a(id):
    mycol_cp.delete_one({'cannot_post':ObjectId(id)})
def bot_post():
    
    install_wd()

    q_up()
    
    for id in pa_ids:
        post_topic = []
        list_type = []
        bedroom = []
        toilet = []
        s_price = []
        f_price = []
        min_area = []
        max_area = []
        description = []
        property_type = []
        watchlistPropertyTypes =[]
        property = []
        imgs = []
        state = []
        city = []
        subcity = []
        address = []
        addressEN = []

        myquery = {"_id":id}

        mydoc = mycol.find(myquery)

        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # address.append(cut_find_address)

            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # address.append(cut_find_address)

        mycol_properties_query = {"_id":ObjectId(property[0])}
        mydoc_mycol_properties = mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            print(nameEN,nameTH,City)

            address.append(nameTH)
            addressEN.append(nameEN)
            city.append(City)


    # # # try:
        a = startlvisd()
        print('startbotlvisd')
        rta = a.startbotlvisd(id)
        print('finishlvisd')
        if rta != True:
        # except Exception as e:
            # data.append('ไม่สามารถโพสลงเว็บ livinginsider')
            update_cp_a(id)
        #     # log_lvisd.append(e)
        elif rta == True:
            mycol_cp.update_one({'cannot_post':id},{"$set":{'weba': '0'}})
    
    for id in pd_ids:

        post_topic = []
        list_type = []
        bedroom = []
        toilet = []
        s_price = []
        f_price = []
        min_area = []
        max_area = []
        description = []
        property_type = []
        watchlistPropertyTypes =[]
        property = []
        imgs = []
        state = []
        city = []
        subcity = []
        address = []
        addressEN = []

        myquery = {"_id":id}

        mydoc = mycol.find(myquery)

        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # address.append(cut_find_address)

            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # address.append(cut_find_address)

        mycol_properties_query = {"_id":ObjectId(property[0])}
        mydoc_mycol_properties = mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            print(nameEN,nameTH,City)

            address.append(nameTH)
            addressEN.append(nameEN)
            city.append(City)


        # # try:
            d = starttht()
            print('startbotthaihometown')
            rtd = d.startbotthaihometown(id)
            print('finishbotthaihometown')
            if rtd != True:
            # except Exception as e:
                data.append('ไม่สามารถโพสลงเว็บ thaihometown')
                update_cp_d(id)
            elif rtd == True:
                mycol_cp.update_one({'cannot_post':id},{"$set":{'webd': '0'}})
            #     # log_tht.append(e)

    for id in pb_ids:

        post_topic = []
        list_type = []
        bedroom = []
        toilet = []
        s_price = []
        f_price = []
        min_area = []
        max_area = []
        description = []
        property_type = []
        watchlistPropertyTypes =[]
        property = []
        imgs = []
        state = []
        city = []
        subcity = []
        address = []
        addressEN = []

        myquery = {"_id":id}

        mydoc = mycol.find(myquery)

        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # address.append(cut_find_address)

            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # address.append(cut_find_address)

        mycol_properties_query = {"_id":ObjectId(property[0])}
        mydoc_mycol_properties = mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            print(nameEN,nameTH,City)

            address.append(nameTH)
            addressEN.append(nameEN)
            city.append(City)
        # # try:
        b = startppth()
        print('startbotppth')
        rtb = b.startbotppth(id)
        print('finishbotppth')
        if rtb != True:
        # except Exception as e:
            data.append('ไม่สามารถโพสลงเว็บ propertyhub')
            update_cp_b(id)

        elif rtb == True:
                mycol_cp.update_one({'cannot_post':id},{"$set":{'webb': '0'}})
            # log_ppth.append(e)
        if rtb == "ไม่มีโปรเจค":
            mycol_cp.update_one({'cannot_post':id},{"$set":{'webb': '0'}})
            
    for id in pf_ids:

        post_topic = []
        list_type = []
        bedroom = []
        toilet = []
        s_price = []
        f_price = []
        min_area = []
        max_area = []
        description = []
        property_type = []
        watchlistPropertyTypes =[]
        property = []
        imgs = []
        state = []
        city = []
        subcity = []
        address = []
        addressEN = []

        myquery = {"_id":id}

        mydoc = mycol.find(myquery)

        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # address.append(cut_find_address)

            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # address.append(cut_find_address)

        mycol_properties_query = {"_id":ObjectId(property[0])}
        mydoc_mycol_properties = mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            print(nameEN,nameTH,City)

            address.append(nameTH)
            addressEN.append(nameEN)
            city.append(City)
        # try:
        f = startresidences()
        print('startbotres')
        rtf = f.startbotresidences(id)
        print('finishbotres')
        if rtf != True:
        # except Exception as e:
            data.append('ไม่สามารถโพสลงเว็บ residences')
            update_cp_f(id)
            # log_res.append(e)
        elif rtf == True:
            mycol_cp.update_one({'cannot_post':id},{"$set":{'webf': '0'}})
        if rtf == "ไม่มีโปรเจค":
            data.append('residences ไม่มีโครงการ')

    for id in pc_ids:

        post_topic = []
        list_type = []
        bedroom = []
        toilet = []
        s_price = []
        f_price = []
        min_area = []
        max_area = []
        description = []
        property_type = []
        watchlistPropertyTypes =[]
        property = []
        imgs = []
        state = []
        city = []
        subcity = []
        address = []
        addressEN = []

        myquery = {"_id":id}

        mydoc = mycol.find(myquery)

        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # address.append(cut_find_address)

            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')

                post_topic.append(topic)
                imgs.append(galleryUrls)
                list_type.append(listType)
                property_type.append(PropertyType)
                property.append(Property)
                bedroom.append(totalBedroom)
                toilet.append(totalToilet)
                s_price.append(startingPrice)
                f_price.append(finalPrice)
                min_area.append(minimumArea)
                max_area.append(maximumArea)
                description.append(Description)
                watchlistPropertyTypes.append(watchListPropertyTypes)
                # state.append(State)
                # city.append(City)
                # subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # address.append(cut_find_address)

        mycol_properties_query = {"_id":ObjectId(property[0])}
        mydoc_mycol_properties = mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            print(nameEN,nameTH,City)

            address.append(nameTH)
            addressEN.append(nameEN)
            city.append(City)
        # try:
        print(list_type)
        # try:
        c = startrenthub()
        print('startbotrenthub')
        rtc = c.startbotrenthub(id)
        print('finishbotrenthub')
        if rtc != True:
        # except Exception as e:
            data.append('ไม่สามารถโพสลงเว็บ renthub')
            update_cp_c(id)
        elif rtc == True:
            mycol_cp.update_one({'cannot_post':id},{"$set":{'webc': '0'}})

    all_query = mycol_cp.find({})
    for o_q in all_query:
        ck = []

        a = o_q.get('a')
        b = o_q.get('b')
        c = o_q.get('c')
        d = o_q.get('d')
        f = o_q.get('f')
        ck.append(a)
        ck.append(b)
        ck.append(c)
        ck.append(d)
        ck.append(f)

        if '1' not in ck:
            mycol_cp.delete_one(o_q)
            

def printa():
    print('id', '')

print('sch')
# schedule.every(5).seconds.do(printa)
schedule.every(6).hours.do(bot_post)

i = 0
while True:
    print(i)
    print('schedule.run_pending()')
    schedule.run_pending()
    time.sleep(1)
    i+=1