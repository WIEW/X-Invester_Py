from pickle import GLOBAL
import requests
from bs4 import BeautifulSoup
import csv
import itertools
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait,Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo
from collections import ChainMap
from bson.objectid import ObjectId
import pyperclip
import random

################################################################
class ppth:
    def __init__(self):

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        # options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        options.add_argument(f'user-agent={user_agent}')
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)

        self.wd.maximize_window()
    ####url################################
        url = "https://propertyhub.in.th/"

        self.wd.get(url)
        self.wait = WebDriverWait(self.wd, 10)
    ####url################################
        ##Start of db zone###
        # self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        self.mydb = self.myclient["devXInvester"]
        self.mycol = self.mydb["posts"]
        self.mycol_properties = self.mydb["properties"]
        self.mycol_uw = self.mydb["websites"]

        self.ad_title = ['test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718']
        ##End of db zone###
        
        ################################################################
        self.JS_ADD_TEXT_TO_INPUT = """
            var elm = arguments[0], txt = arguments[1];
            elm.value += txt;
            elm.dispatchEvent(new Event('change'));
            """
        self.i=0
        self.cpost = 1
        self.n_pic = 1
        self.post_topic = []
        self.list_type = []
        self.bedroom = []
        self.toilet = []
        self.s_price = []
        self.f_price = []
        self.min_area = []
        self.max_area = []
        self.description = []
        self.property_type = []
        self.watchlistPropertyTypes =[]
        self.property = []
        self.imgs = []
        self.state = []
        self.city = []
        self.subcity = []
        self.address = []
        self.addressEN = []
        self.imgs_path = []

        self.no_pjn = []

    ##########################query################################################################
    def query(self,id):
        print(id)
        myquery = {"_id":ObjectId(id)}

        mydoc = self.mycol.find(myquery)
        global galleryUrls
        global ImgsPath

        stage_lookup_property = {
        "$lookup": {
        "from": "properties", 
        "localField": "property", 
        "foreignField": "_id", 
        "as": "data_property",
        }
        }

        stage_lookup_subCity = {
        "$lookup": {
        "from": "subCities", 
        "localField": "subCity", 
        "foreignField": "_id", 
        "as": "data_subCity",
        }
        }
        stage_lookup_city = {
        "$lookup": {
        "from": "cities", 
        "localField": "city", 
        "foreignField": "_id", 
        "as": "data_city",
        }
        }
        stage_lookup_state = {
        "$lookup": {
        "from": "states", 
        "localField": "state", 
        "foreignField": "_id", 
        "as": "data_state",
        }
        }
        # Limit to the first 5 documents:
        stage_limit_5 = { "$limit": 1 }

        pipeline_property = [
        stage_lookup_property,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]

        pipeline_subCity = [
        stage_lookup_subCity,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]
        pipeline_city = [
        stage_lookup_city,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]
        pipeline_state = [
        stage_lookup_state,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]

        # self.properties = self.mycol.aggregate(pipeline_property)
        # self.subCities = self.mycol_properties.aggregate(pipeline_subCity)
        # self.Cities = self.mycol_properties.aggregate(pipeline_city)
        # self.States = self.mycol_properties.aggregate(pipeline_state)

        # for property in self.properties:
        #     # pprint(type(property))
        #     data_property = property.get('data_property')
        #     data_property_dict = (dict(ChainMap(*data_property)))
        #     nameTH = data_property_dict.get('nameTH')
        #     self.address.append(nameTH)
        #     # city = data_property_dict.get('city')
        #     for subCity in self.subCities:
        #         # print(type(subCity))
        #         data_subCity = subCity.get('data_subCity')
        #         data_subCity_dict = (dict(ChainMap(*data_subCity)))
        #         subCity = data_subCity_dict.get('name')
        #         print(subCity)
        #         self.subcity.append(subCity)
        #         # print((subCity))
        #     for City in self.Cities:
        #         data_city = City.get('data_city')
        #         data_city_dict = (dict(ChainMap(*data_city)))
        #         City = data_city_dict.get('name')
        #         print(City)
        #         self.city.append(City)
        #     for State in self.States:
        #         data_State = State.get('data_state')
        #         data_State_dict = (dict(ChainMap(*data_State)))
        #         State = data_State_dict.get('name')
        #         print(State)
        #         self.state.append(State)


        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)                

            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)                

        mycol_properties_query = {"_id":ObjectId(self.property[0])}
        mydoc_mycol_properties = self.mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            Province = x.get('province')
            print(nameEN,nameTH,City)

            self.address.append(nameTH)
            self.addressEN.append(nameEN)
            self.city.append(City)
            self.state.append(Province)


        def pnt():
            print('pnt',self.address)
        pnt()

    ##########################query################################################################
    def query_uw(self):
        myquery = {'name':'propertyhub'}
        myuws = self.mycol_uw.find(myquery)
        for uws in myuws:
            users = uws['users']

        global user
        global passwd

        user = users[0]['username']
        passwd = users[0]['password']

        r = random.choice(users)

        u = r['username']
        p = r['password']

##########################click_login################################################################
    def click_login(self):
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/header/div/span[1]')))
        try:
            self.wd.find_element(By.XPATH, '//*[@id="consent-banner"]/div/div/button[2]').click()
        except Exception as e:
            print('no consent-banner')
        # login_button = WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable(EC.element_to_be_clickable(By.XPATH, '//*[@id="__next"]/div/header/div/span[1]'))
        # login_button.click()
        time.sleep(3)
        link = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/header/div/span[1]')
        link.click()
        print("Click Login_Button")

    # click_login()

    ##########################click_login##########################################

    ###############################fill_login##########################################
    def fill_login(self):
        self.wd.implicitly_wait(0.5)
        #0961599447
        # self.wait.until(EC.element_to_be_clickable((By.NAME, 'email'))).send_keys("testgingging@gmail.com")
        # self.wait.until(EC.element_to_be_clickable((By.NAME, 'email'))).send_keys("fahfah.prapapan@gmail.com")
        self.wait.until(EC.element_to_be_clickable((By.NAME, 'email'))).send_keys(user)

        # self.wait.until(EC.element_to_be_clickable((By.NAME, "password"))).send_keys("qwerty123456Zx")
        # self.wait.until(EC.element_to_be_clickable((By.NAME, "password"))).send_keys("Prapapan1")
        self.wait.until(EC.element_to_be_clickable((By.NAME, "password"))).send_keys(passwd)


        link = self.wd.find_element(By.XPATH, '/html/body/div[4]/div/div/div/div[2]/div/div/div[1]/form/div/div/button')
        link.click()
        print("Click Login_Button finish")

    ###############################fill_login##########################################

    #######################click_createpost#######################################
    def click_createpost(self):
        time.sleep(5)
        link = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/header/div/a[5]')
        print("found Createpost")
        link.click()
        print("Click Createpost")
        
    ########################click_createpost######################################

    ################################fill_data####################################
    def fill_data(self):
        time.sleep(5)
        # close_button = self.wd.find_element(By.XPATH, '/html/body/reach-portal/div/div/div[2]/div/div/form/button')
        # close_button.click()
        # print("close_announce_button")

    ################ข้อมูลทั่วไป#######################################
        self.wait.until(EC.element_to_be_clickable((By.NAME, "title.th")))#.send_keys(post_topic)
        print('title.th')

        def headtitle_th():
            headtitle_th = self.wd.find_element(By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[1]/div[1]/div[1]/div/input')
            headtitle_th.send_keys(self.post_topic[0])
            # pyperclip.copy(str(self.post_topic[0]))
            # act = ActionChains(self.wd)
            # act.send_keys_to_element(headtitle_th).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
        headtitle_th()

        def headtitle_en():
            headtitle_en = self.wd.find_element(By.NAME,'title.en')
            pyperclip.copy(str(self.post_topic[0]))
            act = ActionChains(self.wd)
            act.send_keys_to_element(headtitle_en).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
        # headtitle_en()

        print(self.addressEN[0])
        self.wd.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/div/form/div/div/div[1]/div[3]/div[1]/div/div[1]/input').click()
        # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="projectId"]/div/div[1]/input'))).send_keys(self.addressEN[0])
        try:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="projectId"]/div/div[1]/input'))).send_keys(self.addressEN[0])
            time.sleep(2)
            project_name = self.wd.find_element(By.XPATH, '//*[@id="listbox--1"]')
            # project_name = self.wd.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/div/form/div/div/div[1]/div[3]/div[1]/div/div[2]/ul/li')
            project_name.click()
        except:
            try:
                fill_pjn = self.wd.find_element(By.XPATH, '//*[@id="projectId"]/div/div[1]/input')
                fill_pjn.send_keys(Keys.CONTROL, 'a')
                fill_pjn.send_keys(Keys.BACKSPACE)
                # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="projectId"]/div/div[1]/input'))).send_keys(self.addressEN[0])
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="projectId"]/div/div[1]/input'))).send_keys('ไม่ทราบโครงการ')
                time.sleep(0.5)
                project_name = self.wd.find_element(By.XPATH, '//*[@id="listbox--1"]')
                time.sleep(2)
                project_name.click()
            except:
                self.no_pjn.append("ไม่มีโปรเจค")
                print(self.no_pjn)
                return 'ไม่มีโปรเจค'
        
        # wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.building"))).send_keys("testอาคาร")

    ##########################################check_ifรูปแบบห้อง#########################################################
        self.wd.find_element(By.XPATH, '//*[@id="button--listbox-input--2"]').click()#
        select_property_type = self.wd.find_element(By.XPATH, '//*[@id="button--listbox-input--2"]/span')
        # select_type = Select(building_type)
        # type_list = select_type.options
        # for ele in type_list:
        #     print(ele.text)
        
        # self.wd.find_element(By.XPATH, '//*[@id="button--listbox-input--2"]').click()
        if self.property_type[0] == ObjectId('62c3b1f6dbf2ee974adf6a03'):
            self.wd.find_element(By.XPATH, '//*[@id="option-STUDIO--listbox-input--2"]').click()#
        elif self.bedroom[0] == 1:
            self.wd.find_element(By.XPATH, '//*[@id="option-ONE_BED_ROOM--listbox-input--2"]').click()#
        elif self.bedroom[0] == 2:
            self.wd.find_element(By.XPATH, '//*[@id="option-TWO_BED_ROOM--listbox-input--2"]').click()#
        elif self.bedroom[0] == 3:
            self.wd.find_element(By.XPATH, '//*[@id="option-THREE_BED_ROOM--listbox-input--2"]').click()#
        elif self.bedroom[0] == 4:
            self.wd.find_element(By.XPATH, '//*[@id="option-FOUR_BED_ROOM--listbox-input--2"]').click()#
        elif self.bedroom[0] == 5:
            self.wd.find_element(By.XPATH, '//*[@id="option-FIVE_BED_ROOM--listbox-input--2"]').click()#
        elif self.bedroom[0] == 6:
            self.wd.find_element(By.XPATH, '//*[@id="option-SIX_BED_ROOM--listbox-input--2"]').click()#
        elif ['Moff'] in self.bedroom:
            self.wd.find_element(By.XPATH, '//*[@id="option-MOFF--listbox-input--2"]').click()#
        elif ['Duplex'] in self.bedroom:
            self.wd.find_element(By.XPATH, '//*[@id="option-DUPLEX_ONE_BED--listbox-input--2"]').click()#
        elif ['Duplex 2 ห้องนอน'] in self.bedroom:
            self.wd.find_element(By.XPATH, '//*[@id="option-DUPLEX_TWO_BED--listbox-input--2"]').click()#
        elif ['Duplex 3 ห้องนอน'] in self.bedroom:
            self.wd.find_element(By.XPATH, '//*[@id="option-DUPLEX_TWO_BED--listbox-input--2"]').click()#
        elif ['Duplex 4 ห้องนอน'] in self.bedroom:
            self.wd.find_element(By.XPATH, '//*[@id="option-DUPLEX_TWO_BED--listbox-input--2"]').click()#
        elif ['Duplex 5 ห้องนอน'] in self.bedroom:
            self.wd.find_element(By.XPATH, '//*[@id="option-DUPLEX_TWO_BED--listbox-input--2"]').click()#    
        elif ['Penthouse'] in self.bedroom:
            self.wd.find_element(By.XPATH, '//*[@id="option-PENTHOUSE--listbox-input--2"]').click()#
        elif ['Villa'] in self.bedroom:
            self.wd.find_element(By.XPATH, '//*[@id="option-VILLA--listbox-input--2"]').click()#
        else:
            self.wd.find_element(By.XPATH, '//*[@id="option-STUDIO--listbox-input--2"]').click()#
            print("[else] in self.bedroom:")


    ##########################################################################################################
    ##########################################check_ifรูปแบบห้อง#########################################################

    #     wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.roomHomeAddress"))).send_keys("testบ้านเลขที่ห้อง")
    #     wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.roomNumber"))).send_keys("testหมายเลขห้อง")
        self.wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.onFloor"))).send_keys("1-100")

    # ##########################################check_ifทิศทางห้อง#########################################################
    #     self.wd.find_element(By.XPATH, '//*[@id="button--listbox-input--3"]/span').click()
    #     room_direction = self.wd.find_element(By.XPATH, '//*[@id="option-NORTH--listbox-input--3"]')

    # ##########################################################################################################
    #     room_direction.click()
    ##########################################check_ifทิศทางห้อง#########################################################
        try:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.numberOfBed"))).send_keys(self.bedroom[0].split(' ')[0])
        except:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.numberOfBed"))).send_keys(self.bedroom[0])

        try:
            try:
                self.wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.numberOfBath"))).send_keys(self.toilet[0].split(' ')[0])
            except:
                self.wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.numberOfBath"))).send_keys(self.toilet[0])
        except:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.numberOfBath"))).send_keys(1)
        try:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.roomArea"))).send_keys(self.max_area[0].split(' ')[0])
        except:
            self.wait.until(EC.element_to_be_clickable((By.NAME, "roomInformation.roomArea"))).send_keys(self.max_area[0])

        # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[1]/div[3]/div[4]/div/div/div/div[2]/div[1]/p'))).send_keys("บันทึกช่วยจำ สำหรับเจ้าของประกาศ ไม่แสดงในหน้าประกาศ")

    # ################ข้อมูลทั่วไป#######################################


    #################################ประเภทประกาศ#######################################################################
        
        post_type = self.wd.find_element(By.XPATH,'//*[@id="postType"]')
        post_type.click()
        
    ####################################เช่า##############################
        if self.list_type == ['rent'] or self.list_type == ['เช่า'] or ['rent'] in self.list_type or ['เช่า'] in self.list_type:
            def rent():
                self.wd.find_element(By.XPATH,'//*[@id="option-FOR_RENT--listbox-input--4"]').click()#
        #       rent = self.wd.find_element(By.XPATH,'//*[@id="option-FOR_RENT--listbox-input--4"]')
        #       print("find_rent")
        #       rent.click()

        # ###################################ค่าเช่าต่อเดือน#######################
                try:
                    self.wait.until(EC.element_to_be_clickable((By.NAME, "price.forRent.monthly.price"))).send_keys(self.f_price[0].replace('ราคาเช่า','').replace('บาท/เดือน',''))
                except:
                    self.wait.until(EC.element_to_be_clickable((By.NAME, "price.forRent.monthly.price"))).send_keys(self.f_price[0])

        # ###################################ค่าเช่าต่อเดือน#######################
        # ###################################ค่าเช่าต่อวัน#######################
                # self.wait.until(EC.element_to_be_clickable((By.NAME, "price.forRent.daily.price"))).send_keys("1")
                # self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[3]/div/div/div[2]/div/div[2]/div/label/span').click()
                # self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div/div[2]/div/div[2]/div/label/input').click()
                try:
                    daily = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div/div[2]/div/div[2]/div/label/input')
                except:
                    daily = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[3]/div/div/div[2]/div/div[2]/div/label/input')
                try:
                # daily.click()
                    self.wd.execute_script("arguments[0].click();", daily)
                except:
                    daily.click()

            # ###################################ค่าเช่าต่อวัน#######################
            # ###################################เงินมัดจำ#######################
            #       self.wait.until(EC.element_to_be_clickable((By.NAME, "price.forRent.deposit.amount"))).send_keys("1")
                    # self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[3]/div/div/div[3]/div/div[3]/div/label/span').click()
                try:
                    try:
                        deposit = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div/div[4]/div/div[3]/div/label/input')#.click()
                        print('deposit1') 
                    except:
                        deposit = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div/div[3]/div/div[3]/div/label/input')#.click()
                        print('deposit2') 
                except:
                        try:
                            deposit = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div[2]/form/div/div/div[2]/div[3]/div/div/div[4]/div/div[3]/div/label/input')#.click()
                            print('deposit3') 
                        except:
                            deposit = self.wd.find_element(By.NAME, 'price.forRent.deposit.type')#.click()
                            print('depositL') 

                try:
                    self.wd.execute_script("arguments[0].click();", deposit)
                    print('depositc1')
                except:
                    deposit.click()
                    print('depositc2')
            ###################################เงินมัดจำ#######################
            ###################################จ่ายล่วงหน้า#######################
                    # //*[@id="__next"]/div/div[2]/div[2]/div[2]/form/div/div/div[2]/div[3]/div/div/div[5]/div/div[3]/div/label/input
                    # wait.until(EC.element_to_be_clickable((By.NAME, "price.forRent.advancePayment.amount"))).send_keys("1")
                    # self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[3]/div/div/div[4]/div/div[3]/div/label/span').click()
                    # self.wd.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div/div[4]/div/div[3]/div/label/input').click()
                try:
                    try:
                        advpatm = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div/div[5]/div/div[3]/div/label/input')#.click()
                        print('advpatm1')
                    except:
                        advpatm = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div/div[4]/div/div[3]/div/label/input')
                        print('advpatm2')
                except:
                    try:
                        try:
                            advpatm = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div[2]/form/div/div/div[2]/div[3]/div/div/div[5]/div/div[3]/div/label/input')
                            print('advpatm3')
                        except:
                            advpatm = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[3]/div/div/div[4]/div/div[4]/div/label/input')
                            print('advpatm4')
                    except:
                        advpatm = self.wd.find_element(By.NAME, 'price.forRent.advancePayment.type')#.click()
                        print('advpatmL')

                try:
                    self.wd.execute_script("arguments[0].click();", advpatm)
                    print('advpatmc1')
                except:
                    advpatm.click()
                    print('advpatmc2')
    ###################################จ่ายล่วงหน้า#######################
    ####################################เช่า##############################
            rent()
    ####################################ขาย##############################
        elif self.list_type == ['sell'] or self.list_type == ['ขาย'] or ['sell'] in self.list_type or ['ขาย'] in self.list_type:
        
            def sell():
                self.wd.find_element(By.XPATH,'//*[@id="option-FOR_SALE--listbox-input--4"]').click()#

                try:
                    self.wait.until(EC.element_to_be_clickable((By.NAME, "price.forSale.price"))).send_keys(self.f_price[0].replace('ราคาขาย','').replace('บาท',''))
                except:
                    self.wait.until(EC.element_to_be_clickable((By.NAME, "price.forSale.price"))).send_keys(self.f_price[0])

                # clickcall = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div[1]/div/div[2]/div')
                # clickcall.click()

        ################################sale_with_tenant#############################################   
            # sale_with_tenant = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[2]/div[2]/div/div[2]/div/span')
            # sale_with_tenant.click()
            # print(sale_with_tenant)
        
    ################################sale_with_tenant#############################################   
    ####################################ขาย##############################
            sell()
    #################################ประเภทประกาศ#######################################################################
    ##############################สิ่งอำนวยความสะดวกในห้อง########################################################

        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,"//*[@id='__next']/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[1]/span"))).click()###เฟอร์นิเจอร์
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[2]/span'))).click()###โทรศัพท์บ้าน
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[3]/span'))).click()###เครื่องปรับอากาศ
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[4]/span'))).click()###เครื่องทำน้ำร้อน/น้ำอุ่น
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[5]/span'))).click()###ประตูห้องระบบ digital lock
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[6]/span'))).click()###อ่างอาบน้ำ
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[7]/span'))).click()###TV
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[8]/span'))).click()###เตาปรุงอาหาร
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[9]/span'))).click()###ตู้เย็น
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[10]/span'))).click()###เครื่องดูดควัน
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[11]/span'))).click()###อินเตอร์เน็ตไร้สาย (WIFI) ในห้อง
        # WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[2]/div[12]/span'))).click()###เครื่องซักผ้า

    ##############################สิ่งอำนวยความสะดวกในห้อง########################################################

    #########################################room##############################################################
        # image_list = ["D:\\work\\web_scraping\\test_pic\\404_1.png","D:\\work\\web_scraping\\test_pic\\404_2.png","D:\\work\\web_scraping\\test_pic\\404_3.png","D:\\work\\web_scraping\\test_pic\\404_4.png"]
        # for i in range(0, len(image_list)):
        try:
            for n_img in range(int(len(galleryUrls))):
                print(ImgsPath[n_img])
                self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[4]/div[3]/input').send_keys(ImgsPath[n_img])
        except:
            print('no img insert')
        
        # self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[4]/div[3]/input').send_keys("D:\\work\\web_scraping\\test_pic\\404_2.JPEG")

    ########################################room##############################################################
    # ##########################################building##########################################################
    #     self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[4]/div[4]/input').send_keys("D:\\work\\web_scraping\\test_pic\\404_1.JPEG")
    # ##########################################building##########################################################
    # ##########################################similar image##########################################################
    #     self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[4]/div[5]/input').send_keys("D:\\work\\web_scraping\\test_pic\\404_1.JPEG")
    # ##########################################similar image##########################################################

    ##############################รายละเอียด########################################################
        # des_th = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[5]/div[1]/div/div/div/div[2]/div[1]')
        # self.wd.execute_script(JS_ADD_TEXT_TO_INPUT, des_th, description)
        # des_en = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[5]/div[2]/div/div/div/div[2]/div[1]')
        # self.wd.execute_script(JS_ADD_TEXT_TO_INPUT, des_en, description)
        def des_th():
            des_th = self.wd.find_element(By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[5]/div[1]/div/div/div/div[2]/div[1]')
            des_th.send_keys(self.description[0])
            # pyperclip.copy(str(self.description[0]))
            # act = ActionChains(self.wd)
            # act.send_keys_to_element(des_th).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
        des_th()

        # def des_en():
        #     des_en = self.wd.find_element(By.XPATH,'//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[5]/div[2]/div/div/div/div[2]/div[1]')
        #     pyperclip.copy(str(self.description[0]))
        #     act = ActionChains(self.wd)
        #     act.send_keys_to_element(des_en).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
        # des_en()

    # ##############################รายละเอียด########################################################

        WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,"//label[@for='agreement']"))).click()
        print("agreement.click()")
        # post_button = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[9]/button[1]')
        post_button = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div[2]/form/div/div/div[9]/button[1]')
        time.sleep(15)
        post_button.click()
        time.sleep(15)
        self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[1]/div[2]/div[2]/div/div/label').click()
        # self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/div[3]/div[1]/div/div/div/label').click()
        self.wd.find_element(By.XPATH, '//*[@id="checkAmenities"]/div/div/label').click()
        post_button.click()
        print("click Posted")
        time.sleep(20)
        if self.wd.current_url == 'https://dashboard.propertyhub.in.th/members/listings/online':
            print("finish")
        elif self.wd.current_url == 'https://dashboard.propertyhub.in.th/members/listings/new':
            print("cannot post")
            time.sleep(10)
            post_button.click()
            if self.wd.current_url == 'https://dashboard.propertyhub.in.th/members/listings/online':
                print("finish")
            elif self.wd.current_url == 'https://dashboard.propertyhub.in.th/members/listings/new':
                print("cannot post")
                # try:
                #     a_pjn = self.wd.find_element(By.XPATH, '//*[@id="projectId"]/p').get_attribute('text')
                #     if 'กรุณาระบุชื่อโครงการ' in a_pjn:
                #         self.wd.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/div/form/div/div/div[1]/div[3]/div[1]/div/div[1]/input').click()
                #         self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="projectId"]/div/div[1]/input'))).send_keys(self.addressEN[0])
                #         print('send_keys(self.address[0])',self.addressEN[0])
                # except:
                #     print('error is not a_pjn')
                return False

    ################################fill_data####################################

################################################################
    
    def quit(self):
        self.wd.quit()
class startppth:
    def startbotppth(self,id):
        try:
            fn = ppth()
            fn.click_login()
            fn.query(id)
            fn.query_uw()
            fn.fill_login()
            fn.click_createpost()
            fd = fn.fill_data()
            print('fd',fd)
            if fd == "ไม่มีโปรเจค":
                print("ไม่มีโปรเจค")
                fn.quit()
                return "ไม่มีโปรเจค"
            if fd == False:
                return False
            fn.quit()
            return True

        except Exception as e:
            print(e)
            fn.quit()
            return False
            
    # try:
    #     fn.fill_data()
    # except Exception as e:
    #     fn.print(e)
################################################################

