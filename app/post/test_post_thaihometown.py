from pickle import GLOBAL
import requests
from bs4 import BeautifulSoup
import csv
import itertools
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait,Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo
from collections import ChainMap
from bson.objectid import ObjectId
import pyperclip
import random


################################################################
class tht:
    def __init__(self):

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        # options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        options.add_argument(f'user-agent={user_agent}')
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)

        self.wd.maximize_window()
        ####url################################
        url = "https://www.thaihometown.com/member/"

        self.wd.get(url)
        self.wait = WebDriverWait(self.wd, 30)
        ####url################################
        ##Start of db zone###
        # self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        
        self.mydb = self.myclient["devXInvester"]
        self.mycol = self.mydb["posts"]
        self.mycol_properties = self.mydb["properties"]
        self.mycol_uw = self.mydb["websites"]
        
        self.JS_ADD_TEXT_TO_INPUT = """
            var elm = arguments[0], txt = arguments[1];
            elm.value += txt;
            elm.dispatchEvent(new Event('change'));
            """
        self.i=0
        self.cpost = 1
        self.n_pic = 1
        self.post_topic = []
        self.list_type = []
        self.bedroom = []
        self.toilet = []
        self.s_price = []
        self.f_price = []
        self.min_area = []
        self.max_area = []
        self.description = []
        self.property_type = []
        self.watchlistPropertyTypes =[]
        self.property = []
        self.imgs = []
        self.state = []
        self.city = []
        self.subcity = []
        self.address = []
        self.addressEN = []
        self.imgs_path = []
        self.imgs_rz_path = []


        self.JS_ADD_TEXT_TO_INPUT = """
            var elm = arguments[0], txt = arguments[1];
            elm.value += txt;
            elm.dispatchEvent(new Event('change'));
            """

##########################query################################################################
    def query(self,id):
        print(id)
        myquery = {"_id":ObjectId(id)}

        mydoc = self.mycol.find(myquery)
        global galleryUrls
        global ImgsPath
        global Img_rz_path


        stage_lookup_property = {
        "$lookup": {
        "from": "properties", 
        "localField": "property", 
        "foreignField": "_id", 
        "as": "data_property",
        }
        }

        stage_lookup_subCity = {
        "$lookup": {
        "from": "subCities", 
        "localField": "subCity", 
        "foreignField": "_id", 
        "as": "data_subCity",
        }
        }
        stage_lookup_city = {
        "$lookup": {
        "from": "cities", 
        "localField": "city", 
        "foreignField": "_id", 
        "as": "data_city",
        }
        }
        stage_lookup_state = {
        "$lookup": {
        "from": "states", 
        "localField": "state", 
        "foreignField": "_id", 
        "as": "data_state",
        }
        }
        # Limit to the first 5 documents:
        stage_limit_5 = { "$limit": 1 }

        pipeline_property = [
        stage_lookup_property,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]

        pipeline_subCity = [
        stage_lookup_subCity,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]
        pipeline_city = [
        stage_lookup_city,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]
        pipeline_state = [
        stage_lookup_state,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]

        # self.properties = self.mycol.aggregate(pipeline_property)
        # self.subCities = self.mycol_properties.aggregate(pipeline_subCity)
        # self.Cities = self.mycol_properties.aggregate(pipeline_city)
        # self.States = self.mycol_properties.aggregate(pipeline_state)

        # for property in self.properties:
        #     # pprint(type(property))
        #     data_property = property.get('data_property')
        #     data_property_dict = (dict(ChainMap(*data_property)))
        #     nameTH = data_property_dict.get('nameTH')
        #     self.address.append(nameTH)
        #     # city = data_property_dict.get('city')
        #     for subCity in self.subCities:
        #         # print(type(subCity))
        #         data_subCity = subCity.get('data_subCity')
        #         data_subCity_dict = (dict(ChainMap(*data_subCity)))
        #         subCity = data_subCity_dict.get('name')
        #         print(subCity)
        #         self.subcity.append(subCity)
        #         # print((subCity))
        #     for City in self.Cities:
        #         data_city = City.get('data_city')
        #         data_city_dict = (dict(ChainMap(*data_city)))
        #         City = data_city_dict.get('name')
        #         print(City)
        #         self.city.append(City)
        #     for State in self.States:
        #         data_State = State.get('data_state')
        #         data_State_dict = (dict(ChainMap(*data_State)))
        #         State = data_State_dict.get('name')
        #         print(State)
        #         self.state.append(State)


        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')
                Img_rz_path = x.get('imgs_rz_path')

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)   
                self.imgs_rz_path.append(Img_rz_path)                


            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')
                Img_rz_path = x.get('imgs_rz_path')

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)
                self.imgs_rz_path.append(Img_rz_path)                

        mycol_properties_query = {"_id":ObjectId(self.property[0])}
        mydoc_mycol_properties = self.mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('city')
            Province = x.get('state')
            print(nameEN,nameTH,City)

            self.address.append(nameTH)
            self.addressEN.append(nameEN)
            self.city.append(City)
            self.state.append(Province)


        def pnt():
            print('pnt',self.address)
        pnt()
            # lat_long.append(Latitude)
            # lat_long.append(Longitude)
        ##########################query################################################################
    def query_uw(self):
        myquery = {'name':'thaihometown'}
        myuws = self.mycol_uw.find(myquery)
        for uws in myuws:
            users = uws['users']

        global user
        global passwd

        user = users[0]['username']
        passwd = users[0]['password']

        r = random.choice(users)

        u = r['username']
        p = r['password']
    ##End of db zone###
# ad_title = ['test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718']
################################################################
    ##########################click_login################################################################
    def click_login(self):
        self.wd.implicitly_wait(0.5)
        link = self.wd.find_element(By.ID, 'top_login')
        # link.click()
        # print("Click Login_Button")

    # click_login()
    ##########################click_login##########################################

    ###############################fill_login##########################################
    def fill_login(self):
        self.wd.implicitly_wait(0.5)
        # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="loginform"]/tbody/tr[2]/td[2]/input'))).send_keys("nongtossob@gmail.com")
        # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="loginform"]/tbody/tr[2]/td[2]/input'))).send_keys("fahfah.prapapan@gmail.com")
        # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="loginform"]/tbody/tr[2]/td[2]/input'))).send_keys("Realty.plenty123@gmail.com")
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="loginform"]/tbody/tr[2]/td[2]/input'))).send_keys(user)

        # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="loginform"]/tbody/tr[3]/td[2]/input'))).send_keys("123456Zx")
        # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="loginform"]/tbody/tr[3]/td[2]/input'))).send_keys("Prapapan1")
        # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="loginform"]/tbody/tr[3]/td[2]/input'))).send_keys("realty123")
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="loginform"]/tbody/tr[3]/td[2]/input'))).send_keys(passwd)

        link = self.wd.find_element(By.XPATH, '//*[@id="loginform"]/tbody/tr[4]/td[2]/table/tbody/tr/td[1]')
        link.click()
        print("Click Login_Button finish")

    ###############################fill_login##########################################

    #######################click_createpost#######################################
    def click_createpost(self):
        time.sleep(5)
        # link = self.wd.find_element(By.XPATH, '//*[@id="Sfal"]/center/a')
        # print("found Createpost")
        # link.click()
        # print("Click Createpost")
        self.wd.get('https://www.thaihometown.com/addnew')
        
    ########################click_createpost######################################

    ################################fill_data####################################
    def fill_data(self):
    ################ข้อมูลทั่วไป#######################################
        print(self.post_topic[0])
        self.wait.until(EC.element_to_be_clickable((By.ID, "headtitle")))#.send_keys(post_topic)#หัวข้อประกาศ
        # wait.until(EC.element_to_be_clickable((By.ID, "headtitle")))#.send_keys(post_topic)#หัวข้อประกาศ
        # headtitle = self.wd.find_element(By.ID,'headtitle')
        # self.wd.execute_script(JS_ADD_TEXT_TO_INPUT, headtitle, post_topic)
        # wait.until(EC.element_to_be_clickable((By.ID, "headtitle"))).send_keys(1)#หัวข้อประกาศ

        def headtitle_th():
            self.wd.find_element(By.ID,"headtitle").send_keys(self.post_topic[0],'8')
            # headtitle_th = self.wd.find_element(By.ID,"headtitle")
            # pyperclip.copy(str(self.post_topic[0]))
            # act = ActionChains(self.wd)
            # act.send_keys_to_element(headtitle_th).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
            # self.wait.until(EC.element_to_be_clickable((By.ID, "headtitle"))).send_keys(1)#หัวข้อประกาศ
            print("headtitle_th"+self.post_topic[0])
        headtitle_th()

        # wait.until(EC.element_to_be_clickable((By.ID, "ad_title")))#.send_keys(description)#รายละเอียดประกาศ
        # ad_title = self.wd.find_element(By.ID,'ad_title')
        # self.wd.execute_script(JS_ADD_TEXT_TO_INPUT, ad_title, description)

        def ad_title():
            self.wd.find_element(By.ID,"ad_title").send_keys(self.description[0])
            # ad_title = self.wd.find_element(By.ID,"ad_title")
            # pyperclip.copy(str(self.description[0]))
            # act = ActionChains(self.wd)
            # act.send_keys_to_element(ad_title).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
            print('ad_title'+self.description[0])
        ad_title()
        ################################ประเภทประกาศ######################################################
        print(self.list_type)
        selected_typepart = Select(self.wd.find_element_by_id ('typepart'))
        if self.list_type == ['sell'] or self.list_type == ['ขาย'] or ['sell'] in self.list_type or ['ขาย'] in self.list_type:
            selected_typepart.select_by_visible_text('ประกาศขาย')
        elif self.list_type == ['down'] or self.list_type == ['ประกาศขายดาวน์']:
            selected_typepart.select_by_visible_text('ประกาศขายดาวน์')
        elif self.list_type == ['rent'] or self.list_type == ['เช่า'] or ['rent'] in self.list_type or ['เช่า'] in self.list_type:
            selected_typepart.select_by_visible_text('ประกาศให้เช่า')
        elif self.list_type == ['rentandsell']:
            selected_typepart.select_by_visible_text('ประกาศขาย และให้เช่า')
        ################################ประเภทประกาศ######################################################
        ################################ประเภทอสังหาริมทรัพย์################################################property_typeselected_property_type = Select(self.wd.find_element_by_id ('typepart'))
        selected_property_type = Select(self.wd.find_element_by_id ('property_type'))
        # if typepart == "บ้านเดี่ยวหลังใหญ่":
        # selected_property_type.select_by_visible_text('บ้านเดี่ยวหลังใหญ่')
        print(str(self.property_type))
        if self.property_type[0] == ObjectId('62bd7104f0521b8890fe9a11'):
            selected_property_type.select_by_visible_text('บ้าน')
        elif self.property_type[0] == ObjectId('62bd7104f0521b8890fe9a0f') or self.property_type[0] == ObjectId('62c3b1f6dbf2ee974adf6a03'):
            selected_property_type.select_by_visible_text('คอนโดมิเนียม')
        elif self.property_type[0] == ObjectId('62bd7104f0521b8890fe9a15'):
            selected_property_type.select_by_visible_text('ทาวน์โฮม 2-4 ชั้น')
        elif self.property_type[0] == "ทาวน์เฮ้าส์ 1-2 ชั้น":
            selected_property_type.select_by_visible_text('ทาวน์เฮ้าส์ 1-2 ชั้น')
        elif self.property_type[0] == "อพาร์ทเม้นท์":
            selected_property_type.select_by_visible_text('อพาร์ทเม้นท์')
        elif self.property_type[0] == "ทาวน์โฮม 2-4 ชั้น":
            selected_property_type.select_by_visible_text('ทาวน์โฮม 2-4 ชั้น')
        elif self.property_type[0] == "ตึกแถว / อาคารพาณิชย์":
            selected_property_type.select_by_visible_text('ตึกแถว / อาคารพาณิชย์')
        elif self.property_type[0] == "สำนักงาน":
            selected_property_type.select_by_visible_text('สำนักงาน')
        elif self.property_type[0] == "โฮมออฟฟิศ":
            selected_property_type.select_by_visible_text('โฮมออฟฟิศ')
        elif self.property_type[0] == "โรงงาน":
            selected_property_type.select_by_visible_text('โรงงาน')
        elif self.property_type[0] == "คลังสินค้า":
            selected_property_type.select_by_visible_text('คลังสินค้า')
        elif self.property_type[0] == "โกดัง / สโตร์":
            selected_property_type.select_by_visible_text('โกดัง / สโตร์')
        elif self.property_type[0] == "ขาย-เซ้ง ธุรกิจ | กิจการ | โรงแรม":
            selected_property_type.select_by_visible_text('ขาย-เซ้ง ธุรกิจ | กิจการ | โรงแรม')
        elif self.property_type[0] == "ที่ดิน":
            selected_property_type.select_by_visible_text('ที่ดิน')
        ################################ประเภทอสังหาริมทรัพย์######################################################
        ################################เนื้อที่######################################################
        self.wait.until(EC.element_to_be_clickable((By.ID, "property_area"))).send_keys(self.max_area[0])#เนื้อที่
        selected_property_sqm = Select(self.wd.find_element_by_id ('property_sqm'))
        # selected = Select(self.wd.find_element_by_id ('select2-results'))
        # if typepart == "ตรม":
        selected_property_sqm.select_by_value('1')#ตรม
        # # elif typepart == "ตรว":
        # selected_property_sqm.select_by_value('2')#ตรว
        # # elif typepart == "ไร่":
        # selected_property_sqm.select_by_value('3')#ไร่
        ################################เนื้อที่######################################################
        ################################พื้นที่ใช้สอย######################################################
        self.wait.until(EC.element_to_be_clickable((By.ID, "property_right_area"))).send_keys(self.max_area[0])#เนื้อที่
        property_right_area = Select(self.wd.find_element_by_id ('property_right_sqm'))
        # selected = Select(self.wd.find_element_by_id ('select2-results'))
        # if typepart == "ตรม":
        property_right_area.select_by_value('1')#ตรม
        # elif typepart == "ตรว":
        # property_right_area.select_by_value('2')#ตรว
        ################################พื้นที่ใช้สอย######################################################
        ################################จำนวนห้องนอน######################################################
        room1 = Select(self.wd.find_element_by_id ('room1'))

        print(self.bedroom)
        if self.bedroom[0] == 1:
            room1.select_by_value('1')
        elif self.bedroom[0] == 2:
            room1.select_by_value('2')
        elif self.bedroom[0] == 3:
            room1.select_by_value('3')
        elif self.bedroom[0] == 4:
            room1.select_by_value('4')
        elif self.bedroom[0] == 5:
            room1.select_by_value('5')
        elif self.bedroom[0] == 6:
            room1.select_by_value('6')
        elif self.bedroom[0] == 7:
            room1.select_by_value('7')
        elif self.bedroom[0] == 8:
            room1.select_by_value('8')
        elif self.bedroom[0] == 9:
            room1.select_by_value('9')
        elif self.bedroom[0] == 10:
            room1.select_by_value('10')
        elif self.property_type[0] == ObjectId('62c3b1f6dbf2ee974adf6a03'):
            print("studio room1.select_by_value('100')")
            room1.select_by_value('100')
        else:
            print("else room1.select_by_value('100')")
            room1.select_by_value('100')
        ################################จำนวนห้องนอน######################################################
        ################################จำนวนห้องน้ำ######################################################
        room2 = Select(self.wd.find_element_by_id ('room2'))
        # selected = Select(self.wd.find_element_by_id ('select2-results'))
        # room1.select_by_value(nroom)
        if self.toilet[0] == 1:
            room2.select_by_value('1')
        elif self.toilet[0] == 2:
            room2.select_by_value('2')
        elif self.toilet[0] == 3:
            room2.select_by_value('3')
        elif self.toilet[0] == 4:
            room2.select_by_value('4')
        elif self.toilet[0] == 5:
            room2.select_by_value('5')
        elif self.toilet[0] == 6:
            room2.select_by_value('6')
        elif self.toilet[0] == 7:
            room2.select_by_value('7')
        elif self.toilet[0] == 8:
            room2.select_by_value('8')
        elif self.toilet[0] == 9:
            room2.select_by_value('9')
        elif self.toilet[0] == 10:
            room2.select_by_value('10')
        else:
            print("else room2.select_by_value('1')")
            room2.select_by_value('1')
        ################################จำนวนห้องน้ำ######################################################
        ################################เครื่องปรับอากาศ######################################################
        def conditioning():
            conditioning = Select(self.wd.find_element_by_name ('conditioning'))
            # # selected = Select(self.wd.find_element_by_id ('select2-results'))
            # # room1.select_by_value(nroom)
            # # if room1 == "1"
            # conditioning.select_by_value('1')
            # # if room1 == "2":
            # conditioning.select_by_value('2')
            # # elif room1 == "3":
            # conditioning.select_by_value('3')
            # # elif room1 == "4":
            # conditioning.select_by_value('4')
            # # elif room1 == "5":
            # conditioning.select_by_value('5')
            # # elif room1 == "6":
            # conditioning.select_by_value('6')
            # # elif room1 == "7":
            # conditioning.select_by_value('7')
            # # elif room1 == "8":
            # conditioning.select_by_value('8')
            # # elif room1 == "9":
            # conditioning.select_by_value('9')
            # # elif room1 == "10":
            # conditioning.select_by_value('10')
            conditioning = Select(self.wd.find_element_by_name ('conditioning'))

        ################################เครื่องปรับอากาศ######################################################
        ################################ที่จอดรถ######################################################
        def carpark():
            carpark = Select(self.wd.find_element_by_name ('carpark'))
            # # selected = Select(self.wd.find_element_by_id ('select2-results'))
            # # carpark.select_by_value(nroom)
            # # if carpark == "1"
            # carpark.select_by_value('1')
            # # if carpark == "2":
            # carpark.select_by_value('2')
            # # elif carpark == "3":
            # carpark.select_by_value('3')
            # # elif carpark == "4":
            # carpark.select_by_value('4')
            # # elif carpark == "5":
            # carpark.select_by_value('5')
            # # elif carpark == "6":
            # carpark.select_by_value('6')
            # # elif carpark == "7":
            # carpark.select_by_value('7')
            # # elif carpark == "8":
            # carpark.select_by_value('8')
            # # elif carpark == "9":
            # carpark.select_by_value('9')
            # # elif carpark == "10":
            # carpark.select_by_value('10')
            carpark = Select(self.wd.find_element_by_name ('carpark'))

        ################################ที่จอดรถ######################################################
        ################################เฟอร์นิเจอร์######################################################
        furniture1 = self.wd.find_element(By.ID,'furniture1')
        furniture2 = self.wd.find_element(By.ID,'furniture2')
        furniture3 = self.wd.find_element(By.ID,'furniture3')
        # if furniture =='มี':
        #     furniture1.click()
        # elif furniture =='ไม่มี':
        #     furniture2.click()
        # else:
        #     furniture3.click()
        furniture3.click()
        ################################เฟอร์นิเจอร์######################################################
        ################################เครื่องใช้ไฟฟ้า######################################################
        electric1 = self.wd.find_element(By.ID,'electric1')
        electric2 = self.wd.find_element(By.ID,'electric2')
        electric3 = self.wd.find_element(By.ID,'electric3')
        # if furniture =='มี':
        #     electric1.click()
        # elif furniture =='ไม่มี':
        #     electric2.click()
        # else:
        #     electric3.click()
        electric3.click()
        ################################เครื่องใช้ไฟฟ้า######################################################
        ################################การต่อเติมพื้นที่ใช้สอย######################################################

        ################################การต่อเติมพื้นที่ใช้สอย######################################################
        ################################สิ่งอำนวยความสะดวก######################################################

        ################################สิ่งอำนวยความสะดวก######################################################
        ################################ที่ตั้ง######################################################
        #bangkok
        if self.state[0] == 'กรุงเทพมหานคร':
                        
            property_city_bkk = Select(self.wd.find_element_by_name ('property_city_bkk'))
            if self.city[0] == 'ราชเทวี':
                property_city_bkk.select_by_visible_text('ราชเทวี (สานเสนใน)')
            elif self.city[0] == 'พระนคร':
                property_city_bkk.select_by_visible_text('พระนคร (ราชดำเนิน)')
            elif self.city[0] == 'ประเวศ':
                property_city_bkk.select_by_visible_text('ประเวศ (อ่อนนุช)')
            else:
                property_city_bkk.select_by_visible_text((self.city[0]))

            
        #bangkok
        #จังหวัดที่ตั้ง
        else:
            property_country_2 = Select(self.wd.find_element_by_name ('property_country_2'))
            property_country_2.select_by_visible_text((self.state[0]))
        # property_country_2.select_by_visible_text('อ่างทอง')

        #จังหวัดที่ตั้ง
        #อำเภอ
            WebDriverWait(self.wd, 10).until(EC.visibility_of_element_located((By.ID, "property_city_2")))
            time.sleep(1)
            try:
                try:
                    property_city_2 = Select(self.wd.find_element_by_name ('property_city_2'))
                    property_city_2.select_by_visible_text((self.city[0]))
                except:
                    property_city_2 = Select(self.wd.find_element_by_name ('property_city_2'))
                    property_city_2.select_by_visible_text((self.city[0].replace(' ', '')))
            except:
                property_city_2 = Select(self.wd.find_element_by_name ('property_city_2'))

        #อำเภอ

        ################################ที่ตั้ง######################################################
        ################################ขั้น2ราคา######################################################
        #ขาย
        if 'sell' in self.list_type:
            sell_price = self.wd.find_element(By.ID,'selling_price')
            sell_price.send_keys(self.f_price[0])
        #ขาย
        #เช่า
        elif 'rent' in self.list_type:
            rent_price = self.wd.find_element(By.ID,'rent_price')
            rent_price.send_keys(self.f_price[0])
            type_forrent = Select(self.wd.find_element_by_id ('type_forrent'))
            type_forrent.select_by_visible_text('ต่อเดือน')
        # type_forrent.select_by_visible_text('ต่อสัปดาห์')
        # type_forrent.select_by_visible_text('ต่อวัน')
        else:
            self.wd.find_element(By.NAME, 'notprice').click()
            
        #เช่า
        #ราคาต่อหน่อย
        # price_unit = self.wd.find_element(By.ID,'price_unit')
        # price_unit.send_keys('111')
        # typeunit1 = self.wd.find_element(By.ID,'typeunit1')
        # typeunit2 = self.wd.find_element(By.ID,'typeunit2')
        # typeunit3 = self.wd.find_element(By.ID,'typeunit3')
        # #check if
        # typeunit1.click()
        #ราคาต่อหน่อย

        ################################ขั้น2ราคา######################################################
        ################################ขั้น3######################################################
        def state3():
            # self.wd.find_element(By.ID, 'info0').click()
            # self.wd.find_element(By.ID, 'info1').click()
            # self.wd.find_element(By.ID, 'info2').click()
            # self.wd.find_element(By.ID, 'info3').click()
            # self.wd.find_element(By.ID, 'info4').click()
            # self.wd.find_element(By.ID, 'info5').click()
            # self.wd.find_element(By.ID, 'info6').click()
            # self.wd.find_element(By.ID, 'info7').click()
            # self.wd.find_element(By.ID, 'info8').click()
            # self.wd.find_element(By.ID, 'info9').click()
            # self.wd.find_element(By.ID, 'info10').click()
            # self.wd.find_element(By.ID, 'info11').click()
            # self.wd.find_element(By.ID, 'info12').click()
            # self.wd.find_element(By.ID, 'info13').click()
            # self.wd.find_element(By.ID, 'info14').click()
            # self.wd.find_element(By.ID, 'info15').click()
            # self.wd.find_element(By.ID, 'info16').click()
            # self.wd.find_element(By.ID, 'info17').click()
            # self.wd.find_element(By.ID, 'info18').click()
            # self.wd.find_element(By.ID, 'info19').click()
            # self.wd.find_element(By.ID, 'info20').click()
            # self.wd.find_element(By.ID, 'info21').click()
            # self.wd.find_element(By.ID, 'info22').click()
            # self.wd.find_element(By.ID, 'info23').click()
            # self.wd.find_element(By.ID, 'info24').click()
            # self.wd.find_element(By.ID, 'info25').click()
            # self.wd.find_element(By.ID, 'info26').click()
            # self.wd.find_element(By.ID, 'info27').click()
            self.wd.find_element(By.ID, 'info28').click()
            # self.wd.find_element(By.ID, 'info29').click()
            # self.wd.find_element(By.ID, 'info30').click()
            # self.wd.find_element(By.ID, 'info31').click()
            # self.wd.find_element(By.ID, 'info32').click()
            # self.wd.find_element(By.ID, 'info33').click()
            # self.wd.find_element(By.ID, 'info34').click()
            self.wd.find_element(By.ID, 'info35').click()
            # self.wd.find_element(By.ID, 'info36').click()
            # self.wd.find_element(By.ID, 'info37').click()
            # self.wd.find_element(By.ID, 'info38').click()
            # self.wd.find_element(By.ID, 'info39').click()
            # self.wd.find_element(By.ID, 'info40').click()
            # self.wd.find_element(By.ID, 'info41').click()
            # self.wd.find_element(By.ID, 'info42').click()
            # self.wd.find_element(By.ID, 'info43').click()
            # self.wd.find_element(By.ID, 'info44').click()
            # self.wd.find_element(By.ID, 'info45').click()
            self.wd.find_element(By.ID, 'info46').click()
            # self.wd.find_element(By.ID, 'info47').click()
            # self.wd.find_element(By.ID, 'info48').click()
            # self.wd.find_element(By.ID, 'info49').click()
            # self.wd.find_element(By.ID, 'info50').click()
            # self.wd.find_element(By.ID, 'info51').click()
            # self.wd.find_element(By.ID, 'info52').click()
            # self.wd.find_element(By.ID, 'info53').click()
            # self.wd.find_element(By.ID, 'info54').click()
            # self.wd.find_element(By.ID, 'info55').click()
            # self.wd.find_element(By.ID, 'info56').click()
            # self.wd.find_element(By.ID, 'info57').click()
            # self.wd.find_element(By.ID, 'info58').click()
            # self.wd.find_element(By.ID, 'info59').click()
            # self.wd.find_element(By.ID, 'info60').click()
            # self.wd.find_element(By.ID, 'info61').click()
            # self.wd.find_element(By.ID, 'info62').click()
            # self.wd.find_element(By.ID, 'info63').click()
            # self.wd.find_element(By.ID, 'info64').click()
            # self.wd.find_element(By.ID, 'info65').click()
            # self.wd.find_element(By.ID, 'info66').click()

            # self.wd.find_element(By.ID, 'info70').click()
            # self.wd.find_element(By.ID, 'info71').click()
            # self.wd.find_element(By.ID, 'info72').click()
            # self.wd.find_element(By.ID, 'info73').click()
            # self.wd.find_element(By.ID, 'info74').click()
            # self.wd.find_element(By.ID, 'info75').click()
            # self.wd.find_element(By.ID, 'info76').click()
            # self.wd.find_element(By.ID, 'info77').click()
            # self.wd.find_element(By.ID, 'info78').click()
            # self.wd.find_element(By.ID, 'info79').click()
            # self.wd.find_element(By.ID, 'info80').click()
            # self.wd.find_element(By.ID, 'info81').click()
            # self.wd.find_element(By.ID, 'info82').click()
            # self.wd.find_element(By.ID, 'info83').click()
            # self.wd.find_element(By.ID, 'info84').click()
            # self.wd.find_element(By.ID, 'info85').click()
            # self.wd.find_element(By.ID, 'info86').click()
            # self.wd.find_element(By.ID, 'info87').click()
            # self.wd.find_element(By.ID, 'info88').click()
            # self.wd.find_element(By.ID, 'info89').click()
            # self.wd.find_element(By.ID, 'info90').click()
            # self.wd.find_element(By.ID, 'info91').click()
            # self.wd.find_element(By.ID, 'info92').click()
            # self.wd.find_element(By.ID, 'info93').click()
            # self.wd.find_element(By.ID, 'info94').click()
            # self.wd.find_element(By.ID, 'info95').click()
            # self.wd.find_element(By.ID, 'info96').click()
            # self.wd.find_element(By.ID, 'info97').click()
            # self.wd.find_element(By.ID, 'info98').click()
            # self.wd.find_element(By.ID, 'info99').click()
            # self.wd.find_element(By.ID, 'info100').click()
            # self.wd.find_element(By.ID, 'info101').click()
            # self.wd.find_element(By.ID, 'info102').click()
            # self.wd.find_element(By.ID, 'info103').click()
            # self.wd.find_element(By.ID, 'info104').click()
            # self.wd.find_element(By.ID, 'info105').click()
            # self.wd.find_element(By.ID, 'info106').click()
            # self.wd.find_element(By.ID, 'info107').click()
            # self.wd.find_element(By.ID, 'info108').click()
            # self.wd.find_element(By.ID, 'info109').click()
            # self.wd.find_element(By.ID, 'info110').click()
            # self.wd.find_element(By.ID, 'info111').click()
            # self.wd.find_element(By.ID, 'info112').click()
            # self.wd.find_element(By.ID, 'info113').click()
            # self.wd.find_element(By.ID, 'info114').click()
            # self.wd.find_element(By.ID, 'info115').click()
            # self.wd.find_element(By.ID, 'info116').click()
            # self.wd.find_element(By.ID, 'info117').click()
            # self.wd.find_element(By.ID, 'info118').click()
            # self.wd.find_element(By.ID, 'info119').click()
            # self.wd.find_element(By.ID, 'info120').click()
            # self.wd.find_element(By.ID, 'info121').click()
            # self.wd.find_element(By.ID, 'info122').click()
            # self.wd.find_element(By.ID, 'info123').click()
            # self.wd.find_element(By.ID, 'info124').click()
            # self.wd.find_element(By.ID, 'info125').click()
            # self.wd.find_element(By.ID, 'info126').click()
            # self.wd.find_element(By.ID, 'info127').click()
            # self.wd.find_element(By.ID, 'info128').click()
            # # self.wd.find_element(By.ID, 'info129').click()
            # # self.wd.find_element(By.ID, 'info130').click()
            # # self.wd.find_element(By.ID, 'info131').click()
            # # self.wd.find_element(By.ID, 'info132').click()
            # # self.wd.find_element(By.ID, 'info133').click()
            # # self.wd.find_element(By.ID, 'info134').click()
            self.wd.find_element(By.ID, 'info67').click()
            self.wd.find_element(By.ID, 'info68').click()
            self.wd.find_element(By.ID, 'info69').click()
        state3()
        ################################ขั้น3######################################################
        ################################captcha######################################################
        captcha_src = self.wd.find_element_by_xpath('//*[@id="inAddnew"]/table/tbody/tr[5]/td/table/tbody/tr/td/div[1]/img').get_attribute("src")

        captcha = captcha_src.split('=')[1]

        self.wd.find_element(By.ID, 'string1').send_keys(captcha)
        ################################captcha######################################################
        #########################################################
        self.wd.find_element(By.ID, 'imgSubmitForm').click()
        time.sleep(3)
        try:
            at = self.wd.switch_to.alert.text
            print(at)
            # if 'ซ้ำ' in at:
            return at

            self.wd.switch_to.alert.accept()
            self.wd.find_element(By.ID,"headtitle").send_keys('00')
        except:
            print('pass')
        # if self.wd.switch_to.alert.text == '*** กรุณาแก้ไขหัวข้อประกาศ ใช้ซ้ำ!! กับประกาศรายการอื่น ***':
        #     print('*** กรุณาแก้ไขหัวข้อประกาศ ใช้ซ้ำ!! กับประกาศรายการอื่น ***')
        # img_button = 
        # time.sleep(27)
        # print('27 sec')

        time.sleep(5)
        print('5 sec')

        if self.wd.current_url == 'https://www.thaihometown.com/addcontacts':
            try:
                fininsh_xpath = '//*[@id="Finish"]/table/tbody/tr/td/strong'
                self.wait.until(EC.element_to_be_clickable((By.XPATH, fininsh_xpath)))
                fininsh_status = self.wd.find_element(By.XPATH, fininsh_xpath).get_attribute('innerHTML')
            except:
        #         self.wd.find_element(By.XPATH, '')
                return 'คุณยังไม่สามารถสร้างประกาศใหม่ได้ รอประกาศใหม่อีกครั้งในอีก 10 นาที'

        #########################################################
    ############################picture###########################################
        print(self.wd.current_url)
        self.wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@id="sample"]/div[1]')))
        print(self.wd.current_url)
        self.wd.find_element(By.XPATH, '//*[@id="sample"]/div[1]').click()
        print("Click upload pic")
        print(self.wd.current_url)
        try:
            wi_XPATH =  '/html/body/input[{}]'
            for n_img in range(int(len(galleryUrls))):
            # int_img = int(len(img))
            # self.wd.find_element(By.ID, 'upload_image1').click()
                print(wi_XPATH.format(n_img+1))
                print(Img_rz_path[n_img])
                self.wd.find_element(By.XPATH, wi_XPATH.format(n_img+1)).send_keys(Img_rz_path[n_img])
                print('upload_image'+Img_rz_path[n_img])
            print('upload_image')
            time.sleep(3)
        except:
            print('no img insert')

    ############################picture###########################################
    ############################map###########################################

        # try:
        #     return 'ไม่สามารถค้นหาสถานที่ใน google maps ได้'

        # #     self.wd.find_element(By.XPATH,'//*[@id="mainbody"]/h3[2]/div/div[2]/a/div').click()
        # #     tht_tab = self.wd.current_window_handle
        # #     self.wd.find_element(By.XPATH, '/html/body/div[2]/div[2]/table/tbody/tr[1]/td/a').click()
        # #     all_handles = self.wd.window_handles
        # # #เปลี่ยนแท็ป
        # #     for handle in all_handles:
        # #         if handle != tht_tab:
        # #             self.wd.switch_to.window(handle)
        # #             self.wd.get('https://www.google.co.th/maps')

        # #             self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(self.address[0])
        # #             print(self.address[0])
        # #             self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
        # #             print('clicksearch')
        # #             time.sleep(2)
        # #             try:
        # #                 self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[5]/button')))
        # #                 try:
        # #                     try:
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]/div[1]/div/div[2]').click()
        # #                         try:
        # #                             WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[5]/button')))
        # #                         except:
        # #                             time.sleep(2)
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[3]/div/div[1]/div/div/div[2]/div[4]/div[5]/button').click()
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div[1]/div[2]/button[2]').click()
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()

        # #                     except:
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[5]/button').click()
        # #                         print('clickshare')
        # #                         time.sleep(2)
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div[1]/div[2]/button[2]').click()
        # #                         print('clickiframe')
        # #                         time.sleep(1)
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                         print('clickcopyiframe')
        # #                         time.sleep(1)
        # #                         try:
        # #                             self.wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@id="Ng57nc"]/div/button')))
        # #                             self.wd.find_element(By.XPATH,'//*[@id="Ng57nc"]/div/button').click()
        # #                         except:
        # #                             self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                             self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                             print('clickcopyiframe')
        # #                             time.sleep(1)

        # #                 except:
        # #                     try:
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]/div[3]/div/a').click()
        # #                         print('click1')
        # #                         time.sleep(2)
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[3]/div/div[1]/div/div/div[2]/div[4]/div[5]/button').click()
        # #                         print('clickshare')
        # #                         time.sleep(2)
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div[1]/div[2]/button[2]').click()
        # #                         print('clickiframe')
        # #                         time.sleep(1)
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                         print('clickcopyiframe')
        # #                     except:
        # #                         self.wd.get('https://www.google.co.th/maps')
        # #                         self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(self.address[0])
        # #                         self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
        # #                         WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]/div[1]/div/a')))
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]/div[1]/div/a').click()
        # #                         WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[3]/div/div[1]/div/div/div[2]/div[4]/div[5]/button')))
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[3]/div/div[1]/div/div/div[2]/div[4]/div[5]/button').click()
        # #                         print('clickshare')
        # #                         time.sleep(2)
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div[1]/div[2]/button[2]').click()
        # #                         print('clickiframe')
        # #                         time.sleep(1)
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                         print('clickcopyiframe')

        # #             except:
        # #                 try:
        # #                     self.wd.get('https://www.google.co.th/maps')
        # #                     self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(self.address[0])
        # #                     self.wd.find_element(By.XPATH, '//*[@id="ydp1wd-haAclf"]/div/div').click()
        # #                     self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[5]/button')))
        # #                     self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[5]/button').click()
        # #                     print('clickshare')
        # #                     time.sleep(2)
        # #                     self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div[1]/div[2]/button[2]').click()
        # #                     print('clickiframe')
        # #                     time.sleep(1)
        # #                     self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                     print('clickcopyiframe')
        # #                     time.sleep(1)
        # #                     try:
        # #                         self.wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@id="Ng57nc"]/div/button')))
        # #                         self.wd.find_element(By.XPATH,'//*[@id="Ng57nc"]/div/button').click()
        # #                     except:
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                         self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                         print('clickcopyiframe')
        # #                         time.sleep(1)
        # #                 except:
        # #                     self.wd.get('https://www.google.co.th/maps')
        # #                     self.wd.find_element(By.XPATH, '//*[@id="searchboxinput"]').send_keys(self.address[0])
        # #                     self.wd.find_element(By.XPATH, '//*[@id="searchbox-searchbutton"]').click()
        # #                     time.sleep(1)
        # #                     try:
        # #                         WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]/div[1]/div/a')))
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]/div[1]/div/a').click()
        # #                     except:
        # #                         WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]/div[3]/div/a')))
        # #                         self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]/div[3]/div/a').click()

        # #                     time.sleep(1)
        # #                     WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[3]/div/div[1]/div/div/div[2]/div[4]/div[5]/button')))
        # #                     self.wd.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[3]/div/div[1]/div/div/div[2]/div[4]/div[5]/button').click()
        # #                     print('clickshare')
        # #                     time.sleep(3)
        # #                     # WebDriverWait(wd, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div[1]/div[2]/button[2]')))
        # #                     self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div[1]/div[2]/button[2]').click()
        # #                     print('clickiframe')
        # #                     time.sleep(2)
        # #                     # WebDriverWait(wd, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]')))
        # #                     self.wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                     # wd.find_element(By.XPATH, '//*[@id="modal-dialog"]/div/div[2]/div/div[3]/div/div/div/div[3]/div[1]/button[2]').click()
        # #                     print('clickcopyiframe')

        # # #เปลี่ยนแท็ป
        # #     self.wd.switch_to.window(tht_tab)
        # #     time.sleep(1)

        # #     self.wd.find_element(By.ID, 'iframemaps').send_keys(Keys.CONTROL, 'v')
        # #     print('iframemaps')
        # #     self.wd.find_element(By.ID, 'Submit_SEND').click()
        # #     print('Submit_SEND')
        # #     self.wd.find_element(By.ID, 'Submit_FORM').click()
        # #     print('Submit_FORM')
        # #     time.sleep(1)

        # except:
        #     return 'ไม่สามารถค้นหาสถานที่ใน google maps ได้'
    ############################map###########################################

    #     WebDriverWait(self.wd, 10).until(EC.element_to_be_clickable((By.XPATH,"//label[@for='agreement']"))).click()
    #     print("agreement.click()")
    #     post_button = self.wd.find_element(By.XPATH, '//*[@id="__next"]/div/div[2]/div[2]/div/form/div/div/button')
    #     time.sleep(5)
    #     post_button.click()
    #     print("Posted")

    ################################fill_data####################################
    ###########################loop###############################################
    # for data in doc:
    #     clickpost()
    #     fill_data()

    ###########################loop###############################################
    def quit(self):
        self.wd.quit()
################################################################
class starttht:
    def startbotthaihometown(self,id):
        try:
            fn = tht()

            fn.click_login()
            fn.query(id)
            fn.query_uw()
            fn.fill_login()
            fn.click_createpost()
            fd = fn.fill_data()
            print('fd',fd)
            if fd == None:
                print(fd)
                fn.quit()
                return True
            if fd != None:
                fn.quit()
                return fd
            # fn.quit()
            # return True

        except Exception as e:
            print(e)
            fn.quit()
            return False

        # # try:
        #     fill_data()
        # except Exception as e:
        #     print(e)
################################################################


