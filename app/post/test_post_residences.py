from pickle import GLOBAL
import requests
from bs4 import BeautifulSoup
import csv
import itertools
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait,Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import io
from PIL import Image
import time
import os
from pymongo import MongoClient
import pymongo
from collections import ChainMap
from bson.objectid import ObjectId
import pyperclip
import random

################################################################
class residences:
    def __init__(self):
        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        # options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-dev-shm-using") 
        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        options.add_argument("start-maximized") 
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        options.add_argument(f'user-agent={user_agent}')
        self.wd = webdriver.Chrome(ChromeDriverManager().install(), options = options)

        # self.wd = webdriver.Chrome(chrome_options=chromeOptions) 
        # self.wd = webdriver.Chrome(PATH, chrome_options=chrome_options)
        self.wd.maximize_window()

        ################################################################
        i=0
        cpost = 1
        ####url################################
        url = "https://www.residences.in.th/"
        login_url = "https://www.residences.in.th/users/sign_in"

        self.wd.get(login_url)
        self.wait = WebDriverWait(self.wd, 60)
        ##Start of db zone###
        # self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        
        self.mydb = self.myclient["devXInvester"]
        self.mycol = self.mydb["posts"]
        self.mycol_properties = self.mydb["properties"]
        self.mycol_uw = self.mydb["websites"]

        # self.mydb = self.myclient["test_X-invester"]
        # self.mycol = self.mydb["test_X-invester_ppth_newlo2"]

        self.i=0
        self.cpost = 1
        self.n_pic = 1
        self.post_topic = []
        self.list_type = []
        self.bedroom = []
        self.toilet = []
        self.s_price = []
        self.f_price = []
        self.min_area = []
        self.max_area = []
        self.description = []
        self.property_type = []
        self.watchlistPropertyTypes =[]
        self.property = []
        self.imgs = []
        self.state = []
        self.city = []
        self.subcity = []
        self.address = []
        self.addressEN = []
        self.imgs_path = []

        self.no_pjn = []
        self.JS_ADD_TEXT_TO_INPUT = """
            var elm = arguments[0], txt = arguments[1];
            elm.value += txt;
            elm.dispatchEvent(new Event('change'));
            """
        ####url################################
    
    def query(self,id):
        print(id)
        myquery = {"_id":ObjectId(id)}

        mydoc = self.mycol.find(myquery)
        global galleryUrls
        global ImgsPath

        stage_lookup_property = {
        "$lookup": {
        "from": "properties", 
        "localField": "property", 
        "foreignField": "_id", 
        "as": "data_property",
        }
        }

        stage_lookup_subCity = {
        "$lookup": {
        "from": "subCities", 
        "localField": "subCity", 
        "foreignField": "_id", 
        "as": "data_subCity",
        }
        }
        stage_lookup_city = {
        "$lookup": {
        "from": "cities", 
        "localField": "city", 
        "foreignField": "_id", 
        "as": "data_city",
        }
        }
        stage_lookup_state = {
        "$lookup": {
        "from": "states", 
        "localField": "state", 
        "foreignField": "_id", 
        "as": "data_state",
        }
        }
        # Limit to the first 5 documents:
        stage_limit_5 = { "$limit": 1 }

        pipeline_property = [
        stage_lookup_property,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]

        pipeline_subCity = [
        stage_lookup_subCity,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]
        pipeline_city = [
        stage_lookup_city,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]
        pipeline_state = [
        stage_lookup_state,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]

        # self.properties = self.mycol.aggregate(pipeline_property)
        # self.subCities = self.mycol_properties.aggregate(pipeline_subCity)
        # self.Cities = self.mycol_properties.aggregate(pipeline_city)
        # self.States = self.mycol_properties.aggregate(pipeline_state)

        # for property in self.properties:
        #     # pprint(type(property))
        #     data_property = property.get('data_property')
        #     data_property_dict = (dict(ChainMap(*data_property)))
        #     nameTH = data_property_dict.get('nameTH')
        #     self.address.append(nameTH)
        #     # city = data_property_dict.get('city')
        #     for subCity in self.subCities:
        #         # print(type(subCity))
        #         data_subCity = subCity.get('data_subCity')
        #         data_subCity_dict = (dict(ChainMap(*data_subCity)))
        #         subCity = data_subCity_dict.get('name')
        #         print(subCity)
        #         self.subcity.append(subCity)
        #         # print((subCity))
        #     for City in self.Cities:
        #         data_city = City.get('data_city')
        #         data_city_dict = (dict(ChainMap(*data_city)))
        #         City = data_city_dict.get('name')
        #         print(City)
        #         self.city.append(City)
        #     for State in self.States:
        #         data_State = State.get('data_state')
        #         data_State_dict = (dict(ChainMap(*data_State)))
        #         State = data_State_dict.get('name')
        #         print(State)
        #         self.state.append(State)


        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')                

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)

            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)                

        mycol_properties_query = {"_id":ObjectId(self.property[0])}
        mydoc_mycol_properties = self.mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            Province = x.get('province')
            print(nameEN,nameTH,City)

            self.address.append(nameTH)
            self.addressEN.append(nameEN)
            self.city.append(City)
            self.state.append(Province)


        def pnt():
            print('pnt',self.address)
        pnt()
        # lat_long.append(Latitude)
        # lat_long.append(Longitude)
        # if ['rent'] in self.list_type or ['เช่า'] in self.list_type:

    ##########################query################################################################
    def query_uw(self):
        myquery = {'name':'residences'}
        myuws = self.mycol_uw.find(myquery)
        for uws in myuws:
            users = uws['users']

        global user
        global passwd

        user = users[0]['username']
        passwd = users[0]['password']

        print('print',user,passwd)

        r = random.choice(users)

        u = r['username']
        p = r['password']
        ##########################click_login################################################################
    def click_login(self):
        self.wd.get('https://www.residences.in.th/users/sign_in')
        time.sleep(1)
        # self.wd.find_element(By.XPATH, '//*[@id="loginModal"]/div/div/div/div[1]/div/div[2]/div[5]/div[2]/div/a/div/div').click()
        # print("google")

        # print("Click Login_Button")

    # click_login()

    ##########################click_login##########################################

    ###############################fill_login##########################################
    def fill_login(self):
        self.wd.implicitly_wait(0.5)

        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="user_email"]')))#.send_keys("testgingging@gmail.com")
        self.wd.find_element(By.XPATH, '//*[@id="user_email"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="user_email"]').send_keys("jatossob@gmail.com")
        # self.wd.find_element(By.XPATH, '//*[@id="user_email"]').send_keys("fahfah.prapapan@gmail.com")
        self.wd.find_element(By.XPATH, '//*[@id="user_email"]').send_keys(user)

        print("Click user_email finish")

        # self.wd.find_element(By.ID, 'user_password').send_keys("qwerty123456Zx")
        # self.wd.find_element(By.ID, 'user_password').send_keys("Prapapan1")
        self.wd.find_element(By.ID, 'user_password').send_keys(passwd)

        time.sleep(1)
        self.wd.find_element(By.XPATH, '//*[@id="new_user"]/div[5]/input').click()
        print("Click Login_Button finish")
        if self.wd.current_url == 'https://www.residences.in.th/users/sign_in':
            print("cannot Login")
            pass
            # return False
        elif self.wd.current_url == 'https://www.residences.in.th/dashboard/apartments':
            print("Login finish")
        time.sleep(3)

    ###############################fill_login##########################################


    #######################click_createpost#######################################
    def click_createpost_A(self):
        self.wd.get('https://www.residences.in.th/apartments/new')
        time.sleep(5)

        self.wait.until(EC.element_to_be_clickable((By.ID, 'accept-and-continue-apartment-listing')))
        self.wd.find_element(By.ID, 'accept-and-continue-apartment-listing').click()

    def click_createpost_C(self):
        self.wd.get('https://www.residences.in.th/listings/new')
        time.sleep(5)

        # self.wd.find_element(By.XPATH, '').click()

    ########################click_createpost######################################

    ################################fill_data####################################
    def fill_data_A(self):
    ###################################PAGE1###################################
    ###################################ข้อมูลที่พัก###################################
        self.wd.find_element(By.ID, 'apartment_name').send_keys('test')
        self.wd.find_element(By.ID, 'apartment_en_name').send_keys('test')
        # self.wd.find_element(By.ID, 'apartment_apartment_type').send_keys('test')
        select_apartment_type = Select(self.wd.find_element(By.ID, 'apartment_apartment_type'))
        select_apartment_type.select_by_visible_text('อพาร์ทเม้นท์ หอพัก')
        select_apartment_province = Select(self.wd.find_element(By.ID, 'apartment_province_id'))
        select_apartment_province.select_by_visible_text('กรุงเทพมหานคร')
        time.sleep(2)
        self.wd.switch_to.alert.accept()
        select_apartment_amphur = Select(self.wd.find_element(By.ID, 'apartment_amphur_id'))
        select_apartment_amphur.select_by_visible_text('คลองสาน')
        time.sleep(2)
        self.wd.switch_to.alert.accept()
        select_apartment_district = Select(self.wd.find_element(By.ID, 'apartment_district_id'))
        select_apartment_district.select_by_visible_text('คลองต้นไทร')
        time.sleep(2)
        self.wd.switch_to.alert.accept()

        # try:
        #     select_apartment_amphur = Select(self.wd.find_element(By.ID, 'apartment_amphur_id'))
        #     select_apartment_amphur.select_by_visible_text('คลองสาน')
        # except:
        #     time.sleep(2)
        #     self.wd.switch_to.alert.accept()
        #     select_apartment_amphur = Select(self.wd.find_element(By.ID, 'apartment_amphur_id'))
        #     select_apartment_amphur.select_by_visible_text('คลองสาน')
        # try:
        #     select_apartment_district = Select(self.wd.find_element(By.ID, 'apartment_district_id'))
        #     select_apartment_district.select_by_visible_text('คลองต้นไทร')
        # except:
        #     time.sleep(2)
        #     self.wd.switch_to.alert.accept()
        #     select_apartment_district = Select(self.wd.find_element(By.ID, 'apartment_district_id'))
        #     select_apartment_district.select_by_visible_text('คลองต้นไทร')
        # try:
        #     self.wd.find_element(By.ID, 'apartment_postcode').send_keys('000')
        # except:
        #     time.sleep(2)
        #     self.wd.switch_to.alert.accept()
        #     self.wd.find_element(By.ID, 'apartment_postcode').send_keys('000')
        

        self.wd.find_element(By.ID, 'apartment_address').send_keys('000')
        self.wd.find_element(By.ID, 'apartment_road').send_keys('000')
        self.wd.find_element(By.ID, 'apartment_street').send_keys('000')

    ###################################ข้อมูลที่พัก###################################
    ################ข้อมูลติดต่อ#######################################
        self.wd.find_element(By.ID, 'apartment_staff').send_keys('test')
        self.wd.find_element(By.ID, 'apartment_telephone').send_keys('test')
        self.wd.find_element(By.ID, 'apartment_en_telephone').send_keys('test')
        self.wd.find_element(By.ID, 'apartment_email').send_keys('test')
        self.wd.find_element(By.ID, 'apartment_line_user_id').send_keys('test')
        self.wd.find_element(By.ID, 'apartment_facebook_url').send_keys('https://th-th.facebook.com/Dsssradd/')


    ################ข้อมูลติดต่อ#######################################
    ###################################รายละเอียด###################################
        self.wd.find_element(By.XPATH, '/html/body').send_keys('test')
        self.wd.find_element(By.XPATH, '/html/body').send_keys('test')


    ###################################รายละเอียด###################################
        self.wd.find_element(By.XPATH,'//*[@id="btn-submit-new_apartment"]').click()
    ###################################PAGE1###################################
    ###################################PAGE2###################################
    ###################################สิ่งอำนวยความสะดวก###################################
        self.wd.find_element(By.XPATH,'//*[@id="facility_6"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_11"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_14"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_13"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_2"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_5"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_9"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_8"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_17"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_12"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_3"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_1"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_4"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_16"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_10"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_7"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="facility_15"]').click()

        self.wd.find_element(By.XPATH,'//*[@id="central_facility_4"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_10"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_1"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_5"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_3"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_7"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_8"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_12"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_11"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_2"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_6"]').click()
        self.wd.find_element(By.XPATH,'//*[@id="central_facility_9"]').click()

    ###################################สิ่งอำนวยความสะดวก###################################
    ###################################รูปภาพที่พัก###################################
        # self.wd.find_element(By.ID,'apartment_rooms_attributes_1661934475456_name').send_keys('D:\\work\\web_scraping\\test_pic\\404_1.JPEG')
        # time.sleep(3)

    ######################
        self.wd.find_element(By.XPATH, '//*[@id="main"]/div[2]/div/div/div[4]/div/div/div/div[2]/button').click()

        self.wd.find_element(By.XPATH, '//*[@id="confirm-do-not-validation-listing-images"]/a').click()

    ###################################รูปภาพที่พัก###################################
        self.wd.find_element(By.XPATH, '//*[@id="main"]/div[2]/div/div/div[4]/div/div/div/div[2]/button').click()
    ###################################PAGE2###################################
    ###################################PAGE3###################################
    ###################################ประเภทห้องพัก###################################

        # self.wd.find_element(By.CLASS_NAME,'required form-control room-name').click()
        # self.wd.find_element(By.CLASS_NAME,'required form-control room-name').send_keys('test')
        # select_apartment_room_type = Select(self.wd.find_element(By.CLASS_NAME, 'room_type required form-control'))
        # select_apartment_room_type.select_by_visible_text('สตูดิโอ')
        # select_apartment_room_type.select_by_visible_text('4 ห้องนอน')
        # self.wd.find_element(By.CLASS_NAME,'col_room_size form-control').send_keys('0')
        # self.wd.find_element(By.CLASS_NAME,'monthly-rate').click()
        # self.wd.find_element(By.CLASS_NAME,'min-room-price-monthly room-price-monthly room_price form-control').send_keys('0')
        # self.wd.find_element(By.CLASS_NAME,'max-room-price-monthly room-price-monthly room_price form-control').send_keys('0')
        # self.wd.find_element(By.CLASS_NAME,'daily-rate').click()
        # self.wd.find_element(By.CLASS_NAME,'min-room-price-daily room-price-daily room_price form-control').send_keys('0')
        # self.wd.find_element(By.CLASS_NAME,'max-room-price-daily room-price-daily room_price form-control').send_keys('0')
        # # self.wd.find_element(By.CLASS_NAME,'apartment[rooms_attributes][1661935733499][available]').click()

    ###################################ประเภทห้องพัก###################################
    ###################################ค่าใช้จ่าย###################################
        self.wd.find_element(By.ID,'apartment_water_price_type_6').click()
        self.wd.find_element(By.ID,'apartment_electric_price_type_4').click()
        self.wd.find_element(By.ID,'apartment_deposit_type_4').click()
        self.wd.find_element(By.ID,'apartment_advance_fee_type_4').click()
        self.wd.find_element(By.ID,'apartment_phone_price_type_4').click()
        self.wd.find_element(By.ID,'apartment_internet_price_type_4').click()

    ###################################ค่าใช้จ่าย###################################
    ###################################โปรโมชั่น###################################
        self.wd.find_element(By.ID,'apartment_has_promotion_0').click()

    ###################################โปรโมชั่น###################################
    # /html/body/div/div[2]/div/div/div[4]/div/div/div/div/form/div[3]/button
        # self.wd.find_element(By.CLASS_NAME,'btn pure-btn btn-large big-line lightweight-line').click()
    ###################################PAGE3###################################


    def fill_data_C(self):#finish
        if self.wd.current_url == 'https://www.residences.in.th/':
            print('cannot login')
            # pass
            return 'cannot login'
        print('fill_data_C')
    ###################################PAGE1###################################
    ###################################ข้อมูลพื้นฐาน###################################
        # self.wd.find_element(By.ID, 'listing_title').send_keys('test')
        try:
            flash_alert = self.wd.find_element(By.XPATH, '//*[@id="flash_alert"]').get_attribute('innerHTML')
            print(flash_alert)

            if flash_alert != '':
                return flash_alert
        except:
            print("NO flash_alert")
        
        def headtitle_th():
            # title_eng = self.wd.find_element(By.ID, 'listing_title_en')
            # act = ActionChains(self.wd)
            # pyperclip.copy(str(self.post_topic[0]))

            # act.send_keys_to_element(title_eng).key_down(Keys.SHIFT).key_down(Keys.TAB).key_up(Keys.SHIFT).send_keys(self.post_topic[0]).perform()
            # act.send_keys_to_element(title_eng).key_down(Keys.SHIFT).key_down(Keys.TAB).key_up(Keys.SHIFT).key_up(Keys.TAB).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
            print(self.post_topic[0])
            headtitle_th = self.wd.find_element(By.ID, "listing_title")
            headtitle_th.send_keys(self.post_topic[0])
            # headtitle_th.click()
            # act.send_keys_to_element(headtitle_th).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
            # self.wd.save_screenshot('topic.png')
            # self.wd.find_element(By.ID,'listing_title').send_keys(4)

        headtitle_th()
        print('headtitle_th()')
        
        def headtitle_en():
            headtitle_en = self.wd.find_element(By.ID, "listing_title_en")
            # pyperclip.copy(str(self.post_topic[0]))
            # act = ActionChains(self.wd)
            # act.send_keys_to_element(headtitle_en).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
            self.wd.find_element(By.ID,'listing_title_en').send_keys(self.addressEN[0])

        headtitle_en()
        print('headtitle_en()')


        self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[1]/div[1]/div/fieldset[1]/div[3]/div/div[1]/span/span[1]').click()
        try:
            print('0')
            print('self.addressEN[0][:-1]',self.addressEN[0][:-1])
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input'))).send_keys(self.addressEN[0][:-1])
            # self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.address[0])
            # self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.addressEN[0])

            time.sleep(2)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="select2-listing_listing_project_id-results"]/li[1]')))
            project_name = self.wd.find_element(By.XPATH, '//*[@id="select2-listing_listing_project_id-results"]/li[1]')
            # project_name = self.wd.find_element(By.CLASS_NAME, 'select2-results__option select2-results__option--highlighted')

            print('project_name.click() 1')
            project_name.click()
        except:
            try:
                self.wd.find_element(By.ID, 'listing_title_deed_1').click()
                print('project_name.click() 2')

                self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[1]/div[1]/div/fieldset[1]/div[3]/div/div[1]/span/span[1]').click()

                # self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(Keys.CONTROL, 'a')
                # print('2')

                # self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(Keys. BACKSPACE)
                print('self.addressEN[0][:-1]',self.address[0][:-1])
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input'))).send_keys(self.address[0][:-1])

                print(self.address[0])
                time.sleep(3)
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="select2-listing_listing_project_id-results"]/li[1]')))
                print(3)
                project_name = self.wd.find_element(By.XPATH, 'select2-results__option select2-results__option--highlighted')
                project_name.click()

            except:
                self.no_pjn.append("ไม่มีโปรเจค")
                print(self.no_pjn)
                return 'ไม่มีโปรเจค'
                
        select_post_type = Select(self.wd.find_element(By.ID, 'listing_post_type'))
        select_post_type.select_by_visible_text('คอนโดให้เช่า')
        # self.wd.find_element(By.ID, 'listing_remark').send_keys('test')
        # self.wd.find_element(By.ID, 'listing_title_deed_0').click()
        self.wd.find_element(By.ID, 'listing_title_deed_1').click()

    ###################################ข้อมูลพื้นฐาน###################################
    ################รายละเอียดห้อง#######################################
        select_room_type = Select(self.wd.find_element(By.ID, 'listing_room_type'))
        if self.property_type == ['สตูดิโอ']:
            select_room_type.select_by_visible_text('สตูดิโอ')
        elif self.bedroom == [1]:
            select_room_type.select_by_visible_text('1 ห้องนอน')
        elif self.bedroom == [2]:
            select_room_type.select_by_visible_text('2 ห้องนอน')
        elif self.bedroom == [3]:
            select_room_type.select_by_visible_text('3 ห้องนอน')
        elif self.bedroom == [4]:
            select_room_type.select_by_visible_text('4 ห้องนอน')
        elif self.bedroom == [5]:
            select_room_type.select_by_visible_text('5 ห้องนอน')
        elif self.property_type[0] == ObjectId('62c3b1f6dbf2ee974adf6a03'):
            select_room_type.select_by_value('1')
        else:
            select_room_type.select_by_visible_text('สตูดิโอ')
        print('select_room_type.click()')

        select_bedroom = Select(self.wd.find_element(By.ID, 'listing_no_of_bedroom'))
        if self.bedroom == [1]:
            select_bedroom.select_by_visible_text('1')
        elif self.bedroom == [2]:
            select_bedroom.select_by_visible_text('2')
        elif self.bedroom == [3]:
            select_bedroom.select_by_visible_text('3')
        elif self.bedroom == [4]:
            select_bedroom.select_by_visible_text('4')
        elif self.bedroom == [5]:
            select_bedroom.select_by_visible_text('5')
        elif self.bedroom == [6]:
            select_bedroom.select_by_visible_text('6')
        elif self.bedroom == [7]:
            select_bedroom.select_by_visible_text('7')
        elif self.bedroom == [8]:
            select_bedroom.select_by_visible_text('8')
        elif self.bedroom == [9]:
            select_bedroom.select_by_visible_text('9')
        elif self.bedroom == [10]:
            select_bedroom.select_by_visible_text('10')
        else:
            select_bedroom.select_by_visible_text('10')
        print('select_bedroom.click()')

        select_toilet = Select(self.wd.find_element(By.ID, 'listing_no_of_bathroom'))
        if [1] in self.toilet:
            select_toilet.select_by_visible_text('10')
        elif [2] in self.toilet:
            select_toilet.select_by_visible_text('2')
        elif [3] in self.toilet:
            select_toilet.select_by_visible_text('3')
        elif [4] in self.toilet:
            select_toilet.select_by_visible_text('4')
        elif [5] in self.toilet:
            select_toilet.select_by_visible_text('5')
        elif [6] in self.toilet:
            select_toilet.select_by_visible_text('6')
        elif [7] in self.toilet:
            select_toilet.select_by_visible_text('7')
        elif [8] in self.toilet:
            select_toilet.select_by_visible_text('8')
        elif [9] in self.toilet:
            select_toilet.select_by_visible_text('9')
        elif [10] in self.toilet:
            select_toilet.select_by_visible_text('10')
        else:
            select_toilet.select_by_visible_text('1')
        print('select_toilet.click()')

        self.wd.find_element(By.ID, 'listing_room_area').send_keys(self.max_area[0])
        self.wd.find_element(By.ID, 'listing_floor').send_keys('1')
        # self.wd.find_element(By.ID, 'listing_building').send_keys('test')
        self.wd.find_element(By.ID, 'listing_home_address').send_keys('10')
        # self.wd.find_element(By.ID, 'listing_room_no').send_keys('0')
        print('listing_home_address.click()')

    ################รายละเอียดห้อง#######################################
    ###################################รายละเอียดเพิ่มเติม###################################
        select_furnishing = Select(self.wd.find_element(By.ID, 'listing_furnishing'))
        select_furnishing.select_by_value('2')
        select_parking = Select(self.wd.find_element(By.XPATH, '//*[@id="listing_parking_spaces"]'))
        select_parking.select_by_value('2')
        print('select_parking.click()')

        # select_facing_direction = Select(self.wd.find_element(By.XPATH, '//*[@id="listing_facing_direction"]'))
        # select_facing_direction.select_by_value('11')

    ###################################รายละเอียดเพิ่มเติม###################################
    ###################################สิ่งอำนวยความสะดวก###################################
        # self.wd.find_element(By.XPATH,'//*[@id="facility_6"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_11"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_14"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_13"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_2"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_5"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_9"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_8"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_17"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_12"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_3"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_1"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_4"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_16"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_10"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_7"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="facility_15"]').click()

        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_4"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_10"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_1"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_5"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_3"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_7"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_8"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_12"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_11"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_2"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_6"]').click()
        # self.wd.find_element(By.XPATH,'//*[@id="central_facility_9"]').click()

    ###################################สิ่งอำนวยความสะดวก###################################
    ###################################ค่าใช้จ่าย###################################
        try:
            self.wd.find_element(By.XPATH,'//*[@id="listing_rental_price_type_1"]').click()
            self.wd.find_element(By.XPATH,'//*[@id="listing_rent_price"]').send_keys(self.f_price[0])
        except:
            self.wd.find_element(By.ID,'listing_rental_price_type_2').click()
        self.wd.find_element(By.ID,'listing_daily_rental_price_type_2').click()
        self.wd.find_element(By.ID,'listing_rental_deposit_type_4').click()
        self.wd.find_element(By.ID,'listing_advance_fee_type_4').click()
        self.wd.find_element(By.ID,'listing_common_service_fee_type_3').click()

        print('ค่าใช้จ่าย.click()')

    ###################################ค่าใช้จ่าย###################################
    ###################################รายละเอียด###################################
        # self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[1]/div[4]/div/fieldset/div[1]/div/div[1]/div/iframe').click()
        # self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[1]/div[4]/div/fieldset/div[1]/div/div[1]/div/iframe').send_keys('test')

        # self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[1]/div[4]/div/fieldset/div[1]/div/div[1]/div/input').send_keys('test')
        # self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[1]/div[4]/div/fieldset/div[1]/div/div[2]/div/iframe').click()

        # self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[1]/div[4]/div/fieldset/div[1]/div/div[2]/div/input').send_keys('test')
        self.wd.find_element(By.ID, 'listing_contact_person').send_keys('test')
        self.wd.find_element(By.ID, 'listing_line_user_id').send_keys('test')
        self.wd.find_element(By.ID, 'listing_phone').send_keys('0000000000')


        page1_url = self.wd.current_url
        print(page1_url)
        # self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[2]/input').click()
        # print('new_listing.click()')
        # print(self.wd.current_url)
        
        self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[2]/input').click()
        print('new_listing.click()')

        print(self.wd.current_url)


        if self.wd.current_url == page1_url:
            wt = 1
            while True:
                print(wt)
                self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[2]/input').click()
                print('new_listing.click()')

                print(self.wd.current_url)

                try:
                    a_topic = self.wd.find_element(By.XPATH,'//*[@id="listing_title-error"]').get_attribute('innerHTML')
                    print(a_topic)
                except:
                    print('no a_topic')
                try:
                    a_topic_en = self.wd.find_element(By.XPATH,'//*[@id="listing_title_en-error"]').get_attribute('innerHTML')
                    print(a_topic_en)
                except:
                    print('no a_topic_en')
                try:
                    a_pjn = self.wd.find_element(By.XPATH,'//*[@id="listing_listing_project_id-error"]').get_attribute('innerHTML')
                    print(a_pjn)
                    # return a_pjn
                except:
                    print('no a_pjn')
                try:
                    a_rt = self.wd.find_element(By.XPATH,'//*[@id="listing_room_type-error"]').get_attribute('innerHTML')
                    print(a_rt)
                except:
                    print('no a_rt')
                try:
                    a_br = self.wd.find_element(By.XPATH,'//*[@id="listing_no_of_bedroom-error"]').get_attribute('innerHTML')
                    print(a_br)
                except:
                    print('no a_br')
                try:
                    a_tl = self.wd.find_element(By.XPATH,'//*[@id="listing_no_of_bathroom-error"]').get_attribute('innerHTML')
                    print(a_tl)
                except:
                    print('no a_tl')
                try:
                    a_ra = self.wd.find_element(By.XPATH,'//*[@id="listing_room_area-error"]').get_attribute('innerHTML')
                    print(a_ra)
                except:
                    print('no a_ra')
                try:
                    a_lv = self.wd.find_element(By.XPATH,'//*[@id="listing_floor-error"]').get_attribute('innerHTML')
                    print(a_lv)
                except:
                    print('no a_lv')
                try:
                    a_ha = self.wd.find_element(By.XPATH,'//*[@id="listing_home_address-error"]').get_attribute('innerHTML')
                    print(a_ha)
                except:
                    print('no a_ha')
                try:
                    a_fu = self.wd.find_element(By.XPATH,'//*[@id="listing_furnishing-error"]').get_attribute('innerHTML')
                    print(a_fu)
                except:
                    print('no a_fu')
                try:
                    a_ps = self.wd.find_element(By.XPATH,'//*[@id="listing_parking_spaces-error"]').get_attribute('innerHTML')
                    print(a_ps)
                except:
                    print('no a_ps')
                try:
                    a_pm = self.wd.find_element(By.XPATH,'//*[@id="rent-price"]/div[2]/div/p').get_attribute('innerHTML')
                    print(a_pm)
                except:
                    print('no a_pm')
                try:
                    a_pd = self.wd.find_element(By.XPATH,'//*[@id="daily-rental"]/div[2]/div/p').get_attribute('innerHTML')
                    print(a_pd)
                except:
                    print('no a_pd')
                try:
                    a_df = self.wd.find_element(By.XPATH, '//*[@id="deposit-fee-section"]/div[2]/div/p').get_attribute('innerHTML')
                    print(a_df)
                except:
                    print('no a_df')
                try:
                    a_af = self.wd.find_element(By.XPATH,'//*[@id="advance-fee-section"]/div[2]/div/p').get_attribute('innerHTML')
                    print(a_af)
                except:
                    print('no a_af')
                try:
                    a_rw = self.wd.find_element(By.XPATH,'//*[@id="common-service-fee"]/div[2]/div/p').get_attribute('innerHTML')
                    print(a_rw)
                except:
                    print('no a_rw')
                try:
                    a_cp = self.wd.find_element(By.XPATH,'//*[@id="listing_contact_person-error"]').get_attribute('innerHTML')
                    print(a_cp)
                except:
                    print('no a_cp')
                try:
                    a_cn = self.wd.find_element(By.XPATH,'//*[@id="listing_phone-error"]').get_attribute('innerHTML')
                    print(a_cn)
                except:
                    print('no a_cn')
        
                wt += 1

                if self.wd.current_url != page1_url or wt == 3:
                    print(self.wd.current_url)
                    break
            self.no_pjn.append("ไม่มีโปรเจค")
            print(self.no_pjn)
            return 'ไม่มีโปรเจค'

        else:
            print('self.wd.current_url != page1_url')

        # print(self.wd.current_url)
        # if self.wd.current_url == 'https://www.residences.in.th/listings/new':
        #     self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[2]/input').click()
        # print(self.wd.current_url)
        # if self.wd.current_url == 'https://www.residences.in.th/listings/new':
        #     self.wd.find_element(By.XPATH, '//*[@id="new_listing"]/div[2]/div/div/div[2]/input').click()
    ###################################รายละเอียด###################################
    ###################################PAGE1###################################
    ###################################PAGE2###################################
        # self.wait.until(EC.element_to_be_clickable((By.ID,'accepted_term_and_condition')))
        print('before insert img')
        try:
            try:
                for n_img in range(int(len(galleryUrls))):
                    print(ImgsPath[n_img])
                    self.wd.find_element(By.ID,'listing_photo_attachment').send_keys(ImgsPath[n_img])
            except:
                print('except[n_img]')
                for n_img in range(int(len(galleryUrls))):
                    print(ImgsPath[n_img])
                    self.wd.find_element(By.ID,'listing_photo_attachment').send_keys(ImgsPath[n_img])
        except:
            print('no img insert')

        print('after insert img')

        time.sleep(5)
        try:
            self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME,'handle')))
        except:
            print('no pic')
        self.wd.find_element(By.XPATH, '//*[@id="accepted_term_and_condition"]').click()
        self.wd.find_element(By.XPATH, '/html/body/div/div[2]/div/div/div[3]/form/div[9]/button').click()
    ###################################PAGE2###################################

    def quit(self):
        self.wd.quit()

class startresidences:
    def startbotresidences(self,id):
        try:
            fn = residences()
            fn.click_login()
            fn.query_uw()
            fn.fill_login()
            fn.query(id)
            # fn.click_createpost_A()
            # fn.fill_data_A()
            fn.click_createpost_C()
            fr = fn.fill_data_C()
            print('fr', fr)
            if fr == 'ไม่มีโปรเจค':
                print("ไม่มีโปรเจค")
                fn.quit()
                return "ไม่มีโปรเจค"
            print('tnp')
            fn.quit()
            return True

        except Exception as e:
            print(e)
            fn.quit()
            return False
