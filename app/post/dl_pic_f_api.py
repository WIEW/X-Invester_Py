from pickle import GLOBAL
import requests
from bs4 import BeautifulSoup
import itertools
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait,Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import io
from PIL import Image
import time
import os
from pprint import pprint
from pymongo import MongoClient
import pymongo
from dotenv import load_dotenv
from collections import ChainMap
from bson.objectid import ObjectId


class download_imgs():
    def __init__(self):
        self.mode = 0o666
        self.ch_xpath = "/root/X-Invester_Py/app/public/api_imgs/X-invester"
        self.sys_xpath = "imgs_{}"
        self.sys_rz_xpath = "imgs_rz_{}"
        self.sys_ad_xpath = "imgs_ad_{}"
        self.sys_and_xpath = "imgs_and_{}"


        self.i = 1
        self.img_api_urls = []
        self.img_ad_urls = []
        self.img_and_urls = []
        self.imgs_rz_path = []

        # self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")

        self.mydb = self.myclient["devXInvester"]
        self.mycol = self.mydb["posts"]
        self.mycol_properties = self.mydb["properties"]

    def query(self,id):
        myquery = {"_id":ObjectId(id)}
        
        mydoc = self.mycol.find(myquery)
        for x in mydoc:
            global galleryUrls
            global imgs_rz

            galleryUrls = x.get('galleryUrls')
            uploadAddressDocumentUrl = x.get('uploadAddressDocumentUrl')
            uploadAddressNumberDocumentUrl = x.get('uploadAddressNumberDocumentUrl')
            imgs_rz = x.get('imgs_path')

            self.img_api_urls.append(galleryUrls)
            self.img_ad_urls.append(uploadAddressDocumentUrl)
            self.img_and_urls.append(uploadAddressNumberDocumentUrl)
            self.imgs_rz_path.append(imgs_rz)

    def update_img_path_mongo(self,imgs_path, id):
        myquery = {"_id":ObjectId(id)}

        newvalues = { "$set": imgs_path }

        x = self.mycol.update_one(myquery, newvalues)

    def update_img_rz_path_mongo(self,imgs_path, id):
        myquery = {"_id":ObjectId(id)}

        newvalues = { "$set": imgs_path }

        x = self.mycol.update_one(myquery, newvalues)

    def update_img_ad_path_mongo(self,img_ad_path, id):
        myquery = {"_id":ObjectId(id)}

        newvalues = { "$set": img_ad_path }

        x = self.mycol.update_one(myquery, newvalues)

    def update_img_and_path_mongo(self,img_and_path, id):
        myquery = {"_id":ObjectId(id)}

        newvalues = { "$set": img_and_path }

        x = self.mycol.update_one(myquery, newvalues)


    def download_imgs(self, id):
        print('2222222222222222222')

        save_path = self.sys_xpath.format(ObjectId(id))
        save_ad_path = self.sys_ad_xpath.format(ObjectId(id))
        save_and_path = self.sys_and_xpath.format(ObjectId(id))

###############################img###############################################
        os.chdir(os.path.join(os.getcwd(), self.ch_xpath))#/api_imgs/X-invester

        try:
            os.makedirs(save_path, self.mode)
            os.chdir(os.path.join(os.getcwd(), save_path))
        except:
            os.chdir(os.path.join(os.getcwd(), save_path))
        
        img_api_url_path = []
        
        img_api_url_i = 0
        try:
            for self.img_api_url in galleryUrls:
                
                image_content = requests.get(self.img_api_url).content
                image_file = io.BytesIO(image_content)
                image = Image.open(image_file)
                file_name = str(img_api_url_i) + ".jpg"

                file_path = save_path +'\\' + file_name
                full_path = self.ch_xpath+'/'+save_path+'/'+file_path
                img_api_url_path.append(full_path)
                

                with open(file_path, "wb") as f:
                    print(f)
                    image.save(f, "PNG")

                print("Success img_api_url")

                imgs_path = {
                    'imgs_path' : img_api_url_path
                }

                img_api_url_i += 1

                download_imgs.update_img_path_mongo(self, imgs_path, id)
            print(img_api_url_path)
        except:
            print('img_api_url_path', img_api_url_path)

###############################img###############################################

###############################save_ad_path###############################################

        os.chdir(os.path.join(os.getcwd(), self.ch_xpath))#/api_imgs/X-invester

        try:
            os.makedirs(save_ad_path, self.mode)
            os.chdir(os.path.join(os.getcwd(), save_ad_path))
        except:
            os.chdir(os.path.join(os.getcwd(), save_ad_path))

        img_ad_url_i = 0
        try:
            for self.img_ad_url in self.img_ad_urls:
                print(self.img_ad_url)
                image_content = requests.get(self.img_ad_url).content
                image_file = io.BytesIO(image_content)
                image = Image.open(image_file)
                file_name = str(img_ad_url_i) + ".jpg"

                file_path = save_ad_path +'\\' + file_name
                full_path = self.ch_xpath+'/'+save_ad_path+'/'+file_path
                

                with open(file_path, "wb") as f:
                    print(f)
                    image.save(f, "PNG")

                print("Success img_ad_url")

                img_ad_path = {
                    'img_ad_path' : full_path
                }

                img_ad_url_i += 1

                download_imgs.update_img_ad_path_mongo(self, img_ad_path, id)
        except:
            print("no full_path")#,full_path)

###############################save_ad_path###############################################
###############################save_and_path###############################################

        os.chdir(os.path.join(os.getcwd(), self.ch_xpath))#/api_imgs/X-invester

        try:
            os.makedirs(save_and_path, self.mode)
            os.chdir(os.path.join(os.getcwd(), save_and_path))
        except:
            os.chdir(os.path.join(os.getcwd(), save_and_path))
        img_and_url_i = 0
        try:
            for self.img_and_url in self.img_and_urls:
                
                print(self.img_and_url)

                image_content = requests.get(self.img_and_url).content
                image_file = io.BytesIO(image_content)
                image = Image.open(image_file)
                file_name = str(img_and_url_i) + ".jpg"

                file_path = save_and_path +'\\' + file_name
                full_path = self.ch_xpath+'/'+save_and_path+'/'+file_path
                

                with open(file_path, "wb") as f:
                    print(f)
                    image.save(f, "PNG")

                print("Success img_and_url")

                img_and_path = {
                    'img_and_path' : full_path
                }
                img_and_url_i += 1
                download_imgs.update_img_and_path_mongo(self, img_and_path, id)
        except:
            print("no full_path")#,full_path)

###############################save_and_path###############################################
###############################save_rz_path###############################################
    def download_rz_imgs(self, id):
        save_rz_path = self.sys_rz_xpath.format(ObjectId(id))

        os.chdir(os.path.join(os.getcwd(), self.ch_xpath))#/api_imgs/X-invester

        try:
            os.makedirs(save_rz_path, self.mode)
            os.chdir(os.path.join(os.getcwd(), save_rz_path))
        except:
            os.chdir(os.path.join(os.getcwd(), save_rz_path))
        
        l_img_rz_path = []
        
        img_rz_i = 0
        try:
            for img_rz in imgs_rz:
                
                # image_content = requests.get(img_rz).content
                # image_file = io.BytesIO(image_content)
                image = Image.open(img_rz)
                imageresize = image.resize((545,350),Image.ANTIALIAS)

                file_name = str(img_rz_i) + ".jpg"

                file_path = save_rz_path +'\\' + file_name
                full_path = self.ch_xpath+'/'+save_rz_path+'/'+file_path
                l_img_rz_path.append(full_path)
                

                with open(file_path, "wb") as f:
                    print(f)
                    imageresize.save(f, "PNG")

                print("Success img_api_url")

                imgs_rz_path = {
                    'imgs_rz_path' : l_img_rz_path
                }

                img_rz_i += 1

                download_imgs.update_img_rz_path_mongo(self, imgs_rz_path, id)
            print(imgs_rz_path)
        except:
            # print('imgs_rz_path',imgs_rz_path)
            print("no imgs_rz_path")#,full_path)

###############################save_rz_path###############################################

class update_path:
    def start_update_path(self,id):

        fn = download_imgs()

        fn.query(id)
        fn.download_imgs(id)
        fn.query(id)
        fn.download_rz_imgs(id)
