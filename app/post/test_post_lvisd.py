from pickle import GLOBAL
import requests
from bs4 import BeautifulSoup
import csv
import itertools
from email.mime import image
import imp
from unittest import skip
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait,Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
import io
from PIL import Image
import time
import os
from pprint import pprint
from pymongo import MongoClient
import pymongo
from dotenv import load_dotenv
from collections import ChainMap
from bson.objectid import ObjectId
from datetime import date
import random

################################################################

class livinginsider:
    def __init__(self):
        self.today = date.today()

        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        # options.add_argument("--remote-debugging-port=9230")  # this

        options.add_argument("--disable-extensions") 
        options.add_argument("--disable-gpu") 
        # options.add_argument("start-maximized") 
        options.add_argument("--window-size=1920,1080") 
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
        options.add_argument(f'user-agent={user_agent}')

        self.wd = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options = options)

        ##Start of db zone###
        # self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:7831VO4koqgS69J5@realtyplenty-uat-db-a9419669.mongo.ondigitalocean.com/admin?authMechanism=DEFAULT&authSource=admin")
        self.myclient = pymongo.MongoClient("mongodb+srv://doadmin:a274WB3E56j1Zo8I@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com/devXInvester?replicaSet=realty-plenty-db&readPreference=primary&srvServiceName=mongodb&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1&tls=true&tlsAllowInvalidHostnames=true&tlsAllowInvalidCertificates=true")
        
        self.mydb = self.myclient["devXInvester"]
        self.mycol = self.mydb["posts"]
        self.mycol_properties = self.mydb["properties"]
        self.mycol_uw = self.mydb["websites"]

        self.i=0
        self.cpost = 1
        self.n_pic = 1
        self.post_topic = []
        self.list_type = []
        self.bedroom = []
        self.toilet = []
        self.s_price = []
        self.f_price = []
        self.min_area = []
        self.max_area = []
        self.description = []
        self.property_type = []
        self.watchlistPropertyTypes =[]
        self.property = []
        self.imgs = []
        self.state = []
        self.city = []
        self.subcity = []
        self.address = []
        self.addressEN = []
        self.imgs_path = []
        self.imgs_rz_path = []
        self.JS_ADD_TEXT_TO_INPUT = """
            var elm = arguments[0], txt = arguments[1];
            elm.value += txt;
            elm.dispatchEvent(new Event('change'));
            """

        ####url################################

        self.url = "https://www.livinginsider.com/"

        self.wd.get("https://www.livinginsider.com/")        

        self.wait = WebDriverWait(self.wd, 10)

    ####url################################
##########################query################################################################
    def query(self,id):
        print('id=',id)
        myquery = {"_id":ObjectId(id)}

        mydoc = self.mycol.find(myquery)
        global galleryUrls
        global ImgsPath
        global Img_rz_path

        stage_lookup_property = {
        "$lookup": {
        "from": "properties", 
        "localField": "property", 
        "foreignField": "_id", 
        "as": "data_property",
        }
        }

        stage_lookup_subCity = {
        "$lookup": {
        "from": "subCities", 
        "localField": "subCity", 
        "foreignField": "_id", 
        "as": "data_subCity",
        }
        }
        stage_lookup_city = {
        "$lookup": {
        "from": "cities", 
        "localField": "city", 
        "foreignField": "_id", 
        "as": "data_city",
        }
        }
        stage_lookup_state = {
        "$lookup": {
        "from": "states", 
        "localField": "state", 
        "foreignField": "_id", 
        "as": "data_state",
        }
        }
        # Limit to the first 5 documents:
        stage_limit_5 = { "$limit": 1 }

        pipeline_property = [
        stage_lookup_property,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]

        pipeline_subCity = [
        stage_lookup_subCity,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]
        pipeline_city = [
        stage_lookup_city,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]
        pipeline_state = [
        stage_lookup_state,
        #    stage_add_comment_count,
        #    stage_match_with_comments,
        stage_limit_5,
        ]

        # self.properties = self.mycol.aggregate(pipeline_property)
        # self.subCities = self.mycol_properties.aggregate(pipeline_subCity)
        # self.Cities = self.mycol_properties.aggregate(pipeline_city)
        # self.States = self.mycol_properties.aggregate(pipeline_state)

        # for property in self.properties:
        #     # pprint(type(property))
        #     data_property = property.get('data_property')
        #     data_property_dict = (dict(ChainMap(*data_property)))
        #     nameTH = data_property_dict.get('nameTH')
        #     self.address.append(nameTH)
        #     # city = data_property_dict.get('city')
        #     for subCity in self.subCities:
        #         # print(type(subCity))
        #         data_subCity = subCity.get('data_subCity')
        #         data_subCity_dict = (dict(ChainMap(*data_subCity)))
        #         subCity = data_subCity_dict.get('name')
        #         print(subCity)
        #         self.subcity.append(subCity)
        #         # print((subCity))
        #     for City in self.Cities:
        #         data_city = City.get('data_city')
        #         data_city_dict = (dict(ChainMap(*data_city)))
        #         City = data_city_dict.get('name')
        #         print(City)
        #         self.city.append(City)
        #     for State in self.States:
        #         data_State = State.get('data_state')
        #         data_State_dict = (dict(ChainMap(*data_State)))
        #         State = data_State_dict.get('name')
        #         print(State)
        #         self.state.append(State)


        for x in mydoc:
            try:
                lat_long = []
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                details_array = x.get('cut_find_details')
                details_dict = dict(ChainMap(*details_array))
                totalBedroom = details_dict.get('จำนวนห้องนอน')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                maximumArea = details_dict.get('ขนาดพื้นที่ห้อง')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                Description = x.get('description')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')
                Img_rz_path = x.get('imgs_rz_path')

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)
                self.imgs_rz_path.append(Img_rz_path)                


            except:
                topic = x.get('name')
                galleryUrls = x.get('galleryUrls')
                listType = x.get('listType')
                PropertyType = x.get('propertyType')
                watchListPropertyTypes = x.get('watchListPropertyTypes')
                Property = x.get('property')
                totalBedroom = x.get('totalBedroom')
                totalToilet = x.get('totalToilet')
                startingPrice = x.get('startingPrice')
                finalPrice = x.get('finalPrice')
                minimumArea = x.get('minimumArea')
                maximumArea = x.get('maximumArea')
                Description = x.get('description')
                # TH_Name = x.get('TH_Name')
                # EN_Name = x.get('EN_Name')
                # Address = x.get('Address')
                # Latitude = x.get('Latitude')
                # Longitude = x.get('Longitude')
                # State = x.get('State')
                # City = x.get('City')
                # SubCity = x.get('SubCity')
                # cut_find_address = x.get('cut_find_address')
                ImgsPath = x.get('imgs_path')
                Img_rz_path = x.get('imgs_rz_path')

                self.post_topic.append(topic)
                self.imgs.append(galleryUrls)
                self.list_type.append(listType)
                self.property_type.append(PropertyType)
                self.property.append(Property)
                self.bedroom.append(totalBedroom)
                self.toilet.append(totalToilet)
                self.s_price.append(startingPrice)
                self.f_price.append(finalPrice)
                self.min_area.append(minimumArea)
                self.max_area.append(maximumArea)
                self.description.append(Description)
                self.watchlistPropertyTypes.append(watchListPropertyTypes)
                # self.state.append(State)
                # self.city.append(City)
                # self.subcity.append(SubCity)
                # lat_long.append(Latitude)
                # lat_long.append(Longitude)
                # self.address.append(cut_find_address)
                self.imgs_path.append(ImgsPath)
                self.imgs_rz_path.append(Img_rz_path)                

        mycol_properties_query = {"_id":ObjectId(self.property[0])}
        mydoc_mycol_properties = self.mycol_properties.find(mycol_properties_query)

        for x in mydoc_mycol_properties:
            nameTH = x.get('nameTH')
            nameEN = x.get('nameEN')
            City = x.get('address')
            Province = x.get('state')
            print('City',City)
            print('Province',Province)
            print(nameEN,nameTH,City)

            self.address.append(nameTH)
            self.addressEN.append(nameEN)
            self.city.append(City)
            self.state.append(Province)


        def pnt():
            print('pnt',self.address)

        pnt()


            # lat_long.append(Latitude)
            # lat_long.append(Longitude)
        ##########################query################################################################
    ##End of db zone###
    # ad_title = ['test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718test(English)12345657891011121311415161718']
    ################################################################
    def query_uw(self):
        myquery = {'name':'livinginsider'}
        myuws = self.mycol_uw.find(myquery)
        for uws in myuws:
            users = uws['users']

        global user
        global passwd

        user = users[0]['username']
        passwd = users[0]['password']

        r = random.choice(users)

        u = r['username']
        p = r['password']

    ##########################click_login################################################################
    def click_login(self):
        print('click_login()')
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="none_login_zone"]/a[1]')))
        text_login = self.wd.find_element(By.XPATH, '//*[@id="none_login_zone"]/a[1]').get_attribute("innerHTML")
        print('text_login', text_login)
        self.wd.find_element(By.XPATH, '//*[@id="none_login_zone"]/a[1]').click()
        time.sleep(2)

    # click_login()

    ##########################click_login##########################################
    ###############################fill_login##########################################
    def fill_login(self):
        self.wd.implicitly_wait(0.5)
        # self.wait.until(EC.element_to_be_clickable((By.ID, "login_username"))).send_keys("mailusedfortest@gmail.com")
        # self.wait.until(EC.element_to_be_clickable((By.ID, "login_username"))).send_keys("Prapapan123")
        self.wait.until(EC.element_to_be_clickable((By.ID, "login_username"))).send_keys(user)

        self.wd.find_element(By.CLASS_NAME, 'btn-next-step').click()
        time.sleep(2)
        # self.wd.find_element(By.XPATH, '//*[@id="password"]').send_keys("qwerty123456Zx")
        # self.wd.find_element(By.XPATH, '//*[@id="password"]').send_keys("Prapapan1")
        self.wd.find_element(By.XPATH, '//*[@id="password"]').send_keys(passwd)

        self.wd.find_element(By.CLASS_NAME, 'btn-next-step').click()
        print("Click Login_Button finish")
        time.sleep(4)

    ###############################fill_login##########################################
    #######################click_createpost#######################################
    def click_createpost(self):
        time.sleep(0.5)
        self.wd.get(self.url)
        time.sleep(3)

        self.wd.find_element(By.XPATH, '//*[@id="navbar"]/ul/li[8]/a').click()
        
    ########################click_createpost######################################

    ################################fill_data####################################
    def fill_data(self):
        ###################################PAGE1###################################
        time.sleep(2)
    ################ข้อมูลทั่วไป#######################################
    #########################สถานะผู้ประกาศ#######################################################
        # check if
        # self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[1]/div/div[1]/label').click()  # Owner
        self.wd.find_element(By.XPATH, '//*[@id="web_post_from2"]').click()  # Agent
    #########################สถานะผู้ประกาศ#######################################################
    #########################Stock ID#######################################################
        # self.wd.find_element(By.ID, 'web_sku').send_keys('000')
        # self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[4]/div/div[1]/label').click()  # sell

    #########################Stock ID#######################################################
    #########################Title#######################################################
        headtitle = self.wd.find_element(By.ID, 'web_title')#.send_keys(post_topic)
        self.wd.execute_script(self.JS_ADD_TEXT_TO_INPUT, headtitle, self.post_topic)

    #########################Title#######################################################
    #########################Description #######################################################
        ad_title = self.wd.find_element(By.ID, 'web_description')#.send_keys(description)
        self.wd.execute_script(self.JS_ADD_TEXT_TO_INPUT, ad_title, self.description)

    #########################Description #######################################################
    #########################ประเภทประกาศ#######################################################
        # check if
        self.wd.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        time.sleep(2)

        print(self.list_type)

        if 'sell' in self.list_type or 'ขาย' in self.list_type:
            self.wd.find_element(By.XPATH, '//*[@id="web_post_type1"]').click()  # sell
            if self.property_type == [ObjectId('62bd7104f0521b8890fe9a0f')] or self.property_type == [ObjectId('62c3b1f6dbf2ee974adf6a03')]:
                self.condo_sell_page2()
            elif self.property_type == [ObjectId('62bd7104f0521b8890fe9a11')]:
                self.house_sell_page2()
            elif self.property_type == [ObjectId('62bd7104f0521b8890fe9a15')]:
                self.townhouse_sell_page2()
        elif self.list_type == ['down'] or self.list_type == ['ดาวน์']:
            self.wd.find_element(By.XPATH, '//*[@id="web_post_type5"]').click()  # down
        elif 'rent' in self.list_type or 'เช่า' in self.list_type:

            self.wd.find_element(By.XPATH, '//*[@id="web_post_type4"]').click()  # rent
            print('click rent')
            print(self.property_type)
            if self.property_type == [ObjectId('62bd7104f0521b8890fe9a0f')] or self.property_type == [ObjectId('62c3b1f6dbf2ee974adf6a03')]:
                # self.townhouse_rent_page2()

                self.condo_rent_page2()
                # print('condo_rent_page2')
            elif self.property_type == [ObjectId('62bd7104f0521b8890fe9a11')]:
                self.house_rent_page2()
                print('house_rent_page2')
            elif self.property_type == [ObjectId('62bd7104f0521b8890fe9a15')]:
                self.townhouse_rent_page2()
            else:
                print('error')
                self.condo_rent_page2()
        elif self.list_type == ['long lease'] or self.list_type == ['เซ้ง']:
            self.wd.find_element(By.XPATH, '//*[@id="web_post_type6"]').click()  # long lease

    #########################ประเภทประกาศ#######################################################
    #########################ประเภทอสังหาริมทรัพย์#######################################################
        # check if

        # buildingList_el = self.wd.find_element_by_id('buildingList')
        # for buildingList_option in buildingList_el.find_elements_by_tag_name('option'):
        #     if buildingList_option.text == property_type:
        #         buildingList_option.click()  # select() in earlier versions of webdriver
        #         break
        #     else:
        #         buildingList_option.click('คอนโด') # select() in

        # house
        # land
        # shophouse
        # office
        # townhouse
        # homeoffice
        # retail
        # showroom
        # bfs
        # factory
        # wh
    #########################ประเภทอสังหาริมทรัพย์#######################################################
    ###################################PAGE1###################################
    ###################################PAGE3###################################
        # fn_desc = '/html/body/div[2]/section[2]/div/div[3]/div/div/div/div/div[2]/div[1]/div[13]/div/div'
        # self.wait.until(EC.element_to_be_clickable((By.XPATH, fn_desc)))
        # #draft
        print('save_draft')
        save_draft = '//*[@id="save_draft"]'
        self.wait.until(EC.element_to_be_clickable((By.XPATH, save_draft)))
        print('save_draft click')
        try:
            self.wd.find_element(By.XPATH, save_draft).click()
            print('xp')
        except Exception:
            self.wd.find_element(By.ID, 'save_draft').click()
            print('id')

        print('save_draft clicked')
        # #draft

        # #post
        # self.wd.find_element(By.ID, 'save_publish').click()
        # self.wd.find_element(By.ID, 'btn_post').click()
        # #post

    ###################################PAGE3###################################
    def condo_sell_page2(self):#ผ่าน
        print('condo_sell_page2')
        time.sleep(2)
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ses_web_building_type1"]')))
        self.wd.find_element(By.XPATH, '//*[@id="ses_web_building_type1"]').click()

    #########################condoProject name#######################################################
        time.sleep(2)
        web_sku = self.wd.find_element(By.XPATH, '//*[@id="web_sku"]')
        web_sku.send_keys(' ')

        # fp = '//*[@id="div_web_project_id"]/span'
        # self.wd.find_element(By.XPATH, fp).click()

        pjn = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-container"]')#.click()
        self.wd.execute_script("arguments[0].click();", pjn)

        time.sleep(1)
        try:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
        except:
            # self.wd.find_element(By.XPATH, fp).click()
            # self.wd.execute_script("arguments[0].click();", pjn)
            self.wd.find_element(By.XPATH, '//*[@id="div_web_project_id"]/span').click()
            time.sleep(1)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))

        try:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.address[0])
            time.sleep(2)
            pjm_result = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').get_attribute('innerHTML')
            print(pjm_result)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # if self.address[0] in pjm_result:
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # else:
            #     self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            #     time.sleep(2)
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()
        except:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()

    #########################condoProject name#######################################################
    #########################ทำเล#######################################################
        try:
            # check if
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[6]/span/span[1]/span').click()
            time.sleep(2)
            try:
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
            except:
                self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[6]/span/span[1]/span').click()
                time.sleep(2)
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))

            if 'อยุธยา' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('อยุธยา')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

            elif 'ตาก' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('ตาก')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()
            
            elif 'เพชรบุรี' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('เพชรบุรี')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            elif 'นราธิวาส' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('นราธิวาส')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.city[0])
            # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="select2-web_zone_id-results"]/li')))
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()
            time.sleep(1)

    #########################ทำเล#######################################################
    #########################map#######################################################
            posmap = self.wd.find_element(By.ID, 'posmap')#.click()

            self.wd.execute_script('arguments[0].click();', posmap)
        except:
            print('posmap')
    #########################map#######################################################
        self.wd.find_element(By.ID, 'posmap').submit()

        time.sleep(2)
    ###################################PAGE1###################################
    ###################################PAGE2###################################
    ###########################################Bedrooms#########################################################
        self.wait.until(EC.element_to_be_clickable((By.ID, "select2-web_room-container")))
        self.wd.find_element(By.ID, 'select2-web_room-container').click()
        bedroom_el = self.wd.find_element(By.ID, 'web_room')
        for bedroom_option in bedroom_el.find_elements_by_tag_name('option'):
            if self.property_type == ['สตูดิโอ']:
                if bedroom_option.text == 'ห้องสตูดิโอ':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['1'] or self.bedroom == ['1 ห้อง']:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['2'] or self.bedroom == ['2 ห้อง']:
                if bedroom_option.text == '2 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['3'] or self.bedroom == ['3 ห้อง']:
                if bedroom_option.text == '3 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['4'] or self.bedroom == ['4 ห้อง']:
                if bedroom_option.text == '4 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['5'] or self.bedroom == ['5 ห้อง']:
                if bedroom_option.text == '5 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['6'] or self.bedroom == ['6 ห้อง']:
                if bedroom_option.text == '6 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['7'] or self.bedroom == ['7 ห้อง']:
                if bedroom_option.text == '7 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['8'] or self.bedroom == ['8 ห้อง']:
                if bedroom_option.text == '8 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['9'] or self.bedroom == ['9 ห้อง']:
                if bedroom_option.text == '9 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['10'] or self.bedroom == ['10 ห้อง']:
                if bedroom_option.text == '10 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['มากกว่า 10'] or self.bedroom == ['มากกว่า 10']:
                if bedroom_option.text == 'มากกว่า 10':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bedroom_option.text == 'ห้องสตูดิโอ':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
    ##########################################Bedrooms#########################################################
    ##########################################Bathrooms#########################################################
        self.wd.find_element(By.ID, 'select2-web_bathroom-container').click()
        bathroom_el = self.wd.find_element(By.ID, 'web_bathroom')
        for bathroom_option in bathroom_el.find_elements_by_tag_name('option'):
            if self.toilet == ['1'] or self.toilet == ['1 ห้องน้ำ']:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['2'] or self.toilet == ['2 ห้องน้ำ']:
                if bathroom_option.text == '2 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['3'] or self.toilet == ['3 ห้องน้ำ']:
                if bathroom_option.text == '3 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['4'] or self.toilet == ['4 ห้องน้ำ']:
                if bathroom_option.text == '4 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['5'] or self.toilet == ['5 ห้องน้ำ']:
                if bathroom_option.text == '5 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['มากกว่า 5']:
                if bathroom_option.text == 'มากกว่า 5':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break

    ##########################################Bathrooms#########################################################
    ##########################################Level#########################################################
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()
        level_el = self.wd.find_element(By.ID, 'web_floor')
        for level_option in level_el.find_elements_by_tag_name('option'):
            # if self.level == ['']:
            #     if level_option.text == '1-4':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '5-10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '11-20':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '21-50':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '51-99':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
                if level_option.text == '1-4':
                    level_option.click()  # select() in earlier versions of webdriver
                    break
    ##########################################Level#########################################################
    ##########################################area#########################################################
        self.wd.find_element(By.ID, 'web_useful_space').send_keys(self.max_area[0])

    ##########################################area#########################################################
    ##########################################Selling price#########################################################
        self.wd.find_element(By.ID, 'web_price').send_keys(self.f_price[0])

    ##########################################Selling price#########################################################
    ##########################################Special Additional#########################################################

        # self.wd.find_element(By.XPATH, '//*[@id="web_keeping_pet"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_duplex"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_penthouse"]').click()

    ##########################################Special Additional#########################################################
    ##########################################Matterport URL#########################################################
        # self.wd.find_element(By.ID, 'web_matterport').send_keys('1')

    ##########################################Matterport URL#########################################################
    ##########################################Youtube URL#########################################################
        # self.wd.find_element(By.ID, 'web_youtube').send_keys('1')

    ##########################################Youtube URL#########################################################
    ##########################################picture#########################################################
        try:
            for n_img in range(int(len(galleryUrls))):
                self.wd.find_element(By.ID, 'fileupload').send_keys(ImgsPath[n_img])
        except:
            print('no image insert')
    ##########################################picture#########################################################
        # nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[9]/div/div/div/div/div/button[3]')
        nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[10]/div/div/div/div/div/button[3]')
        
        self.wd.execute_script("arguments[0].click();", nxtbtn)
        print('click post')
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div')))
        self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div').click()
        self.wd.find_element(By.XPATH,'//*[@id="post_data"]/div[3]/button').click()

    ###################################PAGE2###################################
###############################condo_sell_page2#########################################

    def condo_rent_page2(self):#ผ่าน
        print('condo_rent_page2')
        time.sleep(3)
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ses_web_building_type1"]')))
        self.wd.find_element(By.XPATH, '//*[@id="ses_web_building_type1"]').click()
        # buildingList_el = self.wd.find_element_by_id('buildingList')
        # for buildingList_option in buildingList_el.find_elements_by_tag_name('option'):
        #     if buildingList_option.text == 'คอนโด':
        #         time.sleep(2)

        #         buildingList_option.click()  # select() in earlier versions of webdriver
        #         break  
    #########################   condoProject name#######################################################
        # self.wd.execute_script("document.getElementById('div_web_project_id').style.display='block';")
        
        web_sku = self.wd.find_element(By.XPATH, '//*[@id="web_sku"]')
        web_sku.send_keys(' ')

        # fp = '//*[@id="div_web_project_id"]/span'
        # self.wd.find_element(By.XPATH, fp).click()
        pjn = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-container"]')#.click()
        self.wd.execute_script("arguments[0].click();", pjn)
        time.sleep(1)
        try:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
        except:
            # self.wd.find_element(By.XPATH, fp).click()
            # self.wd.execute_script("arguments[0].click();", pjn)
            self.wd.find_element(By.XPATH, '//*[@id="div_web_project_id"]/span').click()
            time.sleep(1)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))

        try:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.address[0])
            time.sleep(2)
            pjm_result = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').get_attribute('innerHTML')
            print(pjm_result)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # if self.address[0] in pjm_result:
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # else:
            #     self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            #     time.sleep(2)
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()
        except:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()

    #########################   condoProject name#######################################################
    #########################ทำเล#######################################################
        try:
            # check if
            time.sleep(1)
            self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[6]/span/span[1]/span').click()
            time.sleep(2)
            try:
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
            except:
                self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[6]/span/span[1]/span').click()
                time.sleep(2)
                self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))

            if 'อยุธยา' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('อยุธยา')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

            elif 'ตาก' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('ตาก')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()
            
            elif 'เพชรบุรี' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('เพชรบุรี')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            elif 'นราธิวาส' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('นราธิวาส')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.city[0])
            # self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="select2-web_zone_id-results"]/li')))
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()
            time.sleep(1)

            # area =
            # area.click()

        #########################ทำเล#######################################################
        #########################map#######################################################
            posmap = self.wd.find_element(By.ID, 'posmap')#.click()

            self.wd.execute_script('arguments[0].click();', posmap)
        except:
            print('posmap')

    #########################map#######################################################
        self.wd.find_element(By.ID, 'posmap').submit()
        time.sleep(2)
    ###################################PAGE1###################################
    ###################################PAGE2###################################
    ###########################################Bedrooms#########################################################
        self.wait.until(EC.element_to_be_clickable((By.ID, "select2-web_room-container")))
        self.wd.find_element(By.ID, 'select2-web_room-container').click()
        bedroom_el = self.wd.find_element(By.ID, 'web_room')
        for bedroom_option in bedroom_el.find_elements_by_tag_name('option'):
            if self.property_type == ['สตูดิโอ']:
                if bedroom_option.text == 'ห้องสตูดิโอ':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['1'] or self.bedroom == ['1 ห้อง']:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['2'] or self.bedroom == ['2 ห้อง']:
                if bedroom_option.text == '2 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['3'] or self.bedroom == ['3 ห้อง']:
                if bedroom_option.text == '3 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['4'] or self.bedroom == ['4 ห้อง']:
                if bedroom_option.text == '4 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['5'] or self.bedroom == ['5 ห้อง']:
                if bedroom_option.text == '5 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['6'] or self.bedroom == ['6 ห้อง']:
                if bedroom_option.text == '6 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['7'] or self.bedroom == ['7 ห้อง']:
                if bedroom_option.text == '7 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['8'] or self.bedroom == ['8 ห้อง']:
                if bedroom_option.text == '8 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['9'] or self.bedroom == ['9 ห้อง']:
                if bedroom_option.text == '9 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['10'] or self.bedroom == ['10 ห้อง']:
                if bedroom_option.text == '10 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['มากกว่า 10'] or self.bedroom == ['มากกว่า 10']:
                if bedroom_option.text == 'มากกว่า 10':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bedroom_option.text == 'ห้องสตูดิโอ':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
    ##########################################Bedrooms#########################################################
    ##########################################Bathrooms#########################################################
        self.wd.find_element(By.ID, 'select2-web_bathroom-container').click()
        bathroom_el = self.wd.find_element(By.ID, 'web_bathroom')
        for bathroom_option in bathroom_el.find_elements_by_tag_name('option'):
            if self.toilet == ['1'] or self.toilet == ['1 ห้องน้ำ']:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['2'] or self.toilet == ['2 ห้องน้ำ']:
                if bathroom_option.text == '2 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['3'] or self.toilet == ['3 ห้องน้ำ']:
                if bathroom_option.text == '3 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['4'] or self.toilet == ['4 ห้องน้ำ']:
                if bathroom_option.text == '4 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['5'] or self.toilet == ['5 ห้องน้ำ']:
                if bathroom_option.text == '5 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['มากกว่า 5']:
                if bathroom_option.text == 'มากกว่า 5':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break

    ##########################################Bathrooms#########################################################
    ##########################################Level#########################################################
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()
        level_el = self.wd.find_element(By.ID, 'web_floor')
        for level_option in level_el.find_elements_by_tag_name('option'):
            # if self.level == ['']:
            #     if level_option.text == '1-4':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '5-10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '11-20':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '21-50':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '51-99':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
                if level_option.text == '1-4':
                    level_option.click()  # select() in earlier versions of webdriver
                    break

    ##########################################Level#########################################################
    ##########################################area#########################################################
        self.wd.find_element(By.ID, 'web_useful_space').send_keys(self.max_area[0])

    ##########################################area#########################################################
    ###############################web_contract_startdate########################################################
        c_day = self.today.strftime('%Y-%m-%d')
        print(c_day)
        self.wd.find_element(By.XPATH, '//*[@id="web_contract_startdate"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="web_contract_startdate"]').send_keys(c_day)

    ###############################web_contract_startdate########################################################
    ##########################################rent price#########################################################
        self.wd.find_element(By.ID, 'web_price').send_keys(self.f_price[0])

    ##########################################rent price#########################################################
    ##########################################Special Additional#########################################################

        # self.wd.find_element(By.XPATH, '//*[@id="web_keeping_pet"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_duplex"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_penthouse"]').click()

    ##########################################Special Additional#########################################################
    ##########################################Matterport URL#########################################################
        # self.wd.find_element(By.ID, 'web_matterport').send_keys('1')

    ##########################################Matterport URL#########################################################
    ##########################################Youtube URL#########################################################
        # self.wd.find_element(By.ID, 'web_youtube').send_keys('1')

    ##########################################Youtube URL#########################################################
    ##########################################picture#########################################################
        try:
            for n_img in range(int(len(galleryUrls))):
                self.wd.find_element(By.ID, 'fileupload').send_keys(ImgsPath[n_img])
        except:
            print('no image insert')
    ##########################################picture#########################################################
        time.sleep(2)
        # nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[11]/div/div/div/div/div/button[3]')
        nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[12]/div/div/div/div/div/button[3]')


        self.wd.execute_script("arguments[0].click();", nxtbtn)
        try:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div')))
        except:
            self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[10]/div/div/div/button[2]').click()
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div')))
        self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div').click()
        self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[3]/button').click()

    ###################################PAGE2###################################
###############################condo_rent_page2#########################################

    def house_sell_page2(self):
        print('house_sell_page2')
        time.sleep(2)
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ses_web_building_type2"]')))
        self.wd.find_element(By.ID, 'ses_web_building_type2').click()
    ######################### Project name#######################################################
        web_sku = self.wd.find_element(By.XPATH, '//*[@id="web_sku"]')
        web_sku.send_keys(' ')
        
        pjn = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-container"]')#.click()
        self.wd.execute_script("arguments[0].click();", pjn)
        time.sleep(1)
        try:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '//html/body/span/span/span[1]/input')))
        except:
            self.wd.find_element(By.XPATH, '//*[@id="div_web_project_id"]/span').click()
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '//html/body/span/span/span[1]/input')))
        try:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.address[0])
            time.sleep(2)
            pjm_result = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').get_attribute('innerHTML')
            print(pjm_result)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # if self.address[0] in pjm_result:
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # else:
            #     self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            #     time.sleep(2)
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()
        except:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()

    ######################### Project name#######################################################
    #########################ทำเล#######################################################
        try:
            # check if
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[6]/span/span[1]/span').click()
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
            
            if 'อยุธยา' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('อยุธยา')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

            elif 'ตาก' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('ตาก')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()
            
            elif 'เพชรบุรี' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('เพชรบุรี')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            elif 'นราธิวาส' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('นราธิวาส')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.city[0])
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

    #########################ทำเล#######################################################
        #########################map#######################################################
            posmap = self.wd.find_element(By.ID, 'posmap')#.click()

            self.wd.execute_script('arguments[0].click();', posmap)
        except:
            print('posmap')
    #########################map#######################################################
        self.wd.find_element(By.ID, 'posmap').submit()
        time.sleep(2)
    ###################################PAGE1###################################
    ###################################PAGE2###################################
    ###########################################Bedrooms#########################################################
        print(self.wd.current_url)
        self.wait.until(EC.element_to_be_clickable((By.ID, "select2-web_room-container")))
        self.wd.find_element(By.ID, 'select2-web_room-container').click()
        bedroom_el = self.wd.find_element(By.ID, 'web_room')
        for bedroom_option in bedroom_el.find_elements_by_tag_name('option'):
            if self.bedroom == ['1'] or self.bedroom == ['1 ห้อง']:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['2'] or self.bedroom == ['2 ห้อง']:
                if bedroom_option.text == '2 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['3'] or self.bedroom == ['3 ห้อง']:
                if bedroom_option.text == '3 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['4'] or self.bedroom == ['4 ห้อง']:
                if bedroom_option.text == '4 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['5'] or self.bedroom == ['5 ห้อง']:
                if bedroom_option.text == '5 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['6'] or self.bedroom == ['6 ห้อง']:
                if bedroom_option.text == '6 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['7'] or self.bedroom == ['7 ห้อง']:
                if bedroom_option.text == '7 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['8'] or self.bedroom == ['8 ห้อง']:
                if bedroom_option.text == '8 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['9'] or self.bedroom == ['9 ห้อง']:
                if bedroom_option.text == '9 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['10'] or self.bedroom == ['10 ห้อง']:
                if bedroom_option.text == '10 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['มากกว่า 10'] or self.bedroom == ['มากกว่า 10']:
                if bedroom_option.text == 'มากกว่า 10':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
    ##########################################Bedrooms#########################################################
    ##########################################Bathrooms#########################################################
        self.wd.find_element(By.ID, 'select2-web_bathroom-container').click()
        bathroom_el = self.wd.find_element(By.ID, 'web_bathroom')
        for bathroom_option in bathroom_el.find_elements_by_tag_name('option'):
            if self.toilet == ['1'] or self.toilet == ['1 ห้องน้ำ']:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['2'] or self.toilet == ['2 ห้องน้ำ']:
                if bathroom_option.text == '2 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['3'] or self.toilet == ['3 ห้องน้ำ']:
                if bathroom_option.text == '3 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['4'] or self.toilet == ['4 ห้องน้ำ']:
                if bathroom_option.text == '4 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['5'] or self.toilet == ['5 ห้องน้ำ']:
                if bathroom_option.text == '5 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['มากกว่า 5']:
                if bathroom_option.text == 'มากกว่า 5':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break

    ##########################################Bathrooms#########################################################
    ##########################################Level#########################################################
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()
        level_el = self.wd.find_element(By.ID, 'web_floor')
        for level_option in level_el.find_elements_by_tag_name('option'):
            # if self.level == ['']:
            #     if level_option.text == '1':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '2':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '3':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '4':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '5':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '6':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '7':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '8':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '9':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == 'มากกว่า 10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
                if level_option.text == '1':
                    level_option.click()  # select() in earlier versions of webdriver
                    break
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()

    ##########################################Level#########################################################
    ##########################################area#########################################################
        global int_max_area
        # print(self.max_area)
        # print(int_max_area)
        try:
            int_max_area = int(self.max_area[0].replace(' ตร.ม.',''))
            self.wd.find_element(By.ID, 'web_area_size').send_keys(int_max_area*0.25)
        except:
            self.wd.find_element(By.ID, 'web_area_size').send_keys(self.max_area[0])

    ##########################################area#########################################################
    ##########################################Selling price#########################################################
        self.wd.find_element(By.ID, 'web_price').send_keys(self.f_price[0])

    ##########################################Selling price#########################################################
    ##########################################Special Additional#########################################################

        # self.wd.find_element(By.XPATH, '//*[@id="web_keeping_pet"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_duplex"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_penthouse"]').click()

    ##########################################Special Additional#########################################################
    ##########################################Matterport URL#########################################################
        # self.wd.find_element(By.ID, 'web_matterport').send_keys('1')

    ##########################################Matterport URL#########################################################
    ##########################################Youtube URL#########################################################
        # self.wd.find_element(By.ID, 'web_youtube').send_keys('1')

    ##########################################Youtube URL#########################################################
    ##########################################picture#########################################################
        try:
            for n_img in range(int(len(galleryUrls))):
                self.wd.find_element(By.ID, 'fileupload').send_keys(ImgsPath[n_img])
            print('insert image')
        except:
            print('no image insert')
    ##########################################picture#########################################################
        # nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[9]/div/div/div/div/div/button[3]')
        nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[10]/div/div/div/div/div/button[3]')

        self.wd.execute_script("arguments[0].click();", nxtbtn)
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div')))
        self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div').click()
        self.wd.find_element(By.XPATH,'//*[@id="post_data"]/div[3]/button').click()

        # self.wd.find_element(By.CLASS_NAME, 'web_post_accept').click()
        # self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[3]/button').click()

    ###################################PAGE2###################################
###############################house_sell_page2#########################################


    def house_rent_page2(self):#finish
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ses_web_building_type2"]')))
        self.wd.find_element(By.ID, 'ses_web_building_type2').click()
    ######################### Project name#######################################################
        web_sku = self.wd.find_element(By.XPATH, '//*[@id="web_sku"]')
        web_sku.send_keys(' ')
        
        pjn = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-container"]')#.click()
        self.wd.execute_script("arguments[0].click();", pjn)
        time.sleep(1)
        try:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
        except:
            self.wd.find_element(By.XPATH, '//*[@id="div_web_project_id"]/span').click()
            time.sleep(1)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))

        try:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.address[0])
            time.sleep(2)
            pjm_result = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').get_attribute('innerHTML')
            print(pjm_result)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # if self.address[0] in pjm_result:
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # else:
            #     self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            #     time.sleep(2)
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()
        except:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()

    ######################### Project name#######################################################
    #########################ทำเล#######################################################
        try:
            # check if
            time.sleep(2)

            self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[6]/span/span[1]/span').click()
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
            if 'อยุธยา' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('อยุธยา')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

            elif 'ตาก' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('ตาก')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()
            
            elif 'เพชรบุรี' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('เพชรบุรี')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            elif 'นราธิวาส' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('นราธิวาส')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.city[0])
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

    #########################ทำเล#######################################################
    #########################map#######################################################
            posmap = self.wd.find_element(By.ID, 'posmap')#.click()

            self.wd.execute_script('arguments[0].click();', posmap)
        except:
            print('posmap')
    #########################map#######################################################
        self.wd.find_element(By.ID, 'posmap').submit()
        time.sleep(2)
    ###################################PAGE1###################################
    ###################################PAGE2###################################
    ###########################################Bedrooms#########################################################
        self.wait.until(EC.element_to_be_clickable((By.ID, "select2-web_room-container")))
        self.wd.find_element(By.ID, 'select2-web_room-container').click()
        bedroom_el = self.wd.find_element(By.ID, 'web_room')
        for bedroom_option in bedroom_el.find_elements_by_tag_name('option'):
            if self.bedroom == ['1'] or self.bedroom == ['1 ห้อง']:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['2'] or self.bedroom == ['2 ห้อง']:
                if bedroom_option.text == '2 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['3'] or self.bedroom == ['3 ห้อง']:
                if bedroom_option.text == '3 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['4'] or self.bedroom == ['4 ห้อง']:
                if bedroom_option.text == '4 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['5'] or self.bedroom == ['5 ห้อง']:
                if bedroom_option.text == '5 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['6'] or self.bedroom == ['6 ห้อง']:
                if bedroom_option.text == '6 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['7'] or self.bedroom == ['7 ห้อง']:
                if bedroom_option.text == '7 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['8'] or self.bedroom == ['8 ห้อง']:
                if bedroom_option.text == '8 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['9'] or self.bedroom == ['9 ห้อง']:
                if bedroom_option.text == '9 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['10'] or self.bedroom == ['10 ห้อง']:
                if bedroom_option.text == '10 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['มากกว่า 10'] or self.bedroom == ['มากกว่า 10']:
                if bedroom_option.text == 'มากกว่า 10':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
    ##########################################Bedrooms#########################################################
    ##########################################Bathrooms#########################################################
        self.wd.find_element(By.ID, 'select2-web_bathroom-container').click()
        bathroom_el = self.wd.find_element(By.ID, 'web_bathroom')
        for bathroom_option in bathroom_el.find_elements_by_tag_name('option'):
            if self.toilet == ['1'] or self.toilet == ['1 ห้องน้ำ']:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['2'] or self.toilet == ['2 ห้องน้ำ']:
                if bathroom_option.text == '2 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['3'] or self.toilet == ['3 ห้องน้ำ']:
                if bathroom_option.text == '3 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['4'] or self.toilet == ['4 ห้องน้ำ']:
                if bathroom_option.text == '4 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['5'] or self.toilet == ['5 ห้องน้ำ']:
                if bathroom_option.text == '5 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['มากกว่า 5']:
                if bathroom_option.text == 'มากกว่า 5':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break

    ##########################################Bathrooms#########################################################
    ##########################################Level#########################################################
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()
        level_el = self.wd.find_element(By.ID, 'web_floor')
        for level_option in level_el.find_elements_by_tag_name('option'):
            # if self.level == ['']:
            #     if level_option.text == '1':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '2':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '3':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '4':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '5':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '6':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '7':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '8':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '9':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == 'มากกว่า 10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
                if level_option.text == '1':
                    level_option.click()  # select() in earlier versions of webdriver
                    break
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()

    ##########################################Level#########################################################
    ##########################################area#########################################################
        try:
            int_max_area = int(self.max_area[0].replace(' ตร.ม.',''))
            self.wd.find_element(By.ID, 'web_area_size').send_keys(int_max_area*0.25)
        except:
            self.wd.find_element(By.ID, 'web_area_size').send_keys(self.max_area[0])


    ##########################################area#########################################################
    ###############################web_contract_startdate########################################################
        c_day = self.today.strftime('%Y-%m-%d')
        print(c_day)
        self.wd.find_element(By.XPATH, '//*[@id="web_contract_startdate"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="web_contract_startdate"]').send_keys(c_day)

    ###############################web_contract_startdate########################################################
    ##########################################Selling price#########################################################
        self.wd.find_element(By.ID, 'web_price').send_keys(self.f_price[0])

    ##########################################Selling price#########################################################
    ##########################################Special Additional#########################################################

        # self.wd.find_element(By.XPATH, '//*[@id="web_keeping_pet"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_duplex"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_penthouse"]').click()

    ##########################################Special Additional#########################################################
    ##########################################Matterport URL#########################################################
        # self.wd.find_element(By.ID, 'web_matterport').send_keys('1')

    ##########################################Matterport URL#########################################################
    ##########################################Youtube URL#########################################################
        # self.wd.find_element(By.ID, 'web_youtube').send_keys('1')

    ##########################################Youtube URL#########################################################
    ##########################################picture#########################################################
        try:
            for n_img in range(int(len(galleryUrls))):
                self.wd.find_element(By.ID, 'fileupload').send_keys(ImgsPath[n_img])
        except:
            print('no image insert')
    ##########################################picture#########################################################
        # nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[9]/div/div/div/div/div/button[3]')
        nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[12]/div/div/div/div/div/button[3]')
        
        self.wd.execute_script("arguments[0].click();", nxtbtn)

        time.sleep(3)
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div')))
        self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div').click()
        self.wd.find_element(By.XPATH,'//*[@id="post_data"]/div[3]/button').click()

        # self.wd.find_element(By.CLASS_NAME, 'web_post_accept').click()
        # self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[3]/button').click()

    ###################################PAGE2###################################
###############################house_sell_page2#########################################

    def townhouse_sell_page2(self):
        print('townhouse_sell_page2')
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ses_web_building_type6"]')))
        self.wd.find_element(By.ID, 'ses_web_building_type6').click()
    ######################### Project name#######################################################
        web_sku = self.wd.find_element(By.XPATH, '//*[@id="web_sku"]')
        web_sku.send_keys(' ')
        time.sleep(1)
        pjn = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-container"]')#.click()
        self.wd.execute_script("arguments[0].click();", pjn)
        time.sleep(1)
        try:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
        except:
            self.wd.find_element(By.XPATH, '//*[@id="div_web_project_id"]/span').click()
            time.sleep(1)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))

        try:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.address[0])
            time.sleep(2)
            pjm_result = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').get_attribute('innerHTML')
            print(pjm_result)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # if self.address[0] in pjm_result:
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # else:
            #     self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            #     time.sleep(2)
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()
        except:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()

    ######################### Project name#######################################################
    #########################ทำเล#######################################################
        try:
            # check if
            time.sleep(2)

            self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[6]/span/span[1]/span').click()
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))

            if 'อยุธยา' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('อยุธยา')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

            elif 'ตาก' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('ตาก')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()
            
            elif 'เพชรบุรี' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('เพชรบุรี')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            elif 'นราธิวาส' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('นราธิวาส')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.city[0])
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

    #########################ทำเล#######################################################
        #########################map#######################################################
            posmap = self.wd.find_element(By.ID, 'posmap')#.click()

            self.wd.execute_script('arguments[0].click();', posmap)
        except:
            print('posmap')
    #########################map#######################################################
        self.wd.find_element(By.ID, 'posmap').submit()
        time.sleep(2)
    ###################################PAGE1###################################
    ###################################PAGE2###################################
    ###########################################Bedrooms#########################################################
        self.wait.until(EC.element_to_be_clickable((By.ID, "select2-web_room-container")))
        self.wd.find_element(By.ID, 'select2-web_room-container').click()
        bedroom_el = self.wd.find_element(By.ID, 'web_room')
        for bedroom_option in bedroom_el.find_elements_by_tag_name('option'):
            if self.bedroom == ['1'] or self.bedroom == ['1 ห้อง']:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['2'] or self.bedroom == ['2 ห้อง']:
                if bedroom_option.text == '2 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['3'] or self.bedroom == ['3 ห้อง']:
                if bedroom_option.text == '3 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['4'] or self.bedroom == ['4 ห้อง']:
                if bedroom_option.text == '4 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['5'] or self.bedroom == ['5 ห้อง']:
                if bedroom_option.text == '5 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['6'] or self.bedroom == ['6 ห้อง']:
                if bedroom_option.text == '6 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['7'] or self.bedroom == ['7 ห้อง']:
                if bedroom_option.text == '7 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['8'] or self.bedroom == ['8 ห้อง']:
                if bedroom_option.text == '8 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['9'] or self.bedroom == ['9 ห้อง']:
                if bedroom_option.text == '9 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['10'] or self.bedroom == ['10 ห้อง']:
                if bedroom_option.text == '10 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['มากกว่า 10'] or self.bedroom == ['มากกว่า 10']:
                if bedroom_option.text == 'มากกว่า 10':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
    ##########################################Bedrooms#########################################################
    ##########################################Bathrooms#########################################################
        self.wd.find_element(By.ID, 'select2-web_bathroom-container').click()
        bathroom_el = self.wd.find_element(By.ID, 'web_bathroom')
        for bathroom_option in bathroom_el.find_elements_by_tag_name('option'):
            if self.toilet == ['1'] or self.toilet == ['1 ห้องน้ำ']:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['2'] or self.toilet == ['2 ห้องน้ำ']:
                if bathroom_option.text == '2 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['3'] or self.toilet == ['3 ห้องน้ำ']:
                if bathroom_option.text == '3 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['4'] or self.toilet == ['4 ห้องน้ำ']:
                if bathroom_option.text == '4 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['5'] or self.toilet == ['5 ห้องน้ำ']:
                if bathroom_option.text == '5 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['มากกว่า 5']:
                if bathroom_option.text == 'มากกว่า 5':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break

    ##########################################Bathrooms#########################################################
    ##########################################Level#########################################################
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()
        level_el = self.wd.find_element(By.ID, 'web_floor')
        for level_option in level_el.find_elements_by_tag_name('option'):
            # if self.level == ['']:
            #     if level_option.text == '1':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '2':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '3':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '4':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '5':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '6':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '7':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '8':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '9':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == 'มากกว่า 10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
                if level_option.text == '1':
                    level_option.click()  # select() in earlier versions of webdriver
                    break
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()

    ##########################################Level#########################################################
    ##########################################area#########################################################
        try:
            int_max_area = int(self.max_area[0].replace(' ตร.ม.',''))
            self.wd.find_element(By.ID, 'web_area_size').send_keys(int_max_area*0.25)
        except:
            self.wd.find_element(By.ID, 'web_area_size').send_keys(self.max_area[0])

    ##########################################area#########################################################
    ##########################################Selling price#########################################################
        self.wd.find_element(By.ID, 'web_price').send_keys(self.f_price[0])

    ##########################################Selling price#########################################################
    ##########################################Special Additional#########################################################

        # self.wd.find_element(By.XPATH, '//*[@id="web_keeping_pet"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_duplex"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_penthouse"]').click()

    ##########################################Special Additional#########################################################
    ##########################################Matterport URL#########################################################
        # self.wd.find_element(By.ID, 'web_matterport').send_keys('1')

    ##########################################Matterport URL#########################################################
    ##########################################Youtube URL#########################################################
        # self.wd.find_element(By.ID, 'web_youtube').send_keys('1')

    ##########################################Youtube URL#########################################################
    ##########################################picture#########################################################
        try:
            for n_img in range(int(len(galleryUrls))):
                self.wd.find_element(By.ID, 'fileupload').send_keys(ImgsPath[n_img])
        except:
            print('no image insert')
    ##########################################picture#########################################################
        # nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[9]/div/div/div/div/div/button[3]')
        nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[10]/div/div/div/div/div/button[3]')

        self.wd.execute_script("arguments[0].click();", nxtbtn)
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div')))
        self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div').click()
        self.wd.find_element(By.XPATH,'//*[@id="post_data"]/div[3]/button').click()

        # self.wd.find_element(By.CLASS_NAME, 'web_post_accept').click()
        # self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[3]/button').click()

    ###################################PAGE2###################################
###############################townhouse_sell_page2#########################################


    def townhouse_rent_page2(self):#finish
        print('townhouse_rent_page2')
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ses_web_building_type6"]')))
        self.wd.find_element(By.ID, 'ses_web_building_type6').click()
    ######################### Project name#######################################################
        web_sku = self.wd.find_element(By.XPATH, '//*[@id="web_sku"]')
        web_sku.send_keys(' ')
        time.sleep(1)
        pjn = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-container"]')#.click()
        self.wd.execute_script("arguments[0].click();", pjn)
        time.sleep(1)
        try:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
        except:
            self.wd.find_element(By.XPATH, '//*[@id="div_web_project_id"]/span').click()
            time.sleep(1)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
            
        try:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.address[0])
            time.sleep(2)
            pjm_result = self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').get_attribute('innerHTML')
            print(pjm_result)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # if self.address[0] in pjm_result:
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[2]').click()
            # else:
            #     self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            #     time.sleep(2)
            #     self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()
        except:
            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys("ไม่ทราบชื่อโครงการ")
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_project_id-results"]/li[1]').click()

    ######################### Project name#######################################################
    #########################ทำเล#######################################################
        try:
            # check if
            time.sleep(2)

            self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div[6]/span/span[1]/span').click()
            self.wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/span/span/span[1]/input')))
            
            if 'อยุธยา' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('อยุธยา')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

            elif 'ตาก' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('ตาก')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()
            
            elif 'เพชรบุรี' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('เพชรบุรี')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            elif 'นราธิวาส' in self.state[0]:
                self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys('นราธิวาส')
                time.sleep(1.5)
                self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li[2]').click()

            self.wd.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(self.city[0])
            time.sleep(2)
            self.wd.find_element(By.XPATH, '//*[@id="select2-web_zone_id-results"]/li').click()

    #########################ทำเล#######################################################
        #########################map#######################################################
            posmap = self.wd.find_element(By.ID, 'posmap')#.click()

            self.wd.execute_script('arguments[0].click();', posmap)
        except:
            print('posmap')
    #########################map#######################################################
        self.wd.find_element(By.ID, 'posmap').submit()
        time.sleep(2)
    ###################################PAGE1###################################
    ###################################PAGE2###################################
    ###########################################Bedrooms#########################################################
        self.wait.until(EC.element_to_be_clickable((By.ID, "select2-web_room-container")))
        self.wd.find_element(By.ID, 'select2-web_room-container').click()
        bedroom_el = self.wd.find_element(By.ID, 'web_room')
        for bedroom_option in bedroom_el.find_elements_by_tag_name('option'):
            if self.bedroom == ['1'] or self.bedroom == ['1 ห้อง']:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['2'] or self.bedroom == ['2 ห้อง']:
                if bedroom_option.text == '2 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['3'] or self.bedroom == ['3 ห้อง']:
                if bedroom_option.text == '3 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['4'] or self.bedroom == ['4 ห้อง']:
                if bedroom_option.text == '4 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['5'] or self.bedroom == ['5 ห้อง']:
                if bedroom_option.text == '5 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['6'] or self.bedroom == ['6 ห้อง']:
                if bedroom_option.text == '6 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['7'] or self.bedroom == ['7 ห้อง']:
                if bedroom_option.text == '7 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['8'] or self.bedroom == ['8 ห้อง']:
                if bedroom_option.text == '8 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['9'] or self.bedroom == ['9 ห้อง']:
                if bedroom_option.text == '9 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['10'] or self.bedroom == ['10 ห้อง']:
                if bedroom_option.text == '10 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.bedroom == ['มากกว่า 10'] or self.bedroom == ['มากกว่า 10']:
                if bedroom_option.text == 'มากกว่า 10':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bedroom_option.text == '1 ห้อง':
                    bedroom_option.click()  # select() in earlier versions of webdriver
                    break
    ##########################################Bedrooms#########################################################
    ##########################################Bathrooms#########################################################
        self.wd.find_element(By.ID, 'select2-web_bathroom-container').click()
        bathroom_el = self.wd.find_element(By.ID, 'web_bathroom')
        for bathroom_option in bathroom_el.find_elements_by_tag_name('option'):
            if self.toilet == ['1'] or self.toilet == ['1 ห้องน้ำ']:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['2'] or self.toilet == ['2 ห้องน้ำ']:
                if bathroom_option.text == '2 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['3'] or self.toilet == ['3 ห้องน้ำ']:
                if bathroom_option.text == '3 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['4'] or self.toilet == ['4 ห้องน้ำ']:
                if bathroom_option.text == '4 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['5'] or self.toilet == ['5 ห้องน้ำ']:
                if bathroom_option.text == '5 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            elif self.toilet == ['มากกว่า 5']:
                if bathroom_option.text == 'มากกว่า 5':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break
            else:
                if bathroom_option.text == '1 ห้องน้ำ':
                    bathroom_option.click()  # select() in earlier versions of webdriver
                    break

    ##########################################Bathrooms#########################################################
    ##########################################Level#########################################################
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()
        level_el = self.wd.find_element(By.ID, 'web_floor')
        for level_option in level_el.find_elements_by_tag_name('option'):
            # if self.level == ['']:
            #     if level_option.text == '1':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '2':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '3':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '4':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '5':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '6':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '7':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '8':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '9':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == '10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
            # elif self.level == ['']:
            #     if level_option.text == 'มากกว่า 10':
            #         level_option.click()  # select() in earlier versions of webdriver
            #         break
                if level_option.text == '1':
                    level_option.click()  # select() in earlier versions of webdriver
                    break
        self.wd.find_element(By.ID, 'select2-web_floor-container').click()

    ##########################################Level#########################################################
    ##########################################area#########################################################
        try:
            int_max_area = int(self.max_area[0].replace(' ตร.ม.',''))
            self.wd.find_element(By.ID, 'web_area_size').send_keys(int_max_area*0.25)
        except:
            self.wd.find_element(By.ID, 'web_area_size').send_keys(self.max_area[0])

    ##########################################area#########################################################
        ###############################web_contract_startdate########################################################
        c_day = self.today.strftime('%Y-%m-%d')
        print(c_day)
        self.wd.find_element(By.XPATH, '//*[@id="web_contract_startdate"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="web_contract_startdate"]').send_keys(c_day)

    ###############################web_contract_startdate########################################################
    ##########################################Selling price#########################################################
        self.wd.find_element(By.ID, 'web_price').send_keys(self.f_price[0])

    ##########################################Selling price#########################################################
    ##########################################Special Additional#########################################################

        # self.wd.find_element(By.XPATH, '//*[@id="web_keeping_pet"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_duplex"]').click()
        # self.wd.find_element(By.XPATH, '//*[@id="topic_penthouse"]').click()

    ##########################################Special Additional#########################################################
    ##########################################Matterport URL#########################################################
        # self.wd.find_element(By.ID, 'web_matterport').send_keys('1')

    ##########################################Matterport URL#########################################################
    ##########################################Youtube URL#########################################################
        # self.wd.find_element(By.ID, 'web_youtube').send_keys('1')

    ##########################################Youtube URL#########################################################
    ##########################################picture#########################################################
        try:
            for n_img in range(int(len(galleryUrls))):
                self.wd.find_element(By.ID, 'fileupload').send_keys(ImgsPath[n_img])
        except:
            print('no image insert')
    ##########################################picture#########################################################
        # nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[11]/div/div/div/div/div/button[3]')
        nxtbtn = self.wd.find_element(By.XPATH, '//*[@id="post_data_main"]/div[12]/div/div/div/div/div/button[3]')

        self.wd.execute_script("arguments[0].click();", nxtbtn)
        self.wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div')))
        self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[1]/div/div[2]/div').click()
        self.wd.find_element(By.XPATH,'//*[@id="post_data"]/div[3]/button').click()

        # self.wd.find_element(By.CLASS_NAME, 'web_post_accept').click()
        # self.wd.find_element(By.XPATH, '//*[@id="post_data"]/div[3]/button').click()

    ###################################PAGE2###################################
###############################townhouse_rent_page2#########################################

    ################################################################

    def upstatus(self,id):
        myquery = {"_id":ObjectId(id)}
        newvalues = {'$set': {'livinginsider_status':''}}
        # self.mycol.update_one


    def quit(self):
        self.wd.quit()

class startlvisd:
    def startbotlvisd(self,id):
        try:
            print('1111111111111111111111')
            print(id)
            fn = livinginsider()
            
            print('fn.click_login()')
            fn.click_login()
            fn.query(id)
            print('fn.query(id)')
            fn.query_uw()
            fn.fill_login()
            fn.click_createpost()
            fn.fill_data()
            fn.quit()
            return True
        except Exception as e:
            print(e)
            fn.quit()
            return False
